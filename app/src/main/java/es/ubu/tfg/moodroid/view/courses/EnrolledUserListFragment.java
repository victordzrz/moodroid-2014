package es.ubu.tfg.moodroid.view.courses;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import java.util.ArrayList;

import es.ubu.tfg.moodroid.R;
import es.ubu.tfg.moodroid.presenter.CourseManager;
import es.ubu.tfg.moodroid.presenter.ExecutorManager;
import es.ubu.tfg.moodroid.presenter.GroupManager;
import es.ubu.tfg.moodroid.presenter.MoodleFunctionExecutor;
import es.ubu.tfg.moodroid.presenter.OnFunctionCompletedListener;
import es.ubu.tfg.moodroid.model.Group;
import es.ubu.tfg.moodroid.model.EnrolledUser;
import es.ubu.tfg.moodroid.view.FadeInArrayAdapter;
import es.ubu.tfg.moodroid.view.MainActivity;
import es.ubu.tfg.moodroid.view.MoodroidPopulableUI;
import es.ubu.tfg.moodroid.view.groups.GroupListFragment;

/**
 * Clase que implementa la vista para mostrar los participantes de un curso.
 * @author Víctor Díez Rodríguez
 * @version 2.0
 */
public class EnrolledUserListFragment extends MoodroidPopulableUI<ArrayList<EnrolledUser>> implements AdapterView.OnItemClickListener {
	/**
	 * Identificador del curso.
	 */
	private String courseid;
	/**
	 * Nombre del curso.
	 */
	private String fullname;
    /**
     * Indica si el usuario puede ver los grupos del curso.
     */
    private boolean hasGroups=false;


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        checkGroupPermissions();
        setTitleEnabled(true);
        setTitle(getResources().getString(R.string.enrolled_users));
    }

    @Override
    protected void populateInterface(ArrayList<EnrolledUser> objects, View rootView) {
        setListAdapter(new EnrolledUserArrayAdapter(getActivity().getApplicationContext(),
                objects));
        getListView().setOnItemClickListener(this);
        setEmptyText(getResources().getString(R.string.no_enrolled_users_available));
    }

    @Override
    protected MoodleFunctionExecutor loadContent(OnFunctionCompletedListener<ArrayList<EnrolledUser>> callback) {
        String token= PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext()).getString("token","invalid_token");

        return CourseManager.getEnrolledUsers(courseid, token, callback);

    }

    @Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        courseid =(String) getArguments().get("courseid");
        fullname = (String)  getArguments().get("fullname");
        if(savedInstanceState!=null){
            setHasGroups(savedInstanceState.getBoolean("hasGroups"));
        }
	}

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("hasGroups",hasGroups());
    }

    /**
     * Establece si el usuario puede ver los grupos del curso.
     * @param hasGroups si el usuario puede ver los grupos del curso.
     */
    private void setHasGroups(boolean hasGroups){
        setHasOptionsMenu(hasGroups);
        getActivity().supportInvalidateOptionsMenu();
        this.hasGroups=hasGroups;
    }

    /**
     * Comprueba si el usuario puede ver los grupos del curso.
     * @return si el usuario puede ver los grupos del curso.
     */
    private boolean hasGroups(){
        return hasGroups;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu,inflater);
        inflater.inflate(R.menu.see_groups_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.action_groups){
            Fragment grupos=new GroupListFragment();
            Bundle args=new Bundle();
            args.putString("courseid", courseid);
            args.putSerializable("participantes", getContent());
            grupos.setArguments(args);
            ((MainActivity)getActivity()).replaceFragment(grupos, FragmentTransaction.TRANSIT_FRAGMENT_OPEN, true);

            return true;

        }
        else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ArrayList<EnrolledUser> enrolledUsers = getContent();
        Fragment perfilFragment=new EnrolledUserProfileFragment();
        Bundle args=new Bundle();
        args.putSerializable("participante", enrolledUsers.get(position));
        perfilFragment.setArguments(args);
        ((MainActivity)getActivity()).replaceFragment(perfilFragment,FragmentTransaction.TRANSIT_FRAGMENT_OPEN,true);


    }

    /**
     * Comprueba que se tiene los permisos necesarios para ver los grupos del curso.
     */
    private void checkGroupPermissions(){
        SharedPreferences preferences= PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        String token= preferences.getString("token","invalid_token");
        OnFunctionCompletedListener<ArrayList<Group>> callback= new OnFunctionCompletedListener<ArrayList<Group>>() {
            @Override
            public void onFunctionCompleted(ArrayList<Group> result) {
                setHasGroups(result!=null);
                stopProgressDialog();
            }
        };
        ExecutorManager.getInstance().addExecutor(GroupManager.getCourseGroups(courseid, token, callback));
        startProgressDialog();
    }


    /**
     * Adaptador para participantes de un curso.
     */
    private class EnrolledUserArrayAdapter extends FadeInArrayAdapter<EnrolledUser> {

        /**
         * Lista de participantes del curso.
         */
        private ArrayList<EnrolledUser> enrolledUsers;

        /**
         * Crea un nuevo adaptador de participantes.
         * @param context contexto de la aplicación.
         * @param enrolledUsers participantes del curso.
         */
        public EnrolledUserArrayAdapter(Context context, ArrayList<EnrolledUser> enrolledUsers){
            super(context,R.layout.list_enrolleduseritem, enrolledUsers);
            this.enrolledUsers = enrolledUsers;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            View v = convertView;

            if (v == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = inflater.inflate(R.layout.list_enrolleduseritem, null);
            }

            EnrolledUser i = enrolledUsers.get(position);

            if (i != null) {

                TextView participanteName = (TextView) v.findViewById(R.id.participante_nombre);
                TextView roles = (TextView) v.findViewById(R.id.participante_rol);

                if(participanteName!=null){
                    participanteName.setText(i.getFullname());
                }
                if(roles!=null){
                    StringBuilder sb=new StringBuilder();
                    for(int j=0;j<i.getRoles().size();j++){
                        if(j!=0){
                            sb.append("\n");
                        }
                        sb.append(i.getRoles().get(j));
                    }
                    roles.setText(sb.toString());
                }

                if(hasGroups()){
                    TextView participanteGrupos=(TextView)v.findViewById(R.id.participante_grupo);
                    participanteGrupos.setVisibility(View.VISIBLE);
                    StringBuilder sb=new StringBuilder();
                    for(int j=0;j<i.getGroups().size();j++){
                        if(j!=0){
                            sb.append("\n");
                        }
                        sb.append(i.getGroups().get(j).getName());
                    }
                    participanteGrupos.setText(sb.toString());

                }
            }
            return applyRowAnimation(v);

        }

    }
}