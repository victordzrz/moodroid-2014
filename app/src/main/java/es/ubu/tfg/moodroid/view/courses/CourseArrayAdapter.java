package es.ubu.tfg.moodroid.view.courses;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import es.ubu.tfg.moodroid.R;
import es.ubu.tfg.moodroid.model.Course;
import es.ubu.tfg.moodroid.view.FadeInArrayAdapter;

/**
 * Adaptador para poblar una lista de cursos.
 *
 * @author Víctor Díez Rodríguez
 * @version 1.0
 */
public class CourseArrayAdapter extends FadeInArrayAdapter<Course> {
    /**
     * Cursos con los que poblar la lista.
     */
    private ArrayList<Course> courses;


    /**
     * Crea un nuevo adaptador de cursos.
     * @param context contexto de la aplicación.
     * @param courses cursos con los que poblar la lista.
     */
    public CourseArrayAdapter(Context context, ArrayList<Course> courses) {
        super(context, R.layout.list_courseitem, courses);
        this.courses = courses;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View v = convertView;

        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.list_courseitem, null);
        }

        Course i = courses.get(position);

        if (i != null) {

            TextView courseName = (TextView) v.findViewById(R.id.texto_curso);

            if(courseName!=null){
                courseName.setText(i.getName());
            }

        }
        return applyRowAnimation(v);

    }

}