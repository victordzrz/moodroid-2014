package es.ubu.tfg.moodroid.model;

import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Clase para representar un curso de Moodle.
 * @author Víctor Díez Rodríguez
 * @version 1.0
 *
 */
public class Course implements Serializable {
    /**
     * id del curso.
     */
    private String id;
    /**
     * Nombre del curso.
     */
    private String name;
    /**
     * Módulos del curso.
     */
    private ArrayList<Module> modules;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
    public void addModule(Module module) {
        modules.add(module);
    }

    public ArrayList<Module> getModules() {
        return modules;
    }

    public Course(String id, String name){
        this.id=id;
        this.name=name;
        Log.i("Curso", name);
        modules =new ArrayList<Module>();
    }

    public String toString(){
        return id+name;
    }
}
