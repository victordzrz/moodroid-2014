package es.ubu.tfg.moodroid.model;

import java.io.InputStream;
import java.util.Scanner;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

/**
 * Clase que implementa GestorPOST.
 * @author Alvar Alonso �lvarez
 * @version 1.0
 *
 */
public class    GestorPOST {
	/**
	 * Inicializaci�n del gestorPost que se va a utilizar.
	 */
	private static GestorPOST gestorPost = null;
	/**
	 * La ip que se utiliza para contectar la aplicaci�n al servidor.
	 */
	public static String DOMINIO = "";
	/**
	 * El servidor donde se conecta la aplicaci�n.
	 */
	private final static String SERVIDOR = "/webservice/rest/server.php";
	/**
	 * La url completa para conectar la aplicaci�n al servidor.
	 */
	public static String WSURL = "http://" + DOMINIO + SERVIDOR; 
	/**
	 * Obtiene el gestor post.
	 * @return el gestor post.
	 */
	public static GestorPOST obtenerGestorPost() {
		if (gestorPost == null) {
			gestorPost = new GestorPOST();
		}
		return gestorPost;
	}
	/**
	 * Crea el dominio.
	 * @param string el dominio.
	 */
	public void setDOMINIO(String string) {
		DOMINIO = string;
		WSURL = "http://" + DOMINIO + SERVIDOR; 
	}
	
	private GestorPOST() {

	}
	/**
	 * Se ejecuta el post.
	 * @param httpPost el post pasado por las clases.
	 * @param editarJSON si hace falta, se crea la cabecera en el json.
	 * @return la cadena creada para ejecutar el post.
	 */
	private String ejecutarPOST(HttpPost httpPost, boolean editarJSON) {
		String string = "";

		try {
			DefaultHttpClient httpclient = new DefaultHttpClient();
			HttpResponse response = httpclient.execute(httpPost);
			HttpEntity entity = response.getEntity();

			if (entity != null) {
				InputStream inputStream = entity.getContent();
				string = inputStreamAString(inputStream);
				inputStream.close();
				Log.e(GestorPOST.class.toString(), "Entity obtenido correctamente.");
			}

			if (editarJSON) {
				StringBuilder stringBuilder  = new StringBuilder("{\"json\":");
				stringBuilder.append(string).append("}");
				string = stringBuilder.toString();
				Log.e(GestorPOST.class.toString(), "JSON editado correctamente.");
			}
		} catch (Exception e) {
			Log.e(GestorPOST.class.toString(), "No se ha obtenido respuesta del servidor." + WSURL);
		    return null;
        }

		return string;

	}
	/**
	 * Obtiene el JSONArray necesario para crear el post en alguna clase.
	 * @param httpPost el post pasado por las clases.
	 * @return jsonArray el jsonArray obtenido.
	 */
	public JSONArray obtenerJSONArray(HttpPost httpPost) {
		JSONArray jsonArray = null;

		try {
			String jsonString = ejecutarPOST(httpPost, true);
			JSONObject jsonObject = new JSONObject(jsonString);
			jsonArray = jsonObject.getJSONArray("json");
		} catch (JSONException e) {
			Log.e(GestorPOST.class.toString(), "Error parseando JSONArray " + e.toString());
		}

		return jsonArray;
	}
	/**
	 * Obtiene el JSONObject necesario para crear el post en alguna clase.
	 * @param httpPost el post pasado por las clases.
	 * @return jsonObject el jsonObject obtenido.
	 */
	public JSONObject obtenerJSONObject(HttpPost httpPost) {
		JSONObject jsonObject = null;

		try {
			String jsonString = ejecutarPOST(httpPost, false);
			jsonObject = new JSONObject(jsonString);
		} catch (JSONException e) {
			Log.e(GestorPOST.class.toString(), "Error parseando JSONObject " + e.toString());
		}

		return jsonObject;
	}
    /**
     * Obtiene el string JSON necesario para crear el post en alguna clase.
     * @param httpPost el post pasado por las clases.
     * @return jsonObject el string obtenido.
     */
    public String obtenerJSONString(HttpPost httpPost) {

        return ejecutarPOST(httpPost, false);

    }

	/**
	 * Convierte a cadena el Stream recibido.
	 * @param is
	 * @return la cadena recibida.
	 */
	private String inputStreamAString(InputStream inputStream) {
		Scanner s = new Scanner(inputStream).useDelimiter("\\A");
		return s.hasNext() ? s.next() : "";
	}
}
