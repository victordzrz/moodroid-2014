package es.ubu.tfg.moodroid.view.courses;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.util.Locale;

import es.ubu.tfg.moodroid.R;
import es.ubu.tfg.moodroid.model.EnrolledUser;
import es.ubu.tfg.moodroid.view.TitleFragment;

/**
 * Clase que implementa la vista para mostrar el perfil de un participante.
 * @author Víctor Díez Rodríguez
 * @version 2.0
 */
public class EnrolledUserProfileFragment extends TitleFragment {
    /**
     * Participante cuyo perfil se desea mostrar.
     */
    private EnrolledUser enrolledUser;
    /**
     * Imagen de perfil del participante.
     */
    private Bitmap imageProfile;


    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View rootView =inflater.inflate(R.layout.enrolled_user_layout,container,false);
        ListView lista=(ListView)rootView.findViewById(R.id.list_cursoparticipante);
        lista.setAdapter(new CourseArrayAdapter(getActivity().getApplicationContext(),
                enrolledUser.getCourses()));

        TextView nombre=(TextView)rootView.findViewById(R.id.perfilparticipante_nombre);
        TextView email=(TextView)rootView.findViewById(R.id.perfilparticipante_email);
        TextView ultimoAcceso=(TextView)rootView.findViewById(R.id.perfilparticipante_uacceso);
        ImageView foto=(ImageView)rootView.findViewById(R.id.perfilparticipante_image);

        nombre.setText(enrolledUser.getFullname());
        email.setText(enrolledUser.getEmail());
        DateFormat format= DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT, Locale.getDefault());
        ultimoAcceso.setText(format.format(enrolledUser.getLastAccess()));

        if(imageProfile !=null){
            foto.setImageBitmap(imageProfile);
        }
        else{
            loadImage();
        }


        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.user_profile_menu,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.menu_contact_sync){
            Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
            intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
            intent.putExtra(ContactsContract.Intents.Insert.EMAIL,enrolledUser.getEmail());
            intent.putExtra(ContactsContract.Intents.Insert.NAME,enrolledUser.getFullname());
            startActivity(intent);
            return true;
        }
        else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitleEnabled(true);
        setTitle(enrolledUser.getFullname());
    }

    /**
     * Carga la imagen de perfil del participante.
     */
    private void loadImage(){
        AsyncTask<URL, Void, Bitmap> imageLoader= new AsyncTask<URL, Void, Bitmap>() {
            @Override
            protected Bitmap doInBackground(URL... params) {
                HttpURLConnection conn = null;
                try {
                    conn = (HttpURLConnection) params[0].openConnection();
                    conn.connect();
                    imageProfile = BitmapFactory.decodeStream(conn.getInputStream());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return imageProfile;
            }

            @Override
            protected void onPostExecute(Bitmap result) {
                View rootView=getView();
                ImageView foto=(ImageView)rootView.findViewById(R.id.perfilparticipante_image);
                foto.setImageBitmap(imageProfile);
            }
        };

        try {
            URL imagenUrl = new URL(enrolledUser.getProfileImageURL());
            imageLoader.execute(imagenUrl);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }


	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        this.enrolledUser =(EnrolledUser)getArguments().getSerializable("participante");
	}
}