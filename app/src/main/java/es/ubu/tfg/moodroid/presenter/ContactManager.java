package es.ubu.tfg.moodroid.presenter;

import java.util.ArrayList;

import es.ubu.tfg.moodroid.model.Contact;
import es.ubu.tfg.moodroid.model.parser.ContactListParser;
import es.ubu.tfg.moodroid.model.parser.SearchContactListParser;
import es.ubu.tfg.moodroid.model.parser.SimpleParser;

/**
 * Clase para gestionar los contactos de Moodle.
 * @author Víctor Díez Rodríguez
 * @version 1.0
 *
 */
public class ContactManager {
    /**
     * Busca usuarios en Moodle.
     * @param cadena cadena de búsqueda.
     * @param token token de usuario.
     * @param callback callback que se debe ejecutar con el resultado de la función.
     * @return ejecutor que ejecuta la función.
     */
    public static MoodleFunctionExecutor searchContacts(String cadena, String token, OnFunctionCompletedListener<ArrayList<Contact>> callback){
        MoodleFunctionExecutor<ArrayList<Contact>> executor=new MoodleFunctionExecutor<ArrayList<Contact>>();
        executor.setOnFunctionCompletedListener(callback);
        executor.addParams("wstoken", token);
        executor.addParams("searchtext", cadena);
        executor.setParser(SearchContactListParser.getInstance());
        executor.execute("core_message_search_contacts");
        return executor;
    }

    /**
     * Bloquea un usuario de la lista de contactos.
     *
     * @param contactID id del contacto.
     * @param token token de usuario.
     * @param callback callback que se debe ejecutar con el resultado de la función.
     * @return ejecutor que ejecuta la función.
     */
    public static MoodleFunctionExecutor blockContact(String contactID, String token, OnFunctionCompletedListener<String> callback){
        MoodleFunctionExecutor<String> executor=new MoodleFunctionExecutor<String>();
        executor.addParams("userids[0]",contactID);
        executor.addParams("wstoken",token);
        executor.setOnFunctionCompletedListener(callback);
        executor.setParser(SimpleParser.getInstance());
        executor.execute("core_message_block_contacts");
        return executor;
    }

    /**
     * Elimina un contacto de la lista de contactos.
     * @param idContacto id del contacto.
     * @param token token de usuario.
     * @param callback callback que se debe ejecutar con el resultado de la función.
     * @return ejecutor que ejecuta la función.
     */
    public static MoodleFunctionExecutor deleteContact(String idContacto, String token, OnFunctionCompletedListener<String> callback){
        MoodleFunctionExecutor<String> executor=new MoodleFunctionExecutor<String>();
        executor.addParams("userids[0]",idContacto);
        executor.addParams("wstoken",token);
        executor.setOnFunctionCompletedListener(callback);
        executor.setParser(SimpleParser.getInstance());
        executor.execute("core_message_delete_contacts");
        return executor;
    }

    /**
     * Obtiene los contactos de la lista de contactos.
     *
     * @param token token de usuario.
     * @param callback callback que se debe ejecutar con el resultado de la función.
     * @return ejecutor que ejecuta la función.
     */
    public static MoodleFunctionExecutor getContacts(String token, OnFunctionCompletedListener<ArrayList<Contact>> callback){
        MoodleFunctionExecutor<ArrayList<Contact>> executor=new MoodleFunctionExecutor<ArrayList<Contact>>();
        executor.setOnFunctionCompletedListener(callback);
        executor.addParams("wstoken", token);
        executor.setParser(ContactListParser.getInstance());
        executor.execute("core_message_get_contacts");
        return executor;
    }

    /**
     * Envía un mensaje a un contacto en Moodle.
     *
     * @param destinoID id del contacto de destino.
     * @param mensaje mensaje a enviar.
     * @param token token de usuario.
     * @param callback callback que se debe ejecutar con el resultado de la función.
     * @return ejecutor que ejecuta la función.
     */
    public static MoodleFunctionExecutor sendMessage(String destinoID, String mensaje, String token, OnFunctionCompletedListener<String> callback){
        MoodleFunctionExecutor<String> executor = new MoodleFunctionExecutor<String>();
        executor.addParams("wstoken", token);
        executor.addParams("messages[0][touserid]", destinoID);
        executor.addParams("messages[0][text]", mensaje);
        executor.setOnFunctionCompletedListener(callback);
        executor.setParser(SimpleParser.getInstance());
        executor.execute("core_message_send_instant_messages");
        return executor;
    }

    /**
     * Añade un contacto a la lista de contactos.
     *
     * @param idContacto id del contacto.
     * @param token token de usuario.
     * @param callback callback que se debe ejecutar con el resultado de la función.
     * @return ejecutor que ejecuta la función.
     */
    public static MoodleFunctionExecutor addContact(String idContacto, String token, OnFunctionCompletedListener<String> callback){
        MoodleFunctionExecutor<String> executor = new MoodleFunctionExecutor<String>();
        executor.addParams("wstoken", token);
        executor.addParams("userids[0]", idContacto);
        executor.setOnFunctionCompletedListener(callback);
        executor.setParser(SimpleParser.getInstance());
        executor.execute("core_message_create_contacts");
        return executor;
    }
}
