package es.ubu.tfg.moodroid.presenter;

import android.util.Log;

import java.util.ArrayList;

import es.ubu.tfg.moodroid.model.Group;
import es.ubu.tfg.moodroid.model.parser.GroupListParser;
import es.ubu.tfg.moodroid.model.parser.SimpleParser;

/**
 * Clase para gestionar los grupos de Moodle.
 * @author Víctor Díez Rodríguez
 * @version 1.0
 *
 */
public class GroupManager {
    /**
     * Obtiene los grupos de un curso.
     *
     * @param courseID id del curso.
     * @param token token de usuario.
     * @param callback callback que se debe ejecutar con el resultado de la función.
     * @return ejecutor que ejecuta la función.
     */
    public static MoodleFunctionExecutor getCourseGroups(String courseID, String token, OnFunctionCompletedListener<ArrayList<Group>> callback){
        MoodleFunctionExecutor<ArrayList<Group>> executor=new MoodleFunctionExecutor<ArrayList<Group>>();
        executor.addParams("wstoken",token);
        executor.addParams("courseid",courseID);
        executor.setOnFunctionCompletedListener(callback);
        executor.setParser(GroupListParser.getInstance());
        executor.execute("core_group_get_course_groups");
        return executor;
    }

    /**
     * Crea un nuevo grupo en un curso.
     * @param courseID id del curso.
     * @param name nombre del grupo.
     * @param description descripción del grupo.
     * @param token token de usuario.
     * @param callback callback que se debe ejecutar con el resultado de la función.
     * @return ejecutor que ejecuta la función.
     */
    public static MoodleFunctionExecutor createGroup(String courseID, String name, String description, String token, OnFunctionCompletedListener<String> callback){
        MoodleFunctionExecutor<String> executor=new MoodleFunctionExecutor<String>();
        executor.addParams("wstoken",token);
        executor.addParams("groups[0][courseid]",courseID);
        executor.addParams("groups[0][name]",name);
        executor.addParams("groups[0][description]",description);
        executor.setOnFunctionCompletedListener(callback);
        executor.setParser(SimpleParser.getInstance());
        executor.execute("core_group_create_groups");
        return executor;
    }

    /**
     * Elimina un grupo.
     * @param groupID id del grupo.
     * @param token token de usuario.
     * @param callback callback que se debe ejecutar con el resultado de la función.
     * @return ejecutor que ejecuta la función.
     */
    public static MoodleFunctionExecutor deleteGroup(String groupID, String token, OnFunctionCompletedListener<String> callback){
        MoodleFunctionExecutor<String> executor=new MoodleFunctionExecutor<String>();
        executor.addParams("wstoken",token);
        executor.addParams("groupids[0]",groupID);
        executor.setOnFunctionCompletedListener(callback);
        executor.setParser(SimpleParser.getInstance());
        executor.execute("core_group_delete_groups");
        return executor;
    }

    /**
     * Añade miembros a un grupo.
     * @param userIDs ids de los nuevos miembros.
     * @param groupID id del grupo.
     * @param token token de usuario.
     * @param callback callback que se debe ejecutar con el resultado de la función.
     * @return ejecutor que ejecuta la función.
     */
    public static MoodleFunctionExecutor addGroupMembers(ArrayList<String> userIDs, String groupID, String token, OnFunctionCompletedListener<String> callback){
        MoodleFunctionExecutor<String> executor=new MoodleFunctionExecutor<String>();
        executor.addParams("wstoken",token);
        for(int i=0;i<userIDs.size();i++){
            executor.addParams("members["+i+"][groupid]",groupID);
            executor.addParams("members["+i+"][userid]",userIDs.get(i));
            Log.i("GroupManager", "Anadiendo miembro"+userIDs.get(i)+"a"+groupID);
        }
        executor.setParser(SimpleParser.getInstance());
        executor.setOnFunctionCompletedListener(callback);
        executor.execute("core_group_add_group_members");
        return executor;
    }

    /**
     * Elimina miembros de un grupo.
     *
     * @param userIDs id de los miembros a eliminar.
     * @param groupID id del grupo.
     * @param token token de usuario.
     * @param callback callback que se debe ejecutar con el resultado de la función.
     * @return ejecutor que ejecuta la función.
     */
    public static MoodleFunctionExecutor deleteGroupMembers(ArrayList<String> userIDs, String groupID, String token, OnFunctionCompletedListener<String> callback){
        MoodleFunctionExecutor<String> executor=new MoodleFunctionExecutor<String>();
        executor.addParams("wstoken",token);
        for(int i=0;i<userIDs.size();i++){
            executor.addParams("members["+i+"][groupid]",groupID);
            executor.addParams("members["+i+"][userid]",userIDs.get(i));
            Log.i("GroupManager", "Eliminando miembro"+userIDs.get(i)+"de"+groupID);
        }
        executor.setOnFunctionCompletedListener(callback);
        executor.setParser(SimpleParser.getInstance());
        executor.execute("core_group_delete_group_members");
        return executor;
    }
}
