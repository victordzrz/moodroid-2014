package es.ubu.tfg.moodroid.view.contacts;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import es.ubu.tfg.moodroid.R;
import es.ubu.tfg.moodroid.presenter.ContactManager;
import es.ubu.tfg.moodroid.presenter.ImageLoader;
import es.ubu.tfg.moodroid.presenter.MoodleFunctionExecutor;
import es.ubu.tfg.moodroid.presenter.OnFunctionCompletedListener;
import es.ubu.tfg.moodroid.presenter.OnImageLoadedListener;
import es.ubu.tfg.moodroid.model.Contact;
import es.ubu.tfg.moodroid.view.FadeInArrayAdapter;
import es.ubu.tfg.moodroid.view.LoadingFragment;
import es.ubu.tfg.moodroid.view.MainActivity;
import es.ubu.tfg.moodroid.view.MoodroidPopulableUI;

/**
 * Clase que implementa la vista para mostrar la lista de contactos.
 *
 * @author Víctor Díez Rodríguez
 * @version 1.0
 */
public class ContactsFragment extends MoodroidPopulableUI<ArrayList<Contact>> {
    /**
     * Contacto de la lista seleccionado.
     */
    private Integer selectedContact=null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if(savedInstanceState!=null){
            //Devuelve 0 si no existe.
            selectedContact = savedInstanceState.getInt("selectedContact", -1);
            if (selectedContact == -1) {
                selectedContact = null;
            }
        }
        if (selectedContact == null) {
            Log.e("SelectedContact", "null");
        } else {
            Log.e("SelectedContact", selectedContact.toString());
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitleEnabled(true);
        setTitle(getResources().getString(R.string.contacts));
        setSelectedContact(selectedContact);
    }

    @Override
    protected void populateInterface(ArrayList<Contact> objects, View rootView) {
        ContactArrayAdapter adapter=new ContactArrayAdapter(getActivity().getApplicationContext()
                , getContent());
        setListAdapter(adapter);
        getListView().setOnItemLongClickListener(adapter);
        getListView().setOnItemClickListener(adapter);
        setEmptyText(getResources().getString(R.string.empty_contact_list));
    }


    @Override
    protected MoodleFunctionExecutor loadContent(OnFunctionCompletedListener callback) {
        SharedPreferences sp= PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        String token    = sp.getString("token","invalid_token");

        return ContactManager.getContacts(token, callback);
    }

    @Override
    public void onCreateOptionsMenu(
            Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu,inflater);
        if (selectedContact != null) {
            inflater.inflate(R.menu.selected_contact_menu, menu);
        } else {
            inflater.inflate(R.menu.search_contact_menu, menu);
            MenuItem item = menu.findItem(R.id.contacto_search);
            final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String s) {
                    Log.e("Contactos", "Buscando " + s);
                    Bundle arguments = new Bundle();
                    arguments.putString("searchtext", s);
                    Fragment busqueda = new SearchContactsFragment();
                    busqueda.setArguments(arguments);
                    ((MainActivity)getActivity()).replaceFragment(busqueda,FragmentTransaction.TRANSIT_FRAGMENT_OPEN,true);

                    return true;
                }

                @Override
                public boolean onQueryTextChange(String s) {
                    return false;
                }
            });
            searchView.setIconified(true);
            View e = searchView.findViewById(R.id.search_plate);
            e.setBackgroundResource(R.drawable.moodroidtheme_searchview_plate);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==R.id.action_contact_block) {
            blockContact(getContent().get(selectedContact).getId());
            return true;
            }
        if(item.getItemId()==R.id.action_contact_remove){
            deleteContact(getContent().get(selectedContact).getId());
            return true;
        }
        else{
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(selectedContact!=null) {
            outState.putInt("selectedContact", selectedContact);
        }
    }

    /**
     * Bloquea un contacto en Moodle.
     * @param idContacto id del contacto a bloquear.
     */
    public void blockContact(String idContacto) {
        Log.e("Contactos","Bloqueando id="+idContacto);
        String token=PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext()).getString("token","invalid_token");

        final Fragment loadingFragment=new LoadingFragment();
        OnFunctionCompletedListener callback=new OnFunctionCompletedListener<String>() {
            @Override
            public void onFunctionCompleted(String jsonString) {
                if (jsonString.equals("[]")) {
                    Toast toast = Toast.makeText(getActivity(), getResources().getString(R.string.contact_blocked), Toast.LENGTH_SHORT);
                    toast.show();
                }
                Log.e("Contactos",jsonString);
                setSelectedContact(null);
                getActivity().getSupportFragmentManager().beginTransaction().remove(loadingFragment);
                createInterface();
            }
        };
        ContactManager.blockContact(idContacto, token, callback);
        getActivity().getSupportFragmentManager().beginTransaction().add(R.id.fragment_container,loadingFragment);


    }

    /**
     * Elimina un contacto en Moodle.
     * @param idContacto id del contacto a eliminar.
     */
    public void deleteContact(String idContacto) {
        Log.e("Contactos","Eliminando id="+idContacto);
        String token=PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext()).getString("token", "invalid_token");

        final Fragment loadingFragment=new LoadingFragment();
        OnFunctionCompletedListener callback=new OnFunctionCompletedListener<String>() {
            @Override
            public void onFunctionCompleted(String jsonString) {
                Toast toast = Toast.makeText(getActivity(), getResources().getString(R.string.contact_deleted), Toast.LENGTH_SHORT);
                toast.show();
                Log.e("Contactos",jsonString);
                setSelectedContact(null);
                getActivity().getSupportFragmentManager().beginTransaction().remove(loadingFragment).commit();
                createInterface();
            }
        };

        ContactManager.deleteContact(idContacto, token, callback);
        getActivity().getSupportFragmentManager().beginTransaction().add(R.id.fragment_container,loadingFragment).commit();

    }

    /**
     * Establece el contacto seleccionado.
     * @param i posición del contacto seleccionado.
     */
    private void setSelectedContact(Integer i){
        if(selectedContact!=null && getListView()!=null){
            View child=getListView().getChildAt(selectedContact);
            if(child!=null){
                child.setBackgroundResource(R.drawable.abc_item_background_holo_light);
            }
        }
        selectedContact=i;
        getActivity().supportInvalidateOptionsMenu();
    }

    @Override
    public boolean onBackPressed(){


        if(selectedContact!=null){
            setSelectedContact(null);
            return true;
        }

        return false;
    }

    /**
     * Adaptador para poblar la lista de contactos.
     */
    private class ContactArrayAdapter extends FadeInArrayAdapter<Contact> implements AdapterView.OnItemClickListener,AdapterView.OnItemLongClickListener {
        /**
         * Contactos con los que poblar.
         */
        private ArrayList<Contact> contacts;

        /**
         * Crea un nuevo adaptador de contactos.
         * @param context contexto de la aplicación.
         * @param contacts contactos con los que poblar.
         */
        public ContactArrayAdapter(Context context, ArrayList<Contact> contacts){
            super(context,R.layout.list_contactitem, contacts);
            this.contacts = contacts;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View v = convertView;

            if (v == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = inflater.inflate(R.layout.list_contactitem, null);
            }

            if(selectedContact!=null && selectedContact.equals(position)){
                v.setBackgroundColor(getResources().getColor(R.color.selected));
            }else{
                v.setBackgroundResource(R.drawable.abc_item_background_holo_light);
            }

            Contact c= contacts.get(position);

            if (c != null) {

                TextView contactName = (TextView) v.findViewById(R.id.contacto_nombre);
                TextView contactStatus = (TextView) v.findViewById(R.id.contacto_estado);
                TextView contactMessages = (TextView) v.findViewById(R.id.contacto_mensajes);
                final ImageView contactImage = (ImageView) v.findViewById(R.id.contacto_imagen);


                if(contactName!=null){
                    contactName.setText(c.getFullname());
                }
                if(contactStatus!=null){
                    String status=c.getStatus();
                    contactStatus.setText(status);
                    if(status.equals(Contact.STATUS_ONLINE)){
                        contactStatus.setTextColor(getResources().getColor(R.color.greenMoodle));
                    }
                    else if(status.equals(Contact.STATUS_OFFLINE)){
                        contactStatus.setTextColor(getResources().getColor(R.color.blackMoodle));
                    }
                    else if(status.equals(Contact.STATUS_STRANGER)){
                        contactStatus.setTextColor(getResources().getColor(R.color.redMoodle));
                    }
                }
                if(contactMessages!=null){
                    int messages=c.getUnreadMessages();
                    contactMessages.setText(String.valueOf(messages));
                    if(messages>0){
                        contactMessages.setTextColor(getResources().getColor(R.color.redMoodle));
                    }
                    else{
                        contactMessages.setTextColor(getResources().getColor(R.color.white));
                    }
                }

                if (contactImage != null) {
                    String profileURL = c.getProfileimageurl();
                    if (profileURL != null) {
                        ImageLoader loader = new ImageLoader();
                        loader.setOnImageLoadedListener(new OnImageLoadedListener() {
                            @Override
                            public void onImageLoaded(Bitmap image) {
                                int index = position;
                                ((ImageView) getListView().getChildAt(position).findViewById(R.id.contacto_imagen)).setImageBitmap(image);
                            }
                        });
                        loader.execute(profileURL);
                    } else {
                        contactImage.setImageResource(R.drawable.profile_default);
                    }
                }

            }
            return applyRowAnimation(v);

        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Fragment enviarMensaje = new SendMessageFragment();
            Bundle arguments = new Bundle();
            arguments.putString("touserid", contacts.get(position).getId());
            enviarMensaje.setArguments(arguments);
            ((MainActivity)getActivity()).replaceFragment(enviarMensaje,FragmentTransaction.TRANSIT_FRAGMENT_OPEN,true);


        }


        @Override
        public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
            view.setBackgroundColor(getResources().getColor(R.color.selected));
            setSelectedContact(position);
            return true;
        }
    }
}
