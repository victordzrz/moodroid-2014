package es.ubu.tfg.moodroid.model;

import android.util.Log;

import java.io.Serializable;

/**
 * Clase para representar un módulo de un curso de Moodle.
 * @author Víctor Díez Rodríguez
 * @version 1.0
 *
 */
public class Module implements Serializable {
    /**
     * URL del icono del módulo.
     */
    private String imgurl;
    /**
     * Tipo de módulo.
     */
    private String type;
    /**
     * URL del módulo.
     */
    private String url;
    /**
     * Nombre del módulo.
     */
    private String name;

    /**
     * @param type tipo del módulo.
     * @param url URL del módulo.
     * @param name nombre del módulo.
     * @param imgurl URL del icono del módulo.
     */
    public Module(String type, String url, String name, String imgurl){
        this.type=type;
        this.url=url;
        this.name=name;
        this.imgurl=imgurl;
        Log.i("Modulo", name);
        Log.i("Modulo", type);
    }
    public String getType() {
        return type;
    }

    public String getUrl() {
        return url;
    }

    public String getName() {
        return name;
    }

    public String getImgurl() {return imgurl; }

}