package es.ubu.tfg.moodroid.view.calendar;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TimePicker;

import java.util.Calendar;
import java.util.GregorianCalendar;

import es.ubu.tfg.moodroid.R;

/**
 * Clase que implementa un {@link android.support.v4.app.DialogFragment} que permite seleccionar una
 * hora.
 *
 * @version 1.0
 * @author Víctor Díez Rodríguez
 */
public class TimePickerFragment extends DialogFragment implements View.OnClickListener{
    /**
     * Seleccionador de hora.
     */
    private TimePicker timePicker;
    /**
     * Callback que se llama cuando se sale del {@link android.support.v4.app.DialogFragment}
     */
    private OnFinishDialogListener listener;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.timepicker_fragment, container);
        timePicker = (TimePicker) view.findViewById(R.id.dialog_timepicker);
        getDialog().setTitle(getResources().getString(R.string.time));
        Button btnListo=(Button)view.findViewById(R.id.dialog_timepicker_button);
        btnListo.setOnClickListener(this);
        return view;
    }

    public void setOnFinishDialogListener(OnFinishDialogListener listener){
        this.listener=listener;
    }

    @Override
    public void onClick(View v) {
        if (listener != null) {
            Calendar time = new GregorianCalendar();
            time.set(Calendar.HOUR,timePicker.getCurrentHour());
            time.set(Calendar.MINUTE,timePicker.getCurrentMinute());
            Log.e("HoraHastaTime", time.toString());
            listener.onFinishDialog((int)(time.getTimeInMillis()/1000));

        }
        this.dismiss();
    }
}
