package es.ubu.tfg.moodroid.test.model.parser;

import android.test.InstrumentationTestCase;

import java.util.ArrayList;

import es.ubu.tfg.moodroid.model.Contact;
import es.ubu.tfg.moodroid.model.parser.ContactListParser;

/**
 * Clase de test para ContactListParser
 */
public class ContactListParserTest extends InstrumentationTestCase {

    public void testGetInstance(){
        ContactListParser parser= ContactListParser.getInstance();
        final int expected=parser.hashCode();
        assertEquals(expected,ContactListParser.getInstance().hashCode());
    }

    public void testParse(){
        String entrada1="{\"online\":[],\"offline\":[{\"id\":6,\"fullname\":\"Test course user 1\",\"unread\":0},{\"id\":8,\"fullname\":\"Test course user 3\",\"unread\":0},{\"id\":10,\"fullname\":\"Test course user 5\",\"unread\":0},{\"id\":11,\"fullname\":\"Test course user 6\",\"unread\":0},{\"id\":12,\"fullname\":\"Test course user 7\",\"unread\":0},{\"id\":13,\"fullname\":\"Test course user 8\",\"unread\":0},{\"id\":14,\"fullname\":\"Test course user 9\",\"unread\":0},{\"id\":15,\"fullname\":\"Test course user 10\",\"unread\":0},{\"id\":9,\"fullname\":\"Test course user 4\",\"unread\":0},{\"id\":7,\"fullname\":\"Test course user 2\",\"unread\":0}],\"strangers\":[]}";
        ContactListParser parser= ContactListParser.getInstance();
        ArrayList<Contact> lista=parser.parseJSON(entrada1);

        int moduleCount=0;
        //10 contactos
        assertEquals(10,lista.size());
        for(int i=0;i<10;i++) {
            assertEquals(lista.get(i).getFullname().startsWith("Test course user"), true);
            assertEquals(lista.get(i).getUnreadMessages(),0);
        }
    }
}
