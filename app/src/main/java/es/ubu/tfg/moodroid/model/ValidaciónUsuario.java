package es.ubu.tfg.moodroid.model;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
/**
 * Clase que implementa GestorConexión.
 * @author Alvar Alonso Álvarez
 * @version 1.0
 */
public class ValidaciónUsuario {
	/**
	 * Se conecta al servidor como usuario y devuelve su clave personal.
	 * @param usuario El nombre del usuario que accede a la aplicación.
	 * @param contraseña La contraseña del usuario que accede a la aplicación.
	 * @param ip La dirección ip del servidor.
	 * @return token si el usuario se ha registrado correctamente al sistema y null al contrario.
	 */
	public String obtenerToken(String usuario, String contraseña, String ip) {
		Log.i("INFO", "Conectado con " + usuario + " y " + contraseña + "en" + GestorPOST.DOMINIO);
		HttpPost httpPost = crearHttpPost(usuario, contraseña, ip);
		if (httpPost != null) {
			ConectorServidor conectorServidor = new ConectorServidor();
			conectorServidor.execute(httpPost);

			try {
				String json = conectorServidor.get();
				Log.i("CONNECT", "Respuesta: " + json);
				if (json != null) {
					JSONObject jObj = new JSONObject(json);
					try {
						return jObj.getString("token");
					} catch (JSONException e) {
						return null;
					}
				} else {
					Log.e("ERROR", "No se obtuvo respuesta del servidor.");
				}
			} catch (Exception e) {
				Log.e("ERROR", "No se obtuvo el httpEntity.");
			}
		}
		return null;
	}
	/**
	 * Se crea la petición al conectarse el usuario con sus datos.
	 * @param usuario El nombre del usuario que accede a la aplicación.
	 * @param contraseña La contraseña del usuario que accede a la aplicación.
	 * @param ip La dirección ip del servidor.
	 * @return El post creado si el usuario se ha registrado correctamente o null al contrario.
	 */
	public HttpPost crearHttpPost(String usuario, String contraseña, String ip) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();

		if (noEstánVacíos(usuario, contraseña, ip)) {
			params.add(new BasicNameValuePair("username", usuario));
			params.add(new BasicNameValuePair("password", contraseña));
			params.add(new BasicNameValuePair("url", ip));
			params.add(new BasicNameValuePair("service", "moodroid"));

			HttpPost httpPost = new HttpPost("http://" + ip + "/login/token.php");
			try {
				httpPost.setEntity(new UrlEncodedFormEntity(params));
				Log.i("INFO", "Post creado correctamente.");
				return httpPost;
			} catch (UnsupportedEncodingException e) {
				Log.e("ERROR", "Error al crear el Post.");
				return null;
			}
		}
		Log.e("ERROR", "Credenciales err�neos, no se crear� el Post.");
		return null;
	}
	/**
	 * Comprueba si los campos requeridos para la inserción del usuario a la aplicacion no están vacíos.
	 * @param username El nombre del usuario que accede a la aplicación.
	 * @param password La contraseña del usuario que accede a la aplicación.
	 * @param ip La dirección ip del servidor.
	 * @return si están vacíos.
	 */
	private boolean noEstánVacíos(String username ,String password, String ip ){
		return !username.equals("") || !password.equals("") || !ip.equals(ip);
	} 
	/**
	 * Se conecta con el servidor de moodle.
	 */
	public class ConectorServidor extends AsyncTask<HttpPost, String, String> {
	
		/**
		 * Realiza la petición al servidor para recibir el token del usuario.
		 * @param httpPost el post para realizar la conexión.
		 * @return El contenido recibido si se ha realizado la petición o null si no se ha realizado.
		 */
		@Override
		protected String doInBackground(HttpPost... httpPost) {
			try {
				DefaultHttpClient httpClient = new DefaultHttpClient();
				if (httpPost[0] != null) {
					Log.i("CONNECT", "Se realizar� la petici�n.");
					HttpResponse httpResponse = httpClient.execute(httpPost[0]);
					Log.i("CONNECT", "Respuesta recibida.");
					HttpEntity httpEntity;
					if ((httpEntity = httpResponse.getEntity()) != null) {
						InputStream is = httpEntity.getContent();
						Log.i("CONNECT", "Modulo recibido.");
						BufferedReader reader = new BufferedReader(new InputStreamReader(is));
						StringBuilder sb = new StringBuilder();
						String line = null;
						while ((line = reader.readLine()) != null) {
							sb.append(line + "\n");
							Log.i("CONNECT", line);
						}
						is.close();
						Log.i("CONNECT", "Stream recibido.");
						return sb.toString();
					} else {
						Log.e("ERROR", "HttpEntity vac�a.");
						return null;
					}
				} else {
					Log.e("ERROR", "HttpPost vac�o.");
					return null;
				}
			} catch (Exception e) {
				Log.e("ERROR", "No se pudo realizar la petici�n del token.");
				return null;
			}
		}
	}
}
