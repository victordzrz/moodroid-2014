package es.ubu.tfg.moodroid.view.groups;

import java.io.Serializable;

/**
 * Enumeración de posible acciones sobre una lista de miembros de un grupo.
 * @author Víctor Díez Rodríguez
 * @version 1.0
 */
public enum MemberAction implements Serializable {
    ADD,DELETE,VIEW
}
