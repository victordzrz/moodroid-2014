package es.ubu.tfg.moodroid.model.parser;

/**
 * Clase que devuelve una cadena JSON igual que la recibe.
 * @author Víctor Díez Rodríguez
 * @version 1.0
 *
 *
 */
public class SimpleParser implements MoodleJSONParser<String> {

    private static SimpleParser instance;

    private SimpleParser(){}

    /**
     * Obtiene una instancia del parser.
     * @return instancia del parser
     */
    public static SimpleParser getInstance(){
        if(instance==null){
            instance=new SimpleParser();
        }
        return instance;
    }

    /**
     * Parsea la cadena JSON
     *
     * @param jsonString cadena a parsear
     * @return misma cadena.
     */
    @Override
    public String parseJSON(String jsonString) {
        if(jsonString!=null) {
            return jsonString;
        }
        else{
            return "";
        }
    }
}
