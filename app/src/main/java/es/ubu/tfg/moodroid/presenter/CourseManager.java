package es.ubu.tfg.moodroid.presenter;

import java.util.ArrayList;

import es.ubu.tfg.moodroid.model.Course;
import es.ubu.tfg.moodroid.model.EnrolledUser;
import es.ubu.tfg.moodroid.model.parser.CourseContentListParser;
import es.ubu.tfg.moodroid.model.parser.CourseListParser;
import es.ubu.tfg.moodroid.model.parser.EnrolledUserListParser;

/**
 * Clase para gestionar los cursos de Moodle.
 * @author Víctor Díez Rodríguez
 * @version 1.0
 *
 */
public class CourseManager {
    /**
     * Obtiene los cursos de un usuario.
     *
     * @param userID usuario del que se desean obtener los cursos.
     * @param token token de usuario.
     * @param callback callback que se debe ejecutar con el resultado de la función.
     * @return ejecutor que ejecuta la función.
     */
    public static MoodleFunctionExecutor getCourses(String userID, String token, OnFunctionCompletedListener<ArrayList<Course>> callback){
        MoodleFunctionExecutor<ArrayList<Course>> executor=new MoodleFunctionExecutor<ArrayList<Course>>();
        executor.addParams("wstoken",token);
        executor.addParams("userid",userID);
        executor.setOnFunctionCompletedListener(callback);
        executor.setParser(CourseListParser.getInstance());
        executor.execute("core_enrol_get_users_courses");
        return executor;
    }

    /**
     * Obtiene los contenidos de un curso.
     *
     * @param courseID id del curso del que se desean obtener los contenidos.
     * @param token token de usuario.
     * @param callback callback que se debe ejecutar con el resultado de la función.
     * @return ejecutor que ejecuta la función.
     */
    public static MoodleFunctionExecutor getCourseContents(String courseID, String token, OnFunctionCompletedListener<ArrayList<Course>> callback){
        MoodleFunctionExecutor<ArrayList<Course>> executor=new MoodleFunctionExecutor<ArrayList<Course>>();
        executor.addParams("wstoken",token);
        executor.addParams("courseid",courseID);
        executor.setOnFunctionCompletedListener(callback);
        executor.setParser(CourseContentListParser.getInstance());
        executor.execute("core_course_get_contents");
        return executor;
    }

    /**
     * Obtiene los participantes de un curso.
     *
     * @param courseID id del curso.
     * @param token token de usuario.
     * @param callback callback que se debe ejecutar con el resultado de la función.
     * @return ejecutor que ejecuta la función.
     */
    public static MoodleFunctionExecutor getEnrolledUsers(String courseID, String token, OnFunctionCompletedListener<ArrayList<EnrolledUser>> callback){
        MoodleFunctionExecutor<ArrayList<EnrolledUser>> executor=new MoodleFunctionExecutor<ArrayList<EnrolledUser>>();
        executor.addParams("wstoken",token);
        executor.addParams("courseid",courseID);
        executor.setOnFunctionCompletedListener(callback);
        executor.setParser(EnrolledUserListParser.getInstance());
        executor.execute("core_enrol_get_enrolled_users");
        return executor;
    }
}
