package es.ubu.tfg.moodroid.view.calendar;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;

import java.util.Calendar;
import java.util.GregorianCalendar;

import es.ubu.tfg.moodroid.R;

/**
 * Clase que implementa un {@link android.support.v4.app.DialogFragment} que permite seleccionar una
 * fecha.
 *
 * @version 1.0
 * @author Víctor Díez Rodríguez
 */
public class DatePickerFragment extends DialogFragment implements View.OnClickListener{
    /**
     * Seleccionador de fecha.
     */
    private DatePicker datePicker;
    /**
     * Callback que se llama cuando se sale del {@link android.support.v4.app.DialogFragment}
     */
    private OnFinishDialogListener listener;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.datepicker_fragment, container);
        datePicker = (DatePicker) view.findViewById(R.id.dialog_datepicker);
        getDialog().setTitle(getResources().getString(R.string.date));
        Button btnListo=(Button)view.findViewById(R.id.dialog_datepicker_button);
        btnListo.setOnClickListener(this);
        return view;
    }

    public void setOnFinishDialogListener(OnFinishDialogListener listener){
        this.listener=listener;
    }

    @Override
    public void onClick(View v) {
        if (listener != null) {
            Calendar date = new GregorianCalendar(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth(), 0, 0,0);
            listener.onFinishDialog((int)(date.getTimeInMillis()/1000));

        }
        this.dismiss();
    }
}
