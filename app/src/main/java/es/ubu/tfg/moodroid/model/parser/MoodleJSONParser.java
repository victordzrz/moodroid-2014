package es.ubu.tfg.moodroid.model.parser;

/**
 * Clase para parsear una cadena JSON en una lista de objetos.
 * @author Víctor Díez Rodríguez
 * @version 1.0
 *
 *
 */
public interface MoodleJSONParser<T> {
    /**
     * Parsea la cadena JSON
     *
     * @param jsonString cadena a parsear
     * @return lista de objetos.
     */
    public T parseJSON(String jsonString);

}
