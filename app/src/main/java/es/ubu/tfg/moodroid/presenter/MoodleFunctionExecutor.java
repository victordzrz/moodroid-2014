package es.ubu.tfg.moodroid.presenter;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import es.ubu.tfg.moodroid.model.GestorPOST;
import es.ubu.tfg.moodroid.model.parser.MoodleJSONParser;
import es.ubu.tfg.moodroid.view.courses.CourseContentFragment;

/**
 * Ejecutor de funciones del servicio web moodroid.
 *
 * @author Víctor Díez Rodríguez
 * @version 1.0
 */
public class MoodleFunctionExecutor<T> extends AsyncTask<String,Void,T> {
    /**
     * Parámetros de la función a ejecutar.
     */
    private HashMap<String,String> functionParams;
    /**
     * Callback para devolver el objeto resultado de ejecutar la función.
     */
    private OnFunctionCompletedListener<T> listener;
    /**
     * Parser para interpretar la cadena JSON obtenida.
     */
    private MoodleJSONParser<T> parser;

    public MoodleFunctionExecutor(){
        super();
        this.functionParams=new HashMap<String, String>();
    }

    /**
     * Ejecuta la función del servicio web moodroid.
     * @param function función a ejecutar.
     * @return objeto resultado de ejecutar la función.
     */
    @Override
    protected T doInBackground(String... function) {
        Log.i("MoodleExecutor","Executing " +function[0]);
     String resultado = null;
        GestorPOST gestorPost = GestorPOST.obtenerGestorPost();

        HttpPost httpPost = new HttpPost(GestorPOST.WSURL);
        List<NameValuePair> params = new ArrayList<NameValuePair>(2);
        for(String name:functionParams.keySet()){
            params.add(new BasicNameValuePair(name,functionParams.get(name)));
        }
        params.add(new BasicNameValuePair("wsfunction", function[0]));
        params.add(new BasicNameValuePair("moodlewsrestformat", "json"));

        try {
            httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
            resultado = gestorPost.obtenerJSONString(httpPost);
        } catch (Exception e) {
            Log.e(CourseContentFragment.class.toString(), "No se ha podido realizar la petici�n POST.");
        }
        return parser.parseJSON(resultado);
    }

    /**
     * Devuelve el objeto resultado a través del callback.
     *
     * @param result objeto de devolver.
     */
    @Override
    protected void onPostExecute(T result){
        if(listener!=null && !isCancelled()) {
            listener.onFunctionCompleted(result);
        }
    }

    /**
     * Añade un parámetro a la función.
     *
     * @param name nombre del parámetro.
     * @param value valor del parámetro.
     */
    public void addParams(String name,String value){
        functionParams.put(name,value);
        Log.i("Executor",name+"="+value);
    }

    /**
     * Establece un callback para devolver la imagen descargada.
     * @param listener
     */
    public void setOnFunctionCompletedListener(OnFunctionCompletedListener<T> listener){
        this.listener=listener;
    }

    /**
     * Establece parser para interpretar la cadena JSON obtenida.
     * @param parser parser para interpretar la cadena JSON obtenida.
     */
    public void setParser(MoodleJSONParser<T> parser){
        this.parser=parser;
    }
}
