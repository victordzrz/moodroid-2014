package es.ubu.tfg.moodroid.view;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import es.ubu.tfg.moodroid.R;
import es.ubu.tfg.moodroid.presenter.ImageLoader;
import es.ubu.tfg.moodroid.presenter.OnImageLoadedListener;
import es.ubu.tfg.moodroid.view.calendar.CalendarFragment;
import es.ubu.tfg.moodroid.view.contacts.ContactsFragment;
import es.ubu.tfg.moodroid.view.courses.CourseListFragment;

/**
 * Pantalla inicial que contiene el menú lateral y el fragment principal.
 *
 * @author Víctor Díez Rodríguez
 * @version 2.0
 */
public class MainActivity extends ActionBarActivity {
    /**
     * La cadena token que es el identificador del usuario conectado.
     */
    private String token;
    /**
     * Menú lateral.
     */
    private DrawerLayout drawer;
    /**
     *  Control del menú lateral.
     */
    private ActionBarDrawerToggle drawerToggle;
    /**
     * Lista de contenidos del menú lateral.
     */
    private ListView drawerList;
    /**
     * Elementos del menú lateral.
     */
    private String[] menuElements;
    /**
     * Fragment que se está mostrando actualmente.
     */
    private Fragment currentFragment;
    /**
     * Imágen de perfil del usuario registrado.
     */
    private Bitmap profileImage;

    /**
     * Crea la vista de la Activity y configura el menú lateral.
     *
     * @param savedInstanceState la configuración inicial de la actividad.
     * @see android.support.v7.app.ActionBarActivity#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        menuElements= new String[] {"profile",
                getResources().getString(R.string.courses),
                getResources().getString(R.string.upload_file),
                getResources().getString(R.string.contacts),
                getResources().getString(R.string.calendar),
                getResources().getString(R.string.web)};

        setupDrawer();


        if (savedInstanceState == null) {
            Fragment fragment = new CourseListFragment();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.fragment_container, fragment);
            transaction.commit();
        }
        Log.e("MainActivity", "BackStackEntryCount: " + getSupportFragmentManager().getBackStackEntryCount());

    }

    /**
     * Recoge las pulsaciones de teclas y reacciona en consecuencia.
     *
     * @param keyCode el valor de la tecla.
     * @param event   descripción del evento
     * @return si se ha consumido el evento.
     * @see android.support.v7.app.ActionBarActivity#onKeyDown(int, android.view.KeyEvent)
     */
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    /**
     * Configura el NavigationDrawer
     */
    private void setupDrawer() {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerList = (ListView) findViewById(R.id.left_drawer);

        drawerList.setAdapter(new MenuArrayAdapter(this, menuElements));
        drawerList.setOnItemClickListener(new DrawerItemClickListener(this));

        drawerToggle = new ActionBarDrawerToggle(this, drawer, R.drawable.ic_drawer,
                R.string.abrirMenu, R.string.cerrarMenu);
        drawerToggle.setDrawerIndicatorEnabled(true);
        drawer.setDrawerListener(drawerToggle);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setActionBarArrowDependingOnFragmentsBackStack();
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                setActionBarArrowDependingOnFragmentsBackStack();
            }
        });

        drawer.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

    }

    /**
     * Muestra o oculta la flecha del ActionBar dependiendo del número de Fragments en el backStack.
     */
    private void setActionBarArrowDependingOnFragmentsBackStack() {
        int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();
        boolean shouldEnableDrawerIndicator = backStackEntryCount == 0;
        drawerToggle.setDrawerIndicatorEnabled(shouldEnableDrawerIndicator);
    }

    /**
     *  Cierra la sesión de Moodle y sale de la aplicación.
     */
    private void closeMoodleSession() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove("url");
        editor.remove("username");
        editor.remove("password");
        editor.remove("token");
        editor.commit();
        finish();
    }

    /**
     * Se sincroniza el estado del DrawerToggle.
     * @see android.support.v7.app.ActionBarActivity#onPostCreate(android.os.Bundle)
     * @param savedInstanceState
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    /**
     * Recoge los cambios en la configuración del ActionBar.
     * @see android.support.v7.app.ActionBarActivity#onConfigurationChanged(android.content.res.Configuration)
     * @param newConfig
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }


    /**
     * Recoge las pulsaciones de teclas al soltarlas y reacciona en consecuencia.
     *
     * @param keyCode el valor de la tecla.
     * @param event   descripción del evento
     * @return si se ha consumido el evento.
     * @see android.support.v7.app.ActionBarActivity#onKeyUp(int, android.view.KeyEvent)
     */
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            boolean consumido = false;
            if (currentFragment != null && currentFragment instanceof MoodroidPopulableUI) {
                MoodroidPopulableUI uiFragment = (MoodroidPopulableUI) currentFragment;
                consumido |= uiFragment.onBackPressed();
            }
            if (!consumido) {
                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getSupportFragmentManager().popBackStack();
                    return true;
                }
            }
        }
        return super.onKeyUp(keyCode, event);
    }

    /**
     * Recoge las pulsaciones en el menú.
     *
     * @param item elemento del menú pulsado.
     * @return  si se ha consumido la pulsación.
     *
     * @see android.support.v7.app.ActionBarActivity#onOptionsItemSelected(android.view.MenuItem)
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (drawerToggle.isDrawerIndicatorEnabled() && drawerToggle.onOptionsItemSelected(item)) {
            return true;
        } else if (item.getItemId() == android.R.id.home && getSupportFragmentManager().popBackStackImmediate()) {
            return true;
        } else if (item.getItemId() == R.id.action_exit) {
            closeMoodleSession();
            return true;
        } else{
            return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Crea el las opciones del menú a partir de un recurso.
     *
     * @param menu menú en el que incluir las opciones.
     * @return si queremos que el menú sea visible.
     * @see android.support.v7.app.ActionBarActivity#onCreateOptionsMenu(android.view.Menu)
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Registra las pulsaciones del botón "up" del menú.
     *
     * @return si se ha completado la navegación.
     * @see android.support.v7.app.ActionBarActivity#onSupportNavigateUp()
     */
    @Override
    public boolean onSupportNavigateUp() {
        //This method is called when the up button is pressed. Just the pop back stack.
        getSupportFragmentManager().popBackStack();
        return true;
    }

    /**
     * Clase para recoger las pulsaciones en el menú lateral.
     */
    private class DrawerItemClickListener implements AdapterView.OnItemClickListener {
        private Context context;

        public DrawerItemClickListener(Context context) {
            super();
            this.context = context;
        }


        /**
         * @see android.widget.AdapterView.OnItemClickListener
         */
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            Fragment fragment = null;
            switch (i) {
                case 1:
                    //Cursos
                    fragment = new CourseListFragment();
                    break;
                case 2:
                    //Subida
                    Intent subida = new Intent(context, Subida.class);
                    startActivity(subida);
                    break;
                case 3:
                    //Contactos
                    fragment = new ContactsFragment();
                    break;
                case 4:
                    //Calendario
                    fragment=new CalendarFragment();
                    break;
                case 5:
                    //Web
                    SharedPreferences sp=PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://"+sp.getString("url","")));
                    startActivity(intent);
                    break;
                default:
                    break;
            }
            if (fragment != null) {
                getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                replaceFragment(fragment,FragmentTransaction.TRANSIT_FRAGMENT_FADE,false);
            }
            drawer.closeDrawer(drawerList);

        }
    }

    /**
     * Reemplaza el fragmento actual con otro.
     *
     * @param fragment fragmento que va a reemplazar al actual.
     * @param transition transición al reemplazar al fragment,
     * @param addToBackStack si se debe añadir al backstack o no.
     */
    public void replaceFragment(Fragment fragment,int transition,boolean addToBackStack){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, fragment).setTransition(transition);
        if(addToBackStack){
            transaction.addToBackStack(null);
        }
        transaction.commit();
        currentFragment = fragment;

    }

    /**
     * Adaptador para el menú lateral.
     */
    private class MenuArrayAdapter extends ArrayAdapter<String> {
        String[] elements;

        /**
         * Constructor con los elementos del menú.
         * @param context contexto de la app.
         * @param elements elementos del menú.
         */
        public MenuArrayAdapter(Context context, String elements[]) {
            super(context, R.layout.list_menuitem, elements);
            this.elements = elements;

        }

        /**
         * @see android.widget.ArrayAdapter#getView(int, android.view.View, android.view.ViewGroup)
         */
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if(position==0){
                v=inflater.inflate(R.layout.list_userdata,null);

                TextView nombre=(TextView)v.findViewById(R.id.username);
                TextView site=(TextView)v.findViewById(R.id.sitename);
                final ImageView imageView=(ImageView)v.findViewById(R.id.profile_img);

                SharedPreferences sharedPreferences= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                nombre.setText(sharedPreferences.getString("firstname","")+" "+ sharedPreferences.getString("lastname",""));
                site.setText(sharedPreferences.getString("sitename",""));

                if(profileImage==null){
                    ImageLoader loader=new ImageLoader();
                    loader.setOnImageLoadedListener(new OnImageLoadedListener() {
                        @Override
                        public void onImageLoaded(Bitmap image) {
                            profileImage=image;
                            imageView.setImageBitmap(image);
                        }
                    });
                    loader.execute(sharedPreferences.getString("userpictureurl",""));
                }
                else{
                    imageView.setImageBitmap(profileImage);
                }

            }else {

                    v = inflater.inflate(R.layout.list_menuitem, null);


                TextView texto = (TextView) v.findViewById(R.id.text_menuitem);
                ImageView icono = (ImageView) v.findViewById(R.id.icon_menuitem);

                if (texto != null) {
                    texto.setText(elements[position]);
                }

                if (icono != null) {
                    int drawable;
                    switch (position) {
                        case 1:
                            drawable = R.drawable.ic_action_sort_by_size;
                            break;
                        case 2:
                            drawable = R.drawable.ic_action_upload;
                            break;
                        case 3:
                            drawable = R.drawable.ic_action_group;
                            break;
                        case 4:
                            drawable = R.drawable.ic_action_go_to_today;
                            break;
                        case 5:
                            drawable = android.R.drawable.ic_menu_mapmode;
                            break;
                        default:
                            drawable = android.R.drawable.ic_media_play;
                    }
                    icono.setImageResource(drawable);
                }
            }

            return v;
        }
    }
}



