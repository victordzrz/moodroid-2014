package es.ubu.tfg.moodroid.model;

import java.io.Serializable;

/**
 * Clase para representar un evento de Moodle.
 * @author Víctor Díez Rodríguez
 * @version 1.0
 *
 */
public class Event implements Serializable {
    /**
     * id del evento.
     */
    private int id;
    /**
     * Nombre del evento.
     */
    private String name;
    /**
     * Descripción del evento.
     */
    private String description;
    /**
     * id del curso al que pertenece el evento.
     */
    private int courseid;
    /**
     * Tipo del módulo del evento.
     */
    private String moduleName;
    /**
     * Hora a la que empieza el evento en segundos.
     */
    private int timeStart;
    /**
     * Segundos de duración del evento.
     */
    private int duration;

    public Event(int id, String name){
        this.id=id;
        this.name=name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCourseid() {
        return courseid;
    }

    public void setCourseid(int courseid) {
        this.courseid = courseid;
    }

    public int getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(int timeStart) {
        this.timeStart = timeStart;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
}
