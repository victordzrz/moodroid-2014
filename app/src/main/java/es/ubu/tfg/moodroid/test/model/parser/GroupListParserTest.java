package es.ubu.tfg.moodroid.test.model.parser;

import android.test.InstrumentationTestCase;

import java.util.ArrayList;

import es.ubu.tfg.moodroid.model.Group;
import es.ubu.tfg.moodroid.model.parser.GroupListParser;

/**
 * Clase de test para GroupListParser
 */
public class GroupListParserTest extends InstrumentationTestCase {

    public void testGetInstance(){
        GroupListParser parser= GroupListParser.getInstance();
        final int expected=parser.hashCode();
        assertEquals(expected, GroupListParser.getInstance().hashCode());
    }

    public void testParse(){
        String entrada1="[{\"id\":40,\"courseid\":5,\"name\":\"Grupo 1\",\"description\":\"\",\"descriptionformat\":1,\"enrolmentkey\":null},{\"id\":49,\"courseid\":5,\"name\":\"Grupo 10\",\"description\":\"\",\"descriptionformat\":1,\"enrolmentkey\":null},{\"id\":50,\"courseid\":5,\"name\":\"Grupo 11\",\"description\":\"\",\"descriptionformat\":1,\"enrolmentkey\":null},{\"id\":51,\"courseid\":5,\"name\":\"Grupo 12\",\"description\":\"\",\"descriptionformat\":1,\"enrolmentkey\":null},{\"id\":52,\"courseid\":5,\"name\":\"Grupo 13\",\"description\":\"\",\"descriptionformat\":1,\"enrolmentkey\":null},{\"id\":53,\"courseid\":5,\"name\":\"Grupo 14\",\"description\":\"\",\"descriptionformat\":1,\"enrolmentkey\":null},{\"id\":54,\"courseid\":5,\"name\":\"Grupo 15\",\"description\":\"\",\"descriptionformat\":1,\"enrolmentkey\":null},{\"id\":55,\"courseid\":5,\"name\":\"Grupo 16\",\"description\":\"\",\"descriptionformat\":1,\"enrolmentkey\":null},{\"id\":56,\"courseid\":5,\"name\":\"Grupo 17\",\"description\":\"\",\"descriptionformat\":1,\"enrolmentkey\":null},{\"id\":57,\"courseid\":5,\"name\":\"Grupo 18\",\"description\":\"\",\"descriptionformat\":1,\"enrolmentkey\":null},{\"id\":58,\"courseid\":5,\"name\":\"Grupo 19\",\"description\":\"\",\"descriptionformat\":1,\"enrolmentkey\":null},{\"id\":41,\"courseid\":5,\"name\":\"Grupo 2\",\"description\":\"\",\"descriptionformat\":1,\"enrolmentkey\":null},{\"id\":59,\"courseid\":5,\"name\":\"Grupo 20\",\"description\":\"\",\"descriptionformat\":1,\"enrolmentkey\":null},{\"id\":60,\"courseid\":5,\"name\":\"Grupo 21\",\"description\":\"\",\"descriptionformat\":1,\"enrolmentkey\":null},{\"id\":61,\"courseid\":5,\"name\":\"Grupo 22\",\"description\":\"\",\"descriptionformat\":1,\"enrolmentkey\":null},{\"id\":62,\"courseid\":5,\"name\":\"Grupo 23\",\"description\":\"\",\"descriptionformat\":1,\"enrolmentkey\":null},{\"id\":63,\"courseid\":5,\"name\":\"Grupo 24\",\"description\":\"\",\"descriptionformat\":1,\"enrolmentkey\":null},{\"id\":64,\"courseid\":5,\"name\":\"Grupo 25\",\"description\":\"\",\"descriptionformat\":1,\"enrolmentkey\":null},{\"id\":42,\"courseid\":5,\"name\":\"Grupo 3\",\"description\":\"\",\"descriptionformat\":1,\"enrolmentkey\":null},{\"id\":43,\"courseid\":5,\"name\":\"Grupo 4\",\"description\":\"\",\"descriptionformat\":1,\"enrolmentkey\":null},{\"id\":44,\"courseid\":5,\"name\":\"Grupo 5\",\"description\":\"\",\"descriptionformat\":1,\"enrolmentkey\":null},{\"id\":45,\"courseid\":5,\"name\":\"Grupo 6\",\"description\":\"\",\"descriptionformat\":1,\"enrolmentkey\":null},{\"id\":46,\"courseid\":5,\"name\":\"Grupo 7\",\"description\":\"\",\"descriptionformat\":1,\"enrolmentkey\":null},{\"id\":47,\"courseid\":5,\"name\":\"Grupo 8\",\"description\":\"\",\"descriptionformat\":1,\"enrolmentkey\":null},{\"id\":48,\"courseid\":5,\"name\":\"Grupo 9\",\"description\":\"\",\"descriptionformat\":1,\"enrolmentkey\":null}]";
        GroupListParser parser= GroupListParser.getInstance();
        ArrayList<Group> lista=parser.parseJSON(entrada1);

        //25 grupos
        assertEquals(25,lista.size());

        for(int i=1;i<lista.size();i++){
            assertTrue(lista.get(i).getName().startsWith("Grupo"));
            assertEquals(5,lista.get(i).getCourseid());
        }

    }

}


