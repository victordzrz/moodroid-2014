package es.ubu.tfg.moodroid.model;

import java.io.Serializable;

/**
 * Clase para representar un contato de Moodle.
 * @author Víctor Díez Rodríguez
 * @version 1.0
 *
 */
public class Contact implements Serializable{
    /**
     * El contacto está online.
     */
    public static final String STATUS_ONLINE="Online";
    /**
     * El contacto está offline.
     */
    public static final String STATUS_OFFLINE="Offline";
    /**
     * El contacto es un extraño.
     */
    public static final String STATUS_STRANGER="Extraño";

    /**
     * id del contacto.
     */
    private String id;
    /**
     * Nombre completo del contacto.
     */
    private String fullname;
    /**
     * URL de la imagen de perfil.
     */
    private String profileimageurl;
    /**
     * Estado del contacto.
     */
    private String status;

    /**
     * Número de mensajes sin leer.
     */
    private int unreadMessages;

    public Contact(String id){
        this.id=id;
    }

    public String getId() {
        return id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getProfileimageurl() {
        return profileimageurl;
    }

    public void setProfileimageurl(String profileimageurl) {
        this.profileimageurl = profileimageurl;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getUnreadMessages() {
        return unreadMessages;
    }

    public void setUnreadMessages(int unread_messages) {
        this.unreadMessages = unread_messages;
    }
}
