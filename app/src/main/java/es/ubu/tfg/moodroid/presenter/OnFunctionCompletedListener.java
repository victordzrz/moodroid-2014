package es.ubu.tfg.moodroid.presenter;

/**
 * Interfaz para devolver el resultado de una función.
 */
public interface OnFunctionCompletedListener<T> {

    /**
     * Devuelve el resultado de una función cuando esta se completa.
     * @param result resultado de la función.
     */
    public void onFunctionCompleted(T result);

}
