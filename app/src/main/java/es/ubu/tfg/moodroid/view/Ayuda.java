package es.ubu.tfg.moodroid.view;

import android.app.Activity;
import android.os.Bundle;

import es.ubu.tfg.moodroid.R;

/**
 * Clase que implementa la Ayuda.
 * @author Alvar Alonso �lvarez
 * @version 1.0
 */
public class Ayuda extends Activity {	
	/**
	 * Crea la interfaz de la clase Ayuda, mostranto un texto descriptivo para facilitar al usuario la comprensi�n de la aplicaci�n.
	 * @param savedInstanceState la configuraci�n inicial de la actividad.
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.texto_ayuda);
	}
}
