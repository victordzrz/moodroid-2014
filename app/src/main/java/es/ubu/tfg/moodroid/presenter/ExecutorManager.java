package es.ubu.tfg.moodroid.presenter;

import java.util.HashSet;

/**
 * Clase para gestionar los ejecutores de funciones de Moodle.
 *
 * Permite cancelar un ejecutor antes de que termine.
 * @author Víctor Díez Rodríguez
 * @version 1.0
 *
 */
public class ExecutorManager {
    /**
     * Lista de ejecutores activos.
     */
    private HashSet<MoodleFunctionExecutor> executorList;
    /**
     * Instancia singleton.
     */
    private static  ExecutorManager instance;

    private ExecutorManager(){
        executorList=new HashSet<MoodleFunctionExecutor>();
    }

    /**
     * Obtiene una instancia del gestor.
     * @return instancia del gestor,
     */
    public static ExecutorManager getInstance(){
        if(instance==null){
            instance=new ExecutorManager();
        }
        return instance;
    }

    /**
     * Añade un ejecutor activo.
     * @param executor ejecutor a añadir.
     */
    public void addExecutor(MoodleFunctionExecutor executor){
        executorList.add(executor);
    }

    /**
     * Cancela un ejecutor activo.
     * @param executor ejecutor a cancelar.
     */
    public void cancelExecutor(MoodleFunctionExecutor executor){
        executor.cancel(false);
        executorList.remove(executor);
    }

    /**
     * Cancela todos los ejecutores de la lista.
     */
    public void cancelAll(){
        for(MoodleFunctionExecutor executor:executorList){
            executor.cancel(false);
        }
        executorList.clear();
    }
}
