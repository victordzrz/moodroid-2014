package es.ubu.tfg.moodroid.view.groups;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashSet;

import es.ubu.tfg.moodroid.R;
import es.ubu.tfg.moodroid.presenter.CourseManager;
import es.ubu.tfg.moodroid.presenter.GroupManager;
import es.ubu.tfg.moodroid.presenter.MoodleFunctionExecutor;
import es.ubu.tfg.moodroid.presenter.OnFunctionCompletedListener;
import es.ubu.tfg.moodroid.model.Group;
import es.ubu.tfg.moodroid.model.EnrolledUser;
import es.ubu.tfg.moodroid.view.FadeInArrayAdapter;
import es.ubu.tfg.moodroid.view.MoodroidPopulableUI;

/**
 * Clase que implementa la lista para mostrar y seleccionar miembros de un grupo.
 *
 * @author Víctor Díez Rodríguez
 * @version 1.0
 */
public class MemberListFragment extends MoodroidPopulableUI<ArrayList<EnrolledUser>> {
    private ArrayList<EnrolledUser> members;
    private String groupid;
    private String courseid;
    private MemberAction action;
    private HashSet<Integer> selectedMembers=new HashSet<Integer>();


    @Override
    protected void populateInterface(ArrayList<EnrolledUser> objects, View rootView) {
        switch (action){
            case ADD:
                members = getMembersOutGroup(Integer.valueOf(groupid), objects);
                break;
            case VIEW:
            case DELETE:
                members = getMembersInGroup(Integer.valueOf(groupid), objects);
                break;
        }
        setListAdapter(new MemberArrayAdapter(getActivity().getApplicationContext(),
                members));

        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CheckBox checkBox=(CheckBox)view.findViewById(R.id.miembro_checkbox);
                checkBox.toggle();
                if(selectedMembers.contains(position)){
                    selectedMembers.remove(position);
                }else
                {
                    selectedMembers.add(position);
                }
            }
        });

    }

    @Override
    protected MoodleFunctionExecutor loadContent(OnFunctionCompletedListener<ArrayList<EnrolledUser>> callback) {
        String token= PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext()).getString("token","invalid_token");

        return CourseManager.getEnrolledUsers(courseid, token, callback);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        groupid=getArguments().getString("groupid");
        courseid=getArguments().getString("courseid");
        action=(MemberAction)getArguments().getSerializable("action");
        switch (action){
            case ADD:
            case DELETE:
                setHasOptionsMenu(true);
                break;
            case VIEW:
                setHasOptionsMenu(false);
                break;
        }

        if(savedInstanceState!=null){
            selectedMembers=(HashSet<Integer>)savedInstanceState.getSerializable("selectedMembers");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("selectedMembers",selectedMembers);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitleEnabled(true);
        switch (action){
            case ADD:
                setTitle(getResources().getString(R.string.add_members));
                break;
            case DELETE:
                setTitle(getResources().getString(R.string.delete_members));
                break;
            case VIEW:
                setTitle(getResources().getString(R.string.members));
                break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu,inflater);
        inflater.inflate(R.menu.member_list_menu,menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        SharedPreferences preferences= PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        String token= preferences.getString("token","invalid_token");
        OnFunctionCompletedListener<String> callback;

        if(item.getItemId()==R.id.menu_members_accept){
            switch (action){
                case ADD:
                    callback=new OnFunctionCompletedListener<String>() {
                        @Override
                        public void onFunctionCompleted(String jsonString) {
                            Toast toast=Toast.makeText(getActivity().getApplicationContext(),getResources().getString(R.string.members_added),Toast.LENGTH_SHORT);
                            toast.show();
                            stopProgressDialog();
                            getActivity().getSupportFragmentManager().popBackStack();
                        }
                    };
                    GroupManager.addGroupMembers(getSelectedMembers(), groupid, token, callback);
                    startProgressDialog();
                    break;
                case DELETE:
                    callback=new OnFunctionCompletedListener<String>() {
                        @Override
                        public void onFunctionCompleted(String jsonString) {
                            Toast toast=Toast.makeText(getActivity().getApplicationContext(),getResources().getString(R.string.members_deleted),Toast.LENGTH_SHORT);
                            toast.show();
                            stopProgressDialog();
                            getActivity().getSupportFragmentManager().popBackStack();
                        }
                    };
                    GroupManager.deleteGroupMembers(getSelectedMembers(), groupid, token, callback);
                    startProgressDialog();
                    break;

            }

            return true;
        }
        else{
            return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Obtiene los IDs de los miembros seleccionados.
     * @return
     */
    private ArrayList<String> getSelectedMembers(){
        ArrayList<String> memberIDs=new ArrayList<String>();
        for(Integer i:selectedMembers){
            memberIDs.add(String.valueOf(members.get(i).getId()));
        }
        return memberIDs;
    }

    /**
     * Obtiene los miembros que pertenecen a un grupo.
     * @param groupID grupo del que se desean obtener los miembros.
     * @param enrolledUsers lista completa de participantes del curso.
     * @return miembros que pertenecen al grupo.
     */
    private ArrayList<EnrolledUser> getMembersInGroup(int groupID, ArrayList<EnrolledUser> enrolledUsers){
        ArrayList<EnrolledUser> miembros=new ArrayList<EnrolledUser>();

        for(EnrolledUser enrolledUser : enrolledUsers){
            ArrayList<Group> groups = enrolledUser.getGroups();
            for(Group group : groups){
                if(group.getId()==groupID){
                    miembros.add(enrolledUser);
                    break;
                }
            }
        }

        return miembros;
    }

    /**
     * Obtiene los miembros que no pertenecen a un grupo.
     * @param groupID grupo del que se desean obtener los miembros que no pertenecen.
     * @param enrolledUsers lista completa de participantes del curso.
     * @return miembros que no pertenecen a un grupo.
     */
    private ArrayList<EnrolledUser> getMembersOutGroup(int groupID, ArrayList<EnrolledUser> enrolledUsers){
        ArrayList<EnrolledUser> miembros=new ArrayList<EnrolledUser>();

        for(EnrolledUser enrolledUser : enrolledUsers){
            ArrayList<Group> groups = enrolledUser.getGroups();
            boolean yaEnGrupo=false;
            for(Group group : groups){
                if(group.getId()==groupID){
                    yaEnGrupo=true;
                }
            }
            if(!yaEnGrupo){
                miembros.add(enrolledUser);
            }
        }

        return miembros;
    }

    /**
     * Adaptador de miembros del grupo.
     */
    private class MemberArrayAdapter extends FadeInArrayAdapter<EnrolledUser> {

        /**
         * Lista de miembros del grupo.
         */
        private ArrayList<EnrolledUser> members;

        /**
         * Crea un nuevo adaptador de miembros de un grupo.
         * @param context contexto de la aplicación.
         * @param members lista de miembros del grupo.
         */
        public MemberArrayAdapter(Context context, ArrayList<EnrolledUser> members) {
            super(context, R.layout.list_usercheckbox, members);
            this.members = members;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;

            if (v == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = inflater.inflate(R.layout.list_usercheckbox, null);
            }

            EnrolledUser i = members.get(position);

            if (i != null) {

                TextView nombre = (TextView) v.findViewById(R.id.miembro_nombre);

                if (nombre != null) {
                    nombre.setText(i.getFullname());
                }


            }

            CheckBox checkBox=(CheckBox)v.findViewById(R.id.miembro_checkbox);
            if(action.equals(MemberAction.VIEW)){
                checkBox.setVisibility(View.GONE);
            }

            if(selectedMembers.contains(position)){
                checkBox.setChecked(true);
            }
            else{
                checkBox.setChecked(false);
            }

            return applyRowAnimation(v);

        }
    }
}
