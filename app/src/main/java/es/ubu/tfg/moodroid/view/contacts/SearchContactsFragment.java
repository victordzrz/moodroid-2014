package es.ubu.tfg.moodroid.view.contacts;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import es.ubu.tfg.moodroid.R;
import es.ubu.tfg.moodroid.presenter.ContactManager;
import es.ubu.tfg.moodroid.presenter.ImageLoader;
import es.ubu.tfg.moodroid.presenter.MoodleFunctionExecutor;
import es.ubu.tfg.moodroid.presenter.OnFunctionCompletedListener;
import es.ubu.tfg.moodroid.presenter.OnImageLoadedListener;
import es.ubu.tfg.moodroid.model.Contact;
import es.ubu.tfg.moodroid.view.FadeInArrayAdapter;
import es.ubu.tfg.moodroid.view.MoodroidPopulableUI;

/**
 * Clase que implementa la vista para mostrar la lista de contactos buscados.
 *
 * @author Víctor Díez Rodríguez
 * @version 1.0
 */
public class SearchContactsFragment extends MoodroidPopulableUI<ArrayList<Contact>> {
    /**
     * Cadena que se ha buscado.
     */
    private String searchString;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        searchString =getArguments().getString("searchtext");
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitleEnabled(true);
        setTitle(getResources().getString(R.string.search) + searchString);
    }

    @Override
    protected void populateInterface(ArrayList<Contact> objects, View rootView) {
        ContactSearchArrayAdapter adapter = new ContactSearchArrayAdapter
                (getActivity().getApplicationContext(), objects);
        setListAdapter(adapter);
        getListView().setOnItemClickListener(adapter);
        setEmptyText(getResources().getString(R.string.no_results));


    }




    @Override
    protected MoodleFunctionExecutor loadContent(OnFunctionCompletedListener<ArrayList<Contact>> callback) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        String token = sp.getString("token", "invalid_token");
        return ContactManager.searchContacts(searchString, token, callback);
    }


    /**
     * Adaptador para poblar la lista de contactos encontrados.
     */
    private class ContactSearchArrayAdapter extends FadeInArrayAdapter<Contact> implements AdapterView.OnItemClickListener {
        /**
         * Contactos encontrados.
         */
        private ArrayList<Contact> contacts;

        /**
         * Crea un nuevo adaptador de contactos encontrados.
         * @param context contexto de la aplicación.
         * @param contacts contactos con los que poblar.
         */
        public ContactSearchArrayAdapter(Context context, ArrayList<Contact> contacts) {
            super(context, R.layout.list_searchcontactitem, contacts);
            this.contacts = contacts;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View v = convertView;

            if (v == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = inflater.inflate(R.layout.list_searchcontactitem, null);
            }

            Contact c = contacts.get(position);

            if (c != null) {

                TextView contactName = (TextView) v.findViewById(R.id.busquedacontacto_nombre);
                final ImageView contactImage = (ImageView) v.findViewById(R.id.busquedacontacto_imagen);


                if (contactName != null) {
                    contactName.setText(c.getFullname());
                }

                if (contactImage != null) {
                    String profileURL = c.getProfileimageurl();
                    if (profileURL != null) {
                        ImageLoader loader = new ImageLoader();
                        loader.setOnImageLoadedListener(new OnImageLoadedListener() {
                            @Override
                            public void onImageLoaded(Bitmap image) {
                                int index = position;
                                ((ImageView) getListView().getChildAt(position).findViewById(R.id.busquedacontacto_imagen)).setImageBitmap(image);
                            }
                        });
                        loader.execute(profileURL);
                    } else {
                        contactImage.setImageResource(R.drawable.profile_default);
                    }
                }

            }
            return applyRowAnimation(v);

        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final Contact contactClicked = contacts.get(position);

            String token = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext())
                    .getString("token", "invalid_token");
            OnFunctionCompletedListener<String> callback=new OnFunctionCompletedListener<String>() {
                @Override
                public void onFunctionCompleted(String jsonString) {
                    if (jsonString.equals("[]")) {
                        Toast toast = Toast.makeText(getActivity(), contactClicked.getFullname() + getResources().getString(R.string.added_contact), Toast.LENGTH_SHORT);
                        toast.show();
                    }
                }
            };
            ContactManager.addContact(contactClicked.getId(), token, callback);
        }
    }
}
