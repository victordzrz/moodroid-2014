package es.ubu.tfg.moodroid.view.contacts;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import es.ubu.tfg.moodroid.R;
import es.ubu.tfg.moodroid.presenter.ContactManager;
import es.ubu.tfg.moodroid.presenter.ExecutorManager;
import es.ubu.tfg.moodroid.presenter.OnFunctionCompletedListener;
import es.ubu.tfg.moodroid.view.LoadingFragment;
import es.ubu.tfg.moodroid.view.TitleFragment;

/**
 * Clase que implementa la vista para enviar un mensaje a un contacto.
 */
public class SendMessageFragment extends TitleFragment {
    /**
     * Mensaje a enviar.
     */
    private String message;
    /**
     * id del destinatario.
     */
    private String userID;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        userID = getArguments().getString("touserid");
        if (savedInstanceState != null) {
            message = savedInstanceState.getString("message");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.send_message_layout, container, false);
        EditText texto = (EditText) rootView.findViewById(R.id.enviar_mensaje_texto);
        texto.setText(message);
        texto.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                sendMessage(v.getText().toString());
                return true;
            }
        });

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitleEnabled(true);
        setTitle(getResources().getString(R.string.new_message));
    }

    @Override
    public void onPause() {
        super.onPause();
        ExecutorManager.getInstance().cancelAll();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        EditText texto = (EditText) getView().findViewById(R.id.enviar_mensaje_texto);
        message = texto.getText().toString();
        outState.putString("message", message);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.send_message_menu, menu);
    }

    /**
     * Envía un mensaje al destinatario seleccionado.
     * @param mensaje mensaje que se va a enviar.
     */
    public void sendMessage(String mensaje) {
        final Fragment loading = new LoadingFragment();
        String token = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext())
                .getString("token", "invalid_token");
        OnFunctionCompletedListener callback=new OnFunctionCompletedListener<String>() {
            @Override
            public void onFunctionCompleted(String jsonString) {
                Log.e("EnviarMensaje", jsonString);
                if (jsonString.startsWith("[{\"msgid\"")) {

                    Toast toast = Toast.makeText(getActivity(), getResources().getString(R.string.message_sent), Toast.LENGTH_SHORT);
                    toast.show();
                } else {

                    Toast toast = Toast.makeText(getActivity(), getResources().getString(R.string.error_sending_message), Toast.LENGTH_SHORT);
                    toast.show();

                }
                getActivity().getSupportFragmentManager().beginTransaction().remove(loading).commit();
                getActivity().getSupportFragmentManager().popBackStack();
            }
        };
        ExecutorManager.getInstance().addExecutor(ContactManager.sendMessage(userID, mensaje, token, callback));
        getActivity().getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, loading).commit();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (item.getItemId() == R.id.action_send_message) {
            EditText texto = (EditText) getView().findViewById(R.id.enviar_mensaje_texto);
            sendMessage(texto.getText().toString());
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }
}
