package es.ubu.tfg.moodroid.model.parser;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import es.ubu.tfg.moodroid.model.Contact;

/**
 * Clase para parsear una cadena JSON en una lista de contactos.
 * @author Víctor Díez Rodríguez
 * @version 1.0
 *
 *
 */
public class ContactListParser implements MoodleJSONParser<ArrayList<Contact>> {
    private static ContactListParser instance;

    private ContactListParser(){}

    /**
     * Obtiene una instancia del parser.
     * @return instancia del parser
     */
    public static ContactListParser getInstance(){
        if(instance==null){
            instance=new ContactListParser();
        }
        return instance;
    }

    /**
     * Parsea la cadena JSON
     *
     * @param jsonString cadena a parsear
     * @return lista de contactos.
     */
    @Override
    public ArrayList<Contact> parseJSON(String jsonString) {
        ArrayList<Contact> contacts =new ArrayList<Contact>();
        Contact contact;

        if(jsonString!=null) {
            try {
                JSONObject jsonObject = new JSONObject(jsonString);
                JSONArray online = jsonObject.getJSONArray("online");
                for (int i = 0; i < online.length(); i++) {
                    JSONObject jsonContact = online.getJSONObject(i);
                    contact = new Contact(jsonContact.getString("id"));
                    contact.setFullname(jsonContact.getString("fullname"));
                    try {
                        String image = jsonContact.getString("profileimageurl");
                        contact.setProfileimageurl(image);
                    } catch (JSONException e) {
                        contact.setProfileimageurl(null);
                    }
                    contact.setUnreadMessages(jsonContact.getInt("unread"));
                    contact.setStatus(Contact.STATUS_ONLINE);
                    contacts.add(contact);
                }
                JSONArray offline = jsonObject.getJSONArray("offline");
                for (int i = 0; i < offline.length(); i++) {
                    JSONObject jsonContact = offline.getJSONObject(i);
                    contact = new Contact(jsonContact.getString("id"));
                    contact.setFullname(jsonContact.getString("fullname"));
                    try {
                        String image = jsonContact.getString("profileimageurl");
                        contact.setProfileimageurl(image);
                    } catch (JSONException e) {
                        contact.setProfileimageurl(null);
                    }
                    contact.setUnreadMessages(jsonContact.getInt("unread"));
                    contact.setStatus(Contact.STATUS_OFFLINE);
                    contacts.add(contact);
                }
                JSONArray stranger = jsonObject.getJSONArray("strangers");
                for (int i = 0; i < stranger.length(); i++) {
                    JSONObject jsonContact = stranger.getJSONObject(i);
                    contact = new Contact(jsonContact.getString("id"));
                    contact.setFullname(jsonContact.getString("fullname"));
                    try {
                        String image = jsonContact.getString("profileimageurl");
                        contact.setProfileimageurl(image);
                    } catch (JSONException e) {
                        contact.setProfileimageurl(null);
                    }
                    contact.setUnreadMessages(jsonContact.getInt("unread"));
                    contact.setStatus(Contact.STATUS_STRANGER);
                    contacts.add(contact);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            for (Contact c : contacts) {
                Log.e("Contactos", c.getFullname());
            }
        }
        return contacts;
    }
}
