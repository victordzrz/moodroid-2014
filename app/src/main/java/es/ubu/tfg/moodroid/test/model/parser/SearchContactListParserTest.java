package es.ubu.tfg.moodroid.test.model.parser;

import android.test.InstrumentationTestCase;

import java.util.ArrayList;

import es.ubu.tfg.moodroid.model.Contact;
import es.ubu.tfg.moodroid.model.parser.SearchContactListParser;

/**
 * Clase de test para SearchContactListParser
 */
public class SearchContactListParserTest extends InstrumentationTestCase {

    public void testGetInstance(){
        SearchContactListParser parser= SearchContactListParser.getInstance();
        final int expected=parser.hashCode();
        assertEquals(expected, SearchContactListParser.getInstance().hashCode());
    }

    public void testParse(){
        //Cadena obtenida al buscar "user 1"
        String entrada1="[{\"id\":6,\"fullname\":\"Test course user 1\"},{\"id\":15,\"fullname\":\"Test course user 10\"},{\"id\":16,\"fullname\":\"Test course user 11\"},{\"id\":17,\"fullname\":\"Test course user 12\"},{\"id\":18,\"fullname\":\"Test course user 13\"},{\"id\":19,\"fullname\":\"Test course user 14\"},{\"id\":20,\"fullname\":\"Test course user 15\"},{\"id\":21,\"fullname\":\"Test course user 16\"},{\"id\":22,\"fullname\":\"Test course user 17\"},{\"id\":23,\"fullname\":\"Test course user 18\"},{\"id\":24,\"fullname\":\"Test course user 19\"},{\"id\":105,\"fullname\":\"Test course user 100\"},{\"id\":106,\"fullname\":\"Test course user 101\"},{\"id\":107,\"fullname\":\"Test course user 102\"},{\"id\":108,\"fullname\":\"Test course user 103\"},{\"id\":109,\"fullname\":\"Test course user 104\"},{\"id\":110,\"fullname\":\"Test course user 105\"},{\"id\":111,\"fullname\":\"Test course user 106\"},{\"id\":112,\"fullname\":\"Test course user 107\"},{\"id\":113,\"fullname\":\"Test course user 108\"},{\"id\":114,\"fullname\":\"Test course user 109\"},{\"id\":115,\"fullname\":\"Test course user 110\"},{\"id\":116,\"fullname\":\"Test course user 111\"},{\"id\":117,\"fullname\":\"Test course user 112\"},{\"id\":118,\"fullname\":\"Test course user 113\"},{\"id\":119,\"fullname\":\"Test course user 114\"},{\"id\":120,\"fullname\":\"Test course user 115\"},{\"id\":121,\"fullname\":\"Test course user 116\"},{\"id\":122,\"fullname\":\"Test course user 117\"},{\"id\":123,\"fullname\":\"Test course user 118\"},{\"id\":124,\"fullname\":\"Test course user 119\"},{\"id\":125,\"fullname\":\"Test course user 120\"},{\"id\":126,\"fullname\":\"Test course user 121\"},{\"id\":127,\"fullname\":\"Test course user 122\"},{\"id\":128,\"fullname\":\"Test course user 123\"},{\"id\":129,\"fullname\":\"Test course user 124\"},{\"id\":130,\"fullname\":\"Test course user 125\"},{\"id\":131,\"fullname\":\"Test course user 126\"},{\"id\":132,\"fullname\":\"Test course user 127\"},{\"id\":133,\"fullname\":\"Test course user 128\"},{\"id\":134,\"fullname\":\"Test course user 129\"},{\"id\":135,\"fullname\":\"Test course user 130\"},{\"id\":136,\"fullname\":\"Test course user 131\"},{\"id\":137,\"fullname\":\"Test course user 132\"},{\"id\":138,\"fullname\":\"Test course user 133\"},{\"id\":139,\"fullname\":\"Test course user 134\"},{\"id\":140,\"fullname\":\"Test course user 135\"},{\"id\":141,\"fullname\":\"Test course user 136\"},{\"id\":142,\"fullname\":\"Test course user 137\"},{\"id\":143,\"fullname\":\"Test course user 138\"},{\"id\":144,\"fullname\":\"Test course user 139\"},{\"id\":145,\"fullname\":\"Test course user 140\"},{\"id\":146,\"fullname\":\"Test course user 141\"},{\"id\":147,\"fullname\":\"Test course user 142\"},{\"id\":148,\"fullname\":\"Test course user 143\"},{\"id\":149,\"fullname\":\"Test course user 144\"},{\"id\":150,\"fullname\":\"Test course user 145\"},{\"id\":151,\"fullname\":\"Test course user 146\"},{\"id\":152,\"fullname\":\"Test course user 147\"},{\"id\":153,\"fullname\":\"Test course user 148\"},{\"id\":154,\"fullname\":\"Test course user 149\"},{\"id\":155,\"fullname\":\"Test course user 150\"},{\"id\":156,\"fullname\":\"Test course user 151\"},{\"id\":157,\"fullname\":\"Test course user 152\"},{\"id\":158,\"fullname\":\"Test course user 153\"},{\"id\":159,\"fullname\":\"Test course user 154\"},{\"id\":160,\"fullname\":\"Test course user 155\"},{\"id\":161,\"fullname\":\"Test course user 156\"},{\"id\":162,\"fullname\":\"Test course user 157\"},{\"id\":163,\"fullname\":\"Test course user 158\"},{\"id\":164,\"fullname\":\"Test course user 159\"},{\"id\":165,\"fullname\":\"Test course user 160\"},{\"id\":166,\"fullname\":\"Test course user 161\"},{\"id\":167,\"fullname\":\"Test course user 162\"},{\"id\":168,\"fullname\":\"Test course user 163\"},{\"id\":169,\"fullname\":\"Test course user 164\"},{\"id\":170,\"fullname\":\"Test course user 165\"},{\"id\":171,\"fullname\":\"Test course user 166\"},{\"id\":172,\"fullname\":\"Test course user 167\"},{\"id\":173,\"fullname\":\"Test course user 168\"},{\"id\":174,\"fullname\":\"Test course user 169\"},{\"id\":175,\"fullname\":\"Test course user 170\"},{\"id\":176,\"fullname\":\"Test course user 171\"},{\"id\":177,\"fullname\":\"Test course user 172\"},{\"id\":178,\"fullname\":\"Test course user 173\"},{\"id\":179,\"fullname\":\"Test course user 174\"},{\"id\":180,\"fullname\":\"Test course user 175\"},{\"id\":181,\"fullname\":\"Test course user 176\"},{\"id\":182,\"fullname\":\"Test course user 177\"},{\"id\":183,\"fullname\":\"Test course user 178\"},{\"id\":184,\"fullname\":\"Test course user 179\"},{\"id\":185,\"fullname\":\"Test course user 180\"},{\"id\":186,\"fullname\":\"Test course user 181\"},{\"id\":187,\"fullname\":\"Test course user 182\"},{\"id\":188,\"fullname\":\"Test course user 183\"},{\"id\":189,\"fullname\":\"Test course user 184\"},{\"id\":190,\"fullname\":\"Test course user 185\"},{\"id\":191,\"fullname\":\"Test course user 186\"},{\"id\":192,\"fullname\":\"Test course user 187\"},{\"id\":193,\"fullname\":\"Test course user 188\"},{\"id\":194,\"fullname\":\"Test course user 189\"},{\"id\":195,\"fullname\":\"Test course user 190\"},{\"id\":196,\"fullname\":\"Test course user 191\"},{\"id\":197,\"fullname\":\"Test course user 192\"},{\"id\":198,\"fullname\":\"Test course user 193\"},{\"id\":199,\"fullname\":\"Test course user 194\"},{\"id\":200,\"fullname\":\"Test course user 195\"},{\"id\":201,\"fullname\":\"Test course user 196\"},{\"id\":202,\"fullname\":\"Test course user 197\"},{\"id\":203,\"fullname\":\"Test course user 198\"},{\"id\":204,\"fullname\":\"Test course user 199\"},{\"id\":1005,\"fullname\":\"Test course user 1000\"}]";
        SearchContactListParser parser= SearchContactListParser.getInstance();
        ArrayList<Contact> lista=parser.parseJSON(entrada1);

        //user 1,10-19+100-199+1000= 112 resultados
        assertEquals(112,lista.size());

        for(int i=1;i<lista.size();i++){
            assertTrue(lista.get(i).getFullname().contains("user 1"));
        }

    }

}


