package es.ubu.tfg.moodroid.view;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import es.ubu.tfg.moodroid.R;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
/**
 * Clase que implementa ExploraArchivoSubida.
 * @author Alvar Alonso �lvarez
 * @version 1.0
 */
public class ExplorarArchivoSubida extends ListActivity {
	/**
	 * La lista de cadenas de archivos en el dispositivo del usuario.
	 */
	private List<String> item = null;
	/**
	 * La lista de cadenas del path de los archivos del dispositivo del usuario.
	 */
	private List<String> path = null;
	/**
	 * El directorio del archivo.
	 */
	private String root="/";
	/**
	 * Texto correspondiente al path del usuario.
	 */
	private TextView myPath;
	/**
	 * El archivo seleccionado por el usuario en su dispositivo.
	 */
	private String archivo_seleccionado;
	/**
	 * El nombre del archivo seleccionado por el usuario en su dispositivo.
	 */
	private String archivo_nombre;

	/**
	 * Muestra un mensaje cuando se ha seleccionado el archivo correctamente.
	 * @see android.app.Activity#finish()
	 */
	public void finish() {
		Intent data = new Intent();
		data.putExtra("archivo_seleccionado", archivo_seleccionado);
		data.putExtra("archivo_nombre", archivo_nombre);
		setResult(RESULT_OK, data);
		super.finish();
	}
	/**
	 * Muestra una lista de los archivos de nuestro dispositivo.
	 * @param savedInstanceState la configuraci�n inicial de la actividad.
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_file);                
		myPath = (TextView)findViewById(R.id.path);
		obtenerDireccion(root);
	}
	/**
	 * Obtiene la direcci�n de los archivos en nuestro dispositivo.
	 * @param dirPath el directorio del archivo seleccionado.
	 */
	private void obtenerDireccion(String dirPath){
		myPath.setText(dirPath);
		item = new ArrayList<String>();
		path = new ArrayList<String>();
		File f = new File(dirPath);
		File[] files = f.listFiles();

		if(!dirPath.equals(root)){
			item.add(root);
			path.add(root);
			item.add("../");
			path.add(f.getParent());
		}

		for(int i=0; i < files.length; i++){
			File file = files[i];
			path.add(file.getPath());
			if(file.isDirectory())
				item.add(file.getName() + "/");
			else
				item.add(file.getName());
		}
		ArrayAdapter<String> fileList =
				new ArrayAdapter<String>(this, R.layout.row_file, item);
		setListAdapter(fileList);
	}
	/**
	 * Selecciona el archivo de nuestro dispositivo y muestra su nombre en pantalla si se puede o muestra un mensaje por lo contrario.
	 * 
	 * @param l el listView donde se va a mostrar el resultado.
	 * @param v la vista del resultado.
	 * @param position la posici�n del archivo selecccionado.
	 * @param id el identificativo del archivo seleccionado.
	 * @see android.app.ListActivity#onListItemClick(android.widget.ListView, android.view.View, int, long)
	 */
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {

		File file = new File(path.get(position));
		if (file.isDirectory())
		{
			if(file.canRead())
				obtenerDireccion(path.get(position));
			else
			{
				new AlertDialog.Builder(this)
				.setTitle("[" + file.getName() + "] folder no puede ser leido!")
				.setPositiveButton("OK", 
						new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {}
				}).show();
			}
		}
		else
		{
			archivo_seleccionado = file.getAbsolutePath();
			archivo_nombre = file.getName();
			finish();
		}
	}
}