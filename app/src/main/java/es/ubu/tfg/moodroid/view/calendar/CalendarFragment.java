package es.ubu.tfg.moodroid.view.calendar;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;

import es.ubu.tfg.moodroid.R;
import es.ubu.tfg.moodroid.presenter.CalendarManager;
import es.ubu.tfg.moodroid.presenter.CourseManager;
import es.ubu.tfg.moodroid.presenter.MoodleFunctionExecutor;
import es.ubu.tfg.moodroid.presenter.OnFunctionCompletedListener;
import es.ubu.tfg.moodroid.model.Course;
import es.ubu.tfg.moodroid.model.Event;
import es.ubu.tfg.moodroid.view.FadeInArrayAdapter;
import es.ubu.tfg.moodroid.view.MainActivity;
import es.ubu.tfg.moodroid.view.MoodroidPopulableUI;

/**
 * Clase que implementa la vista del calendario.
 * @author Víctor Díez Rodríguez
 * @version 1.0
 */
public class CalendarFragment extends MoodroidPopulableUI<ArrayList<Event>> {

    /**
     * Spinner para seleccionar el mes.
     */
    private Spinner spinnerMonth;
    /**
     * Spinner para seleccionar el año.
     */
    private Spinner spinnerYear;
    /**
     * Lista los nombres de los meses.
     */
    private String[] months= new String[0];
    /**
     * Mes seleccionado.
     */
    private int month=-1;
    /**
     * Año seleccionado.
     */
    private int year=-1;
    /**
     * Posición del año seleccionado.
     */
    private int selectedYear=1;
    /**
     * Lista de posibles años.
     */
    private Integer[] years=new Integer[0];
    /**
     * Lista de cursos de los que ver un evento.
     */
    private int[] courseIDs;
    /**
     * Mapa de nombres de lo IDs de los cursos y su nombre.
     */
    private HashMap<Integer,String> courseNames;

    /**
     * @see es.ubu.tfg.moodroid.view.MoodroidPopulableUI#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        months= new String[]{getResources().getString(R.string.jan),
                getResources().getString(R.string.feb),
                getResources().getString(R.string.mar),
                getResources().getString(R.string.apr),
                getResources().getString(R.string.may),
                getResources().getString(R.string.jun),
                getResources().getString(R.string.jul),
                getResources().getString(R.string.aug),
                getResources().getString(R.string.sep),
                getResources().getString(R.string.oct),
                getResources().getString(R.string.nov),
                getResources().getString(R.string.dec)};
        courseIDs=null;
        if(savedInstanceState!=null){
            courseIDs=savedInstanceState.getIntArray("courseIDs");
            year=savedInstanceState.getInt("year",-1);
            month=savedInstanceState.getInt("month",-1);
            courseNames= (HashMap<Integer, String>) savedInstanceState.getSerializable("courseNames");
            selectedYear=savedInstanceState.getInt("selectedYear",1);
        }
        else{
            selectedYear=1;
        }
        Bundle arguments=getArguments();
        if(arguments!=null) {
            courseIDs=arguments.getIntArray("courseids");
            year= arguments.getInt("year", -1);
            month = arguments.getInt("month", -1);
        }
        if(month<0 || year<0){
            Calendar calendar=new GregorianCalendar(TimeZone.getDefault());
            month= calendar.get(Calendar.MONTH);
            year=calendar.get(Calendar.YEAR);
        }
        int startYear=new GregorianCalendar(TimeZone.getDefault()).get(Calendar.YEAR);
        years=new Integer[4];
        years[0]=startYear-1;
        years[1]=startYear;
        years[2]=startYear+1;
        years[3]=startYear+2;
        setHasOptionsMenu(true);
    }

    /**
     * @see es.ubu.tfg.moodroid.view.MoodroidPopulableUI#onViewCreated(android.view.View, android.os.Bundle)
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitleEnabled(false);
        if(spinnerMonth !=null){
            spinnerMonth.setSelection(month);
        }
        if(spinnerYear !=null){
            spinnerYear.setSelection(selectedYear);
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu,inflater);
        inflater.inflate(R.menu.calendar_menu, menu);
        spinnerMonth = (Spinner) MenuItemCompat.getActionView(menu.findItem(R.id.menu_calendar_month));
        spinnerYear =(Spinner)MenuItemCompat.getActionView(menu.findItem(R.id.menu_calendar_year));

        spinnerMonth.setBackgroundDrawable(getResources().getDrawable(R.drawable.moodroidtheme_spinner_background_holo_light));
        spinnerYear.setBackgroundDrawable(getResources().getDrawable(R.drawable.moodroidtheme_spinner_background_holo_light));


       spinnerYear.setAdapter(new ArrayAdapter<Integer>(getActivity(),
               R.layout.list_monthitem, R.id.item_month_name, years));
       spinnerYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
           @Override
           public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               setYear(years[position]);
               selectedYear = position;
               Log.i("Calendario", "dataLoaded: " + isDataLoaded());
               if (courseIDs != null) {
                   Log.i("Calendario", "Solicitando eventos desde Spinner");
                   if (isDataLoaded()) {
                       createInterface();
                   }
               }
           }

           @Override
           public void onNothingSelected(AdapterView<?> parent) {

           }
       });


        spinnerMonth.setAdapter(new ArrayAdapter<String>(getActivity(),
                R.layout.list_monthitem, R.id.item_month_name, months));
        spinnerMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setMonth(position);
                Log.i("Calendario", "dataLoaded: " + isDataLoaded());
                if (courseIDs != null) {
                    Log.i("Calendario", "Solicitando eventos desde Spinner");
                    if (isDataLoaded()) {
                        createInterface();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerMonth.setSelection(month);
        spinnerYear.setSelection(selectedYear);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.menu_calendar_add){
            Fragment crearEvento = new CreateEventFragment();
            ((MainActivity)getActivity()).replaceFragment(crearEvento,FragmentTransaction.TRANSIT_FRAGMENT_OPEN,true);
            return true;
        }
        else {
            return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putIntArray("courseIDs", courseIDs);
        outState.putSerializable("courseNames",courseNames);
        outState.putInt("month",month);
        outState.putInt("year",year);
        outState.putInt("selectedYear",selectedYear);
    }


    @Override
    protected void populateInterface(ArrayList<Event> objects, View rootView) {
        setListAdapter(new EventArrayAdapter(getActivity(), objects));
        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Fragment infoEvento = new EventInfoFragment();
                Bundle arguments = new Bundle();
                arguments.putSerializable("event", getContent().get(position));
                arguments.putString("coursename",courseNames.get(getContent().get(position).getCourseid()));
                infoEvento.setArguments(arguments);
                ((MainActivity)getActivity()).replaceFragment(infoEvento,FragmentTransaction.TRANSIT_FRAGMENT_OPEN,true);

            }
        });

        setEmptyText(getResources().getString(R.string.no_events));
    }



    @Override
    protected MoodleFunctionExecutor loadContent(final OnFunctionCompletedListener<ArrayList<Event>> callback) {
        SharedPreferences preferences= PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        final String token= preferences.getString("token","invalid_token");
        if(courseIDs==null) {
            String userid = preferences.getString("userid", "invalid_userid");
            OnFunctionCompletedListener coursesReceived=new OnFunctionCompletedListener<ArrayList<Course>>() {
                @Override
                public void onFunctionCompleted(ArrayList<Course> resultado) {
                    courseNames=new HashMap<Integer, String>();
                    courseIDs= new int[resultado.size()];
                    for(int i = 0; i < resultado.size(); i++){
                        String courseID=resultado.get(i).getId();
                        if(courseID!=null) {
                            Integer id=Integer.valueOf(courseID);
                            courseIDs[i] = Integer.valueOf(courseID);
                            courseNames.put(id, resultado.get(i).getName());
                            Log.i("Calendario","Curso: "+id+resultado.get(i).getName());
                        }
                    }
                    CalendarManager.getEvents(month, year, courseIDs, token, callback);
                }
            };
            return CourseManager.getCourses(userid, token, coursesReceived);
        }
        else{
            return CalendarManager.getEvents(month, year, courseIDs, token, callback);
        }
    }

    /**
     * Establece un mes seleccionado.
     * @param month mes seleccionado.
     */
    private void setMonth(int month){
        this.month=month;
    }

    /**
     * Establece un año seleccionado.
     * @param year año seleccionado.
     */
    private void setYear(int year){
        this.year=year;
    }

    /**
     * Adaptador para poblar la lista de eventos.
     */
    private class EventArrayAdapter extends FadeInArrayAdapter<Event> {
        /**
         * Eventos con los que poblar.
         */
        ArrayList<Event> events;

        /**
         * Crea un nuevo adaptador para una lista de eventos.
         * @param context contexto de la aplicación.
         * @param events  lista de eventos.
         */
        public EventArrayAdapter(Context context, ArrayList<Event> events){
            super(context,R.layout.list_eventitem, events);
            this.events = events;
        }



        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View v = convertView;

            if (v == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = inflater.inflate(R.layout.list_eventitem, null);
            }

            Event event = events.get(position);
            GregorianCalendar calendarInicio=new GregorianCalendar();
            calendarInicio.setTimeInMillis((long) event.getTimeStart() * 1000);
            GregorianCalendar calendarFin=new GregorianCalendar();
            calendarFin.setTimeInMillis((long) event.getTimeStart() * 1000+(long) event.getDuration() * 1000);

            if(event !=null){
                TextView dia= (TextView) v.findViewById(R.id.evento_dia_inicio);
                TextView dia_semana=(TextView)v.findViewById(R.id.evento_dia_semana_inicio);
                TextView nombre=(TextView)v.findViewById(R.id.evento_nombre);
                TextView horaInicio=(TextView)v.findViewById(R.id.evento_hora_inicio);
                TextView mesInicio=(TextView)v.findViewById(R.id.evento_mes_inicio);
                TextView diaFin= (TextView) v.findViewById(R.id.evento_dia_fin);
                TextView dia_semanaFin=(TextView)v.findViewById(R.id.evento_dia_semana_fin);
                TextView horaFin=(TextView)v.findViewById(R.id.evento_hora_fin);
                TextView mesFin=(TextView)v.findViewById(R.id.evento_mes_fin);
                TextView curso=(TextView)v.findViewById(R.id.evento_curso);


                if(dia!=null){
                    dia.setText(String.valueOf(calendarInicio.get(Calendar.DAY_OF_MONTH)));
                }

                if(dia_semana!=null){
                    dia_semana.setText(calendarInicio.getDisplayName(Calendar.DAY_OF_WEEK,
                            Calendar.SHORT
                            , Locale.getDefault()));
                }

                if(nombre!=null){
                    nombre.setText(event.getName());
                }

                if(horaInicio!=null){
                    int horasInt=calendarInicio.get(Calendar.HOUR_OF_DAY);
                    int minutosInt=calendarInicio.get(Calendar.MINUTE);
                    String horas=String.valueOf(horasInt);
                    if(horasInt<10){
                        horas="0"+horas;
                    }
                    String minutos=String.valueOf(minutosInt);
                    if(minutosInt<10){
                        minutos="0"+minutos;
                    }
                    horaInicio.setText(horas+":"+minutos);
                }

                if(diaFin!=null){
                    diaFin.setText(String.valueOf(calendarFin.get(Calendar.DAY_OF_MONTH)));
                }

                if(dia_semanaFin!=null){
                    dia_semanaFin.setText(calendarFin.getDisplayName(Calendar.DAY_OF_WEEK,
                            Calendar.SHORT
                            , Locale.getDefault()));

                }
                if(horaFin!=null){
                    int horasInt=calendarFin.get(Calendar.HOUR_OF_DAY);
                    int minutosInt=calendarFin.get(Calendar.MINUTE);
                    String horas=String.valueOf(horasInt);
                    if(horasInt<10){
                        horas="0"+horas;
                    }
                    String minutos=String.valueOf(minutosInt);
                    if(minutosInt<10){
                        minutos="0"+minutos;
                    }
                    horaInicio.setText(horas+":"+minutos);
                }

                if(mesFin!=null){
                    String mes=calendarFin.getDisplayName(Calendar.MONTH, Calendar.SHORT,
                            Locale.getDefault());
                    mesFin.setText(mes);
                }

                if(mesInicio!=null){
                    String mes=calendarInicio.getDisplayName(Calendar.MONTH, Calendar.SHORT,
                            Locale.getDefault());
                    mesInicio.setText(mes.toString());
                }

                if(curso!=null){
                    String nombreCurso=courseNames.get(new Integer(event.getCourseid()));
                    if(nombreCurso!=null){
                        curso.setText(nombreCurso);
                        curso.setVisibility(View.VISIBLE);
                    }
                    else{
                        curso.setVisibility(View.GONE);
                    }
                }

            }

            return applyRowAnimation(v);
        }
    }
}
