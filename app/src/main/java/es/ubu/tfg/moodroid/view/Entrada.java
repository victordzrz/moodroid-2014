package es.ubu.tfg.moodroid.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import es.ubu.tfg.moodroid.R;
import es.ubu.tfg.moodroid.presenter.MoodleFunctionExecutor;
import es.ubu.tfg.moodroid.model.GestorPOST;
import es.ubu.tfg.moodroid.model.ValidaciónUsuario;
import es.ubu.tfg.moodroid.model.parser.SimpleParser;

/**
 * Pantalla inicial en el que se ingresa el usuario en el sistema.
 * @author Víctor Díez Rodríguez
 * @version 2.0
 */
public class Entrada extends Activity {
	/**
	 * Texto editable por el usuario para introducir su nombre.
	 */
	private EditText user;
	/**
	 * Texto editable por el usuario para introducir su contrase�a.
	 */
	private EditText pass;
	/**
	 * Texto editable por el usuario para introducir la url para conectarse al sistema.
	 */
	private EditText url;
	/**
	 * Botón creado para que se ingrese el usuario en el sistema.
	 */
	private Button login;

	/**
	 * Comprueba si el usuario es correcto. Si lo es, ingresa en el sistema o por el contrario, devuelve un mensaje de error.
     * Guarda los datos del usuario en las preferencias compartidas de la aplicación.
	 * @param savedInstanceState la configuración inicial de la actividad.
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        final HashMap<String,String> preferences=new HashMap<String, String>();

        SharedPreferences sharedPreferences= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if(!checkValidPreferences()) {
            setContentView(R.layout.entrada);
            url = (EditText) findViewById(R.id.URL);
            user = (EditText) findViewById(R.id.usuario);
            pass = (EditText) findViewById(R.id.password);
            login = (Button) findViewById(R.id.Login);

            login.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {

                    String ip = url.getText().toString();
                    String usuario = user.getText().toString();
                    String contraseña = pass.getText().toString();


                    preferences.put("url", ip);
                    preferences.put("username", usuario);
                    preferences.put("password", contraseña);

                    GestorPOST.obtenerGestorPost().setDOMINIO(ip);

                    ValidaciónUsuario gestorConexión = new ValidaciónUsuario();
                    String token = gestorConexión.obtenerToken(usuario, contraseña, ip);
                    if (token == null) {
                        err_login();
                    } else {
                        preferences.put("token",token);
                        preferences.putAll(getUserData(token));
                        savePreferences(preferences);
                        Intent intentEntrada = new Intent(Entrada.this, MainActivity.class);
                        intentEntrada.putExtra("token", token);
                        startActivity(intentEntrada);
                        finish();
                    }
                }
            });
        }
        else{
            GestorPOST.obtenerGestorPost().setDOMINIO(sharedPreferences.getString("url","invalid_url"));
            Intent intentEntrada = new Intent(Entrada.this, MainActivity.class);
            startActivity(intentEntrada);
            finish();

        }
	}

    /**
     * Obtiene datos del usuario.
     *
     * @param token token del usuario.
     * @return datos del usuario. Estos datos son:"userid", "firstname", "lastname", "userpictureurl" y "sitename".
     */
    public Map<String,String> getUserData(String token) {
        HashMap<String,String> userData=new HashMap<String, String>();
        MoodleFunctionExecutor<String> executor=new MoodleFunctionExecutor<String>();
        executor.addParams("wstoken",token);
        executor.setParser(SimpleParser.getInstance());
        executor.execute("core_webservice_get_site_info");

        try {
            String jsonString=executor.get();
            JSONObject jsonObject=new JSONObject(jsonString);
            userData.put("userid",jsonObject.getString("userid"));
            userData.put("firstname",jsonObject.getString("firstname"));
            userData.put("lastname",jsonObject.getString("lastname"));
            userData.put("userpictureurl",jsonObject.getString("userpictureurl"));
            userData.put("sitename",jsonObject.getString("sitename"));

            Log.e("Entrada", "Data"+userData.toString());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return userData;
    }

	/**
	 * Si el usuario es incorrecto, vibra el móvil y muestra un mensaje por pantalla de error.
	 */
	public void err_login(){
		Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		vibrator.vibrate(500);
		Toast toast1 = Toast.makeText(getApplicationContext(),getString(R.string.login_error_message), Toast.LENGTH_LONG);
		toast1.show();    	
	}

    /**
     * Guarda los datos necesarios para hacer login en las preferencias de la aplicación.
     *
     */
    public void savePreferences(Map<String,String> preferences){
        SharedPreferences sharedPreferences= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor=sharedPreferences.edit();
        for(String key: preferences.keySet()){
            editor.putString(key,preferences.get(key));

        }
        editor.commit();
    }

    /**
     * Comprueba que las preferencias establecidas son válidas
     * @return true si son válidas, false si no lo son.
     */
    public boolean checkValidPreferences(){
        SharedPreferences sharedPreferences= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        return !(sharedPreferences.getString("url",null)==null
                || sharedPreferences.getString("username",null)==null
                || sharedPreferences.getString("password",null)==null
                || sharedPreferences.getString("token",null)==null
                ||sharedPreferences.getString("userid",null)==null);
    }
}