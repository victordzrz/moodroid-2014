package es.ubu.tfg.moodroid.view.calendar;

/**
 * Interfaz que permite devolver un resultado al cerrar un Dialog
 * @author Víctor Díez Rodríguez
 * @version 1.0
 */
public interface OnFinishDialogListener {

    /**
     * Método para devolver el resultado del Dialog.
     * @param result resultado del Dialog.
     */
    public void onFinishDialog(int result);
}
