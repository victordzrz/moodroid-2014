package es.ubu.tfg.moodroid.model.parser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import es.ubu.tfg.moodroid.model.Course;
import es.ubu.tfg.moodroid.model.Module;

/**
 * Clase para parsear una cadena JSON en una lista de contenido de cursos.
 * @author Víctor Díez Rodríguez
 * @version 1.0
 *
 *
 */
public class CourseContentListParser implements MoodleJSONParser<ArrayList<Course>>{

    private static CourseContentListParser instance;
    private CourseContentListParser(){}

    /**
     * Obtiene una instancia del parser.
     * @return instancia del parser
     */
    public static CourseContentListParser getInstance(){
        if(instance==null){
            instance=new CourseContentListParser();
        }
        return instance;
    }

    /**
     * Parsea la cadena JSON
     *
     * @param jsonString cadena a parsear
     * @return lista de contenido de cursos.
     */
    @Override
    public ArrayList<Course> parseJSON(String jsonString) {
        ArrayList <Course> courses =new ArrayList<Course>();
        if(jsonString!=null) {
            try {

                JSONArray jsonArray = new JSONArray(jsonString);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject c = jsonArray.getJSONObject(i);
                    Course course = new Course(c.getString("id"), c.getString("name"));
                    JSONArray a = c.getJSONArray("modules");

                    for (int j = 0; j < a.length(); j++) {
                        JSONObject segundo = a.getJSONObject(j);
                        String modname = segundo.getString("modname");
                        String url;
                        String name;
                        if (modname.equals("resource")) {
                            JSONArray contents = segundo.getJSONArray("contents");
                            JSONObject resource = contents.getJSONObject(0);
                            url = resource.getString("fileurl");
                            name = resource.getString("filename");
                        } else {
                            url = segundo.getString("url");
                            name = segundo.getString("name");
                        }
                        course.addModule(new Module(
                                modname,
                                url,
                                name,
                                segundo.getString("modicon")));
                    }
                    courses.add(course);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return courses;
    }
}
