package es.ubu.tfg.moodroid.view.groups;

import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import es.ubu.tfg.moodroid.R;
import es.ubu.tfg.moodroid.presenter.GroupManager;
import es.ubu.tfg.moodroid.presenter.MoodleFunctionExecutor;
import es.ubu.tfg.moodroid.presenter.OnFunctionCompletedListener;
import es.ubu.tfg.moodroid.model.Group;
import es.ubu.tfg.moodroid.view.FadeInArrayAdapter;
import es.ubu.tfg.moodroid.view.MainActivity;
import es.ubu.tfg.moodroid.view.MoodroidPopulableUI;

/**
 * Clase que implementa la vista para mostrar una lista de grupos.
 *
 * @author Víctor Díez Rodríguez
 * @version 1.0
 */
public class GroupListFragment extends MoodroidPopulableUI<ArrayList<Group>> {
    /**
     * Curso del que se muestran los grupos.
     */
    private String courseid;
    /**
     * Posición del curso seleccionado.
     */
    private int selectedViewPosition=-1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        courseid=getArguments().getString("courseid");
        if(savedInstanceState!=null){
            selectedViewPosition=savedInstanceState.getInt("selectedViewPosition");
        }
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitleEnabled(true);
        setTitle("Grupos");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("selectedViewPosition",selectedViewPosition);
    }

    @Override
    protected void populateInterface(ArrayList<Group> objects, View rootView) {
        setListAdapter(new GroupArrayAdapter(getActivity().getApplicationContext(),
                objects));
        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                seeMembers(position);
            }
        });

        setEmptyText(getResources().getString(R.string.no_groups_available));

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu,inflater);
        inflater.inflate(R.menu.group_list_menu,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.menu_group_add){
            createGroup();
            return true;
        }
        else {
            return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected MoodleFunctionExecutor loadContent(OnFunctionCompletedListener callback) {
        String token= PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext()).getString("token","invalid_token");

        return GroupManager.getCourseGroups(courseid, token, callback);

    }

    /**
     * Selecciona un elemento y muestra su menú.
     * @param position posición del elemento seleccionado.
     */
    private void selectElement(int position){
        if(position>=0){
            View menu=getListView().getChildAt(position).findViewById(R.id.grupo_menu);
            if(menu.getVisibility()==View.VISIBLE){
                menu.setVisibility(View.GONE);
                selectedViewPosition=-1;
            }
            else{
                menu.setVisibility(View.VISIBLE);
                if(selectedViewPosition>=0){
                    getListView().getChildAt(selectedViewPosition).findViewById(R.id.grupo_menu).setVisibility(View.GONE);
                }
                selectedViewPosition=position;
            }
        }
    }

    /**
     * Añade miembros al grupo seleccionado.
     */
    private void addMembers(){
        int groupID= getContent().get(selectedViewPosition).getId();

        Fragment grupos=new MemberListFragment();
        Bundle args=new Bundle();
        args.putSerializable("courseid",courseid);
        args.putSerializable("action", MemberAction.ADD);
        args.putString("groupid",String.valueOf(groupID));
        grupos.setArguments(args);
        ((MainActivity)getActivity()).replaceFragment(grupos,FragmentTransaction.TRANSIT_FRAGMENT_OPEN,true);


    }

    /**
     * Elimina miembros del grupo seleccinado.
     */
    private void deleteMembers(){
        int groupID= getContent().get(selectedViewPosition).getId();

        Fragment grupos=new MemberListFragment();
        Bundle args=new Bundle();
        args.putSerializable("courseid",courseid);
        args.putSerializable("action", MemberAction.DELETE);
        args.putString("groupid",String.valueOf(groupID));
        grupos.setArguments(args);
        ((MainActivity)getActivity()).replaceFragment(grupos,FragmentTransaction.TRANSIT_FRAGMENT_OPEN,true);


    }

    /**
     * Muestra los miembros del grupo seleccionado.
     * @param position
     */
    private void seeMembers(int position){
        int groupID= getContent().get(position).getId();

        Fragment grupos=new MemberListFragment();
        Bundle args=new Bundle();
        args.putSerializable("courseid",courseid);
        args.putSerializable("action", MemberAction.VIEW);
        args.putString("groupid",String.valueOf(groupID));
        grupos.setArguments(args);
        ((MainActivity)getActivity()).replaceFragment(grupos,FragmentTransaction.TRANSIT_FRAGMENT_OPEN,true);


    }

    /**
     * Elimina el grupo seleccionado.
     */
    private void deleteGroup(){
        String token= PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext()).getString("token","invalid_token");
        int groupID= getContent().get(selectedViewPosition).getId();
        GroupManager.deleteGroup(String.valueOf(groupID), token, new OnFunctionCompletedListener<String>() {
            @Override
            public void onFunctionCompleted(String jsonString) {
                Toast toast = Toast.makeText(getActivity().getApplicationContext(),
                        getResources().getString(R.string.group_deleted), Toast.LENGTH_SHORT);
                toast.show();
                createInterface();
                selectElement(-1);
            }
        });

    }

    /**
     * Crea un nuevo grupo.
     */
    private void createGroup(){
        Fragment crearGrupo=new CreateGroupFragment();
        Bundle args=new Bundle();
        args.putSerializable("courseid", courseid);
        crearGrupo.setArguments(args);
        ((MainActivity)getActivity()).replaceFragment(crearGrupo, FragmentTransaction.TRANSIT_FRAGMENT_OPEN, true);


    }

    /**
     * Adaptador para poblar una lista con grupos.
     */
    private class GroupArrayAdapter extends FadeInArrayAdapter<Group> {
        /**
         * Lista de grupos con los que poblar la lista.
         */
        private ArrayList<Group> groups;
        /**
         * Listiener para añadir miembros a un grupo.
         */
        private View.OnClickListener addMembersListener;
        /**
         * Listener para eliminar miembros de un grupo.
         */
        private View.OnClickListener deleteMembersListener;
        /**
         * Listener para eliminar un grupo.
         */
        private View.OnClickListener deleteGroupListener;

        /**
         * Crea un nuevo adaptador de grupos.
         * @param context contexto de la aplicación.
         * @param groups grupos con los que poblar la lista.
         */
        public GroupArrayAdapter(Context context, ArrayList<Group> groups){
            super(context, R.layout.list_groupitem, groups);
            this.groups = groups;

            addMembersListener =new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addMembers();
                }
            };

            deleteMembersListener =new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteMembers();
                }
            };

            deleteGroupListener =new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteGroup();
                }
            };

        }

        @Override
        public View getView(final int position, final View convertView, ViewGroup parent) {
            View v = convertView;

            if (v == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = inflater.inflate(R.layout.list_groupitem, null);
            }

            Group group = groups.get(position);

            TextView grupoNombre=(TextView)v.findViewById(R.id.grupo_nombre);
            TextView grupoDescripcion=(TextView)v.findViewById(R.id.grupo_descripcion);

            if (grupoNombre!=null){
                grupoNombre.setText(group.getName());
            }

            if(grupoDescripcion!=null){
                grupoDescripcion.setText(group.getDescription());
            }

            if(position==selectedViewPosition){
                v.findViewById(R.id.grupo_menu).setVisibility(View.VISIBLE);
            }
            else{
                v.findViewById(R.id.grupo_menu).setVisibility(View.GONE);
            }

            View anadirMiembros=v.findViewById(R.id.grupo_anadir_miembros);
            View quitarMiembros=v.findViewById(R.id.grupo_quitar_miembros);
            View eliminarGrupo=v.findViewById(R.id.grupo_eliminar);
            ImageView overflow=(ImageView)v.findViewById(R.id.grupo_overflow);


            anadirMiembros.setOnClickListener(addMembersListener);
            quitarMiembros.setOnClickListener(deleteMembersListener);
            eliminarGrupo.setOnClickListener(deleteGroupListener);
            overflow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.i("Grupos","Click recibido");
                    selectElement(position);
                }
            });



            return applyRowAnimation(v);
        }
    }
}
