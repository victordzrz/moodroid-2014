package es.ubu.tfg.moodroid.view.calendar;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import es.ubu.tfg.moodroid.R;
import es.ubu.tfg.moodroid.presenter.CalendarManager;
import es.ubu.tfg.moodroid.presenter.OnFunctionCompletedListener;
import es.ubu.tfg.moodroid.model.Event;
import es.ubu.tfg.moodroid.view.LoadingFragment;
import es.ubu.tfg.moodroid.view.TitleFragment;

/**
 * Clase que implementa la vista para mostrar la información de un evento.
 */
public class EventInfoFragment extends TitleFragment {
    /**
     * Evento del que se muestra la información.
     */
    private Event event;
    /**
     * Nombre del curso al que pertenece el evento.
     */
    private String courseName;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        event =(Event)getArguments().getSerializable("event");
        courseName=getArguments().getString("coursename");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.event_info_layout,container,false);

        GregorianCalendar calendarInicio=new GregorianCalendar();
        calendarInicio.setTimeInMillis((long) event.getTimeStart() * 1000);
        GregorianCalendar calendarFin=new GregorianCalendar();
        calendarFin.setTimeInMillis((long) event.getTimeStart() * 1000+(long) event.getDuration() * 1000);


        if(event !=null) {
            TextView diaInicio = (TextView) rootView.findViewById(R.id.evento_dia_inicio);
            TextView diaSemanaInicio = (TextView) rootView.findViewById(R.id.evento_dia_semana_inicio);
            TextView nombre = (TextView) rootView.findViewById(R.id.evento_nombre);
            TextView horaInicio = (TextView) rootView.findViewById(R.id.evento_hora_inicio);
            TextView mesInicio=(TextView)rootView.findViewById(R.id.evento_mes_inicio);
            WebView descripcion = (WebView) rootView.findViewById(R.id.infoevento_descripción);
            TextView diaFin= (TextView) rootView.findViewById(R.id.evento_dia_fin);
            TextView dia_semanaFin=(TextView)rootView.findViewById(R.id.evento_dia_semana_fin);
            TextView horaFin=(TextView)rootView.findViewById(R.id.evento_hora_fin);
            TextView mesFin=(TextView)rootView.findViewById(R.id.evento_mes_fin);
            TextView curso=(TextView)rootView.findViewById(R.id.evento_curso);

            if(descripcion!=null){
                descripcion.loadData(event.getDescription(),"text/html; charset=UTF-8",null);
            }

            if(diaInicio!=null){
                diaInicio.setText(String.valueOf(calendarInicio.get(Calendar.DAY_OF_MONTH)));
            }

            if(diaSemanaInicio!=null){
                diaSemanaInicio.setText(calendarInicio.getDisplayName(Calendar.DAY_OF_WEEK,
                        Calendar.SHORT
                        , Locale.getDefault()));
            }

            if(nombre!=null){
                nombre.setText(event.getName());
            }

            if(horaInicio!=null){
                int horasInt=calendarInicio.get(Calendar.HOUR_OF_DAY);
                int minutosInt=calendarInicio.get(Calendar.MINUTE);
                String horas=String.valueOf(horasInt);
                if(horasInt<10){
                    horas="0"+horas;
                }
                String minutos=String.valueOf(minutosInt);
                if(minutosInt<10){
                    minutos="0"+minutos;
                }
                horaInicio.setText(horas+":"+minutos);
            }

            if(diaFin!=null){
                diaFin.setText(String.valueOf(calendarFin.get(Calendar.DAY_OF_MONTH)));
            }

            if(dia_semanaFin!=null){
                dia_semanaFin.setText(calendarFin.getDisplayName(Calendar.DAY_OF_WEEK,
                        Calendar.SHORT
                        , Locale.getDefault()));

            }
            if(horaFin!=null){
                int horasInt=calendarFin.get(Calendar.HOUR_OF_DAY);
                int minutosInt=calendarFin.get(Calendar.MINUTE);
                String horas=String.valueOf(horasInt);
                if(horasInt<10){
                    horas="0"+horas;
                }
                String minutos=String.valueOf(minutosInt);
                if(minutosInt<10){
                    minutos="0"+minutos;
                }
                horaInicio.setText(horas+":"+minutos);
            }

            if(mesFin!=null){
                String mes=calendarFin.getDisplayName(Calendar.MONTH, Calendar.SHORT,
                        Locale.getDefault());
                mesFin.setText(mes);
            }

            if(mesInicio!=null){
                String mes=calendarInicio.getDisplayName(Calendar.MONTH, Calendar.SHORT,
                        Locale.getDefault());
                mesInicio.setText(mes.toString());
            }

            if(curso!=null){
                if(courseName!=null){
                    curso.setText(courseName);
                    curso.setVisibility(View.VISIBLE);
                }
                else{
                    curso.setVisibility(View.GONE);
                }
            }


        }

            return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.event_info_menu,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.menu_event_delete){
            deleteEvent(event.getId());
            return true;
        }
        else if(item.getItemId()==R.id.menu_event_sync){
            syncEvent(event);
            return true;
        }
        else {
            return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Elimina un evento de Moodle.
     * @param eventID id del evento a eliminar.
     */
    private void deleteEvent(int eventID){
        Toast toast = Toast.makeText(getActivity(), getResources().getString(R.string.deleting_event), Toast.LENGTH_SHORT);
        toast.show();
        String token= PreferenceManager.getDefaultSharedPreferences(getActivity())
                .getString("token", "invalid_token");
        final Fragment loadingFragment=new LoadingFragment();
        final Fragment thisFragment=this;
        OnFunctionCompletedListener callback=new OnFunctionCompletedListener<String>() {
            @Override
            public void onFunctionCompleted(String jsonString) {
                getActivity().getSupportFragmentManager().beginTransaction().remove(loadingFragment).commit();
                Toast toast;
                if(jsonString.contains("nopermissions")){
                        toast = Toast.makeText(getActivity(), getResources().getString(R.string.no_permission_delete_event), Toast.LENGTH_SHORT);
                }else {
                    toast = Toast.makeText(getActivity(), getResources().getString(R.string.event_deleted), Toast.LENGTH_SHORT);

                }
                toast.show();
                getActivity().getSupportFragmentManager().popBackStack();
                Log.e("InfoEvento",jsonString);
            }
        };
        CalendarManager.deleteEvent(eventID, token, callback);
        getActivity().getSupportFragmentManager().beginTransaction().add(R.id.fragment_container,loadingFragment).commit();
    }

    /**
     * Sincroniza el evento con el calendario del smartphone.
     * @param event evento a sincronizar.
     */
    private void syncEvent(Event event){
        Intent intent = new Intent(Intent.ACTION_INSERT);
        intent.setType("vnd.android.cursor.item/event");
        intent.putExtra("title", event.getName());
        intent.putExtra("description", event.getDescription());
        intent.putExtra("beginTime",(long) event.getTimeStart()*1000);
        intent.putExtra("endTime", (long) event.getTimeStart()*1000+(long) event.getDuration()*1000);
        startActivity(intent);

    }
}
