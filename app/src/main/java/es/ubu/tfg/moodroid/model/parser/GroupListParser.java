package es.ubu.tfg.moodroid.model.parser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import es.ubu.tfg.moodroid.model.Group;

/**
 * Clase para parsear una cadena JSON en una lista de grupos.
 * @author Víctor Díez Rodríguez
 * @version 1.0
 *
 *
 */
public class GroupListParser implements MoodleJSONParser<ArrayList<Group>> {
    private static GroupListParser instance;

    private GroupListParser(){}

    /**
     * Obtiene una instancia del parser.
     * @return instancia del parser
     */
    public static GroupListParser getInstance(){
        if(instance==null){
            instance=new GroupListParser();
        }
        return instance;
    }

    /**
     * Parsea la cadena JSON
     *
     * @param jsonString cadena a parsear
     * @return lista de grupos.
     */
    @Override
    public ArrayList<Group> parseJSON(String jsonString) {
        ArrayList<Group> groups =new ArrayList<Group>();
        if(jsonString!=null) {
            try {
                JSONArray jsonGrupos = new JSONArray(jsonString);
                for (int i = 0; i < jsonGrupos.length(); i++) {
                    JSONObject jsonGrupo = jsonGrupos.getJSONObject(i);
                    Group group = new Group(jsonGrupo.getInt("id"));
                    group.setCourseid(jsonGrupo.getInt("courseid"));
                    group.setName(jsonGrupo.getString("name"));
                    group.setDescription(jsonGrupo.getString("description"));
                    groups.add(group);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        }
        return groups;
    }
}
