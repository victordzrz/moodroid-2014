package es.ubu.tfg.moodroid.view.calendar;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import es.ubu.tfg.moodroid.R;
import es.ubu.tfg.moodroid.presenter.CalendarManager;
import es.ubu.tfg.moodroid.presenter.OnFunctionCompletedListener;
import es.ubu.tfg.moodroid.model.Event;
import es.ubu.tfg.moodroid.view.LoadingFragment;
import es.ubu.tfg.moodroid.view.TitleFragment;

/**
 * Clase que implementa la vista para crear un nuevo evento.
 *
 * @author Víctor Díez Rodríguez
 * @version 1.0
 */
public class CreateEventFragment extends TitleFragment {
    /**
     * Nombre del evento.
     */
    private EditText editName;
    /**
     * Seleccionador de fecha de inicio.
     */
    private Button btnDateFrom;
    /**
     * Seleccionador de hora de inicio.
     */
    private Button btnHourFrom;
    /**
     * Seleccionador de fecha de fin.
     */
    private Button btnDateTo;
    /**
     * Seleccionador de hora de fin.
     */
    private Button btnHourTo;
    /**
     * Descripción del evento.
     */
    private EditText editDescription;
    /**
     * Hora de inicio.
     */
    private int hourFrom =0;
    /**
     * Hora de fin.
     */
    private int hourTo =0;
    /**
     * Fecha de inicio.
     */
    private int dateFrom =0;
    /**
     * Fecha de fin.
     */
    private int dateTo =0;
    /**
     * Nombre del evento.
     */
    private String name =null;
    /**
     * Descripción del evento.
     */
    private String description =null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if(savedInstanceState!=null){
            hourFrom =savedInstanceState.getInt("hourFrom");
            hourTo =savedInstanceState.getInt("hourTo");
            dateFrom =savedInstanceState.getInt("dateFrom");
            dateTo =savedInstanceState.getInt("dateTo");
            name =savedInstanceState.getString("name");
            description =savedInstanceState.getString("description");
        }
        else{
            Calendar calendar=new GregorianCalendar(TimeZone.getDefault());
            Log.e("CrearEvento",calendar.getTime().toString());
            hourFrom = hourTo = dateTo = dateFrom =(int)(calendar.getTimeInMillis()/1000);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.create_event_layout,container,false);
        editName =(EditText)rootView.findViewById(R.id.edit_nombre_evento);

        btnDateFrom =(Button)rootView.findViewById(R.id.btn_fecha_evento_desde);
        btnHourFrom =(Button)rootView.findViewById(R.id.btn_hora_evento_desde);
        btnDateTo =(Button)rootView.findViewById(R.id.btn_fecha_evento_hasta);
        btnHourTo =(Button)rootView.findViewById(R.id.btn_hora_evento_hasta);


        setDateFrom(dateFrom);
        setDateTo(dateTo);
        setHourFrom(hourFrom);
        setHourTo(hourTo);

        if(description !=null){
            editDescription.setText(description);
        }
        if(name !=null){
            editName.setText(name);
        }


        btnDateFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerFragment fragment = new DatePickerFragment();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fragment.setOnFinishDialogListener(new OnFinishDialogListener() {
                    @Override
                    public void onFinishDialog(int result) {
                        setDateFrom(result);
                    }
                });
                fragment.show(fm, "datePickerFragment");
            }
        });

        btnDateTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerFragment fragment = new DatePickerFragment();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fragment.setOnFinishDialogListener(new OnFinishDialogListener() {
                    @Override
                    public void onFinishDialog(int result) {
                        setDateTo(result);
                    }
                });
                fragment.show(fm, "datePickerFragment");
            }
        });

        btnHourTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerFragment fragment = new TimePickerFragment();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fragment.setOnFinishDialogListener(new OnFinishDialogListener() {
                    @Override
                    public void onFinishDialog(int result) {
                        setHourTo(result);
                    }
                });
                fragment.show(fm, "timePickerFragment");
            }
        });

        btnHourFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerFragment fragment = new TimePickerFragment();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fragment.setOnFinishDialogListener(new OnFinishDialogListener() {
                    @Override
                    public void onFinishDialog(int result) {
                        setHourFrom(result);
                    }
                });
                fragment.show(fm, "timePickerFragment");
            }
        });

        editDescription =(EditText)rootView.findViewById(R.id.edit_descripcion_evento);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitleEnabled(true);
        setTitle(getResources().getString(R.string.add_event));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.create_event_menu,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.menu_event_accept){
            Calendar calendarFecha=new GregorianCalendar();
            Calendar calendarHora=new GregorianCalendar();
            calendarFecha.setTimeInMillis((long) dateFrom *1000);
            calendarHora.setTimeInMillis((long) hourFrom *1000);
            calendarFecha.set(Calendar.HOUR,calendarHora.get(Calendar.HOUR));
            calendarFecha.set(Calendar.MINUTE,calendarHora.get(Calendar.MINUTE));
            Log.e("AñadirEvento",calendarFecha.getTime().toString());
            int timeStart=(int)(calendarFecha.getTimeInMillis()/1000);

            calendarFecha.setTimeInMillis((long) dateTo *1000);
            calendarHora.setTimeInMillis((long) hourTo *1000);
            calendarFecha.set(Calendar.HOUR,calendarHora.get(Calendar.HOUR));
            calendarFecha.set(Calendar.MINUTE,calendarHora.get(Calendar.MINUTE));
            Log.e("AñadirEvento",calendarFecha.getTime().toString());
            int timeEnd=(int)(calendarFecha.getTimeInMillis()/1000);

            crearEvento(editName.getText().toString(), editDescription.getText().toString(),timeStart,timeEnd-timeStart);
            return true;
        }
        else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        name = editName.getText().toString();
        description = editDescription.getText().toString();
        outState.putInt("hourFrom", hourFrom);
        outState.putInt("hourTo", hourTo);
        outState.putInt("dateFrom", dateFrom);
        outState.putInt("dateTo", dateTo);
        outState.putString("name", name);
        outState.putString("description", description);
    }

    /**
     * Crea un nuevo evento en Moodle.
     * @param name nombre del evento.
     * @param descripcion descripción del evento.
     * @param timeStart fecha de inicio en segundos.
     * @param duration duración del evento en segundos.
     */
    private void crearEvento(String name,String descripcion, int timeStart, int duration){
        Toast toast = Toast.makeText(getActivity(), getResources().getString(R.string.creating_event), Toast.LENGTH_SHORT);
        toast.show();
        String token= PreferenceManager.getDefaultSharedPreferences(getActivity())
                .getString("token", "invalid_token");
        final Fragment loadingFragment=new LoadingFragment();
        OnFunctionCompletedListener callback=new OnFunctionCompletedListener<String>() {
            @Override
            public void onFunctionCompleted(String jsonString) {
                getActivity().getSupportFragmentManager().beginTransaction().remove(loadingFragment).commit();
                Toast toast = Toast.makeText(getActivity(), getResources().getString(R.string.event_created), Toast.LENGTH_SHORT);
                toast.show();
                getActivity().getSupportFragmentManager().popBackStack();
                Log.e("CrearEvento",jsonString);
            }
        };
        Event event =new Event(0,name);
        event.setDescription(descripcion);
        event.setTimeStart(timeStart);
        event.setDuration(duration);
        CalendarManager.createEvent(event, token, callback);
        getActivity().getSupportFragmentManager().beginTransaction().add(R.id.fragment_container,loadingFragment).commit();


    }

    /**
     * Establece la hora de inicio.
     * @param timeSeconds hora de inicio en segundos.
     */
    private void setHourFrom(int timeSeconds){
        this.hourFrom =timeSeconds;
        Calendar cal=new GregorianCalendar();
        cal.setTimeInMillis((long)timeSeconds*1000);
        btnHourFrom.setText(cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE));
    }

    /**
     * Establece la hora de fin.
     * @param timeSeconds hora de fin en segundos.
     */
    private void setHourTo(int timeSeconds){
        this.hourTo =timeSeconds;
        Calendar cal=new GregorianCalendar();
        cal.setTimeInMillis((long)timeSeconds*1000);
        Log.e("HoraHasta",cal.toString());
        btnHourTo.setText(cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE));
    }

    /**
     * Establece la fecha de inicio.
     * @param timeSeconds fecha de inicio en segundos.
     */
    private void setDateFrom(int timeSeconds){
        this.dateFrom =timeSeconds;
        Calendar cal=new GregorianCalendar();
        cal.setTimeInMillis((long)timeSeconds*1000);
        btnDateFrom.setText(cal.get(Calendar.DAY_OF_MONTH) + "/"
                + (cal.get(Calendar.MONTH) + 1) + "/"
                + cal.get(Calendar.YEAR));
    }

    /**
     * Establece la fecha de fin.
     * @param timeSeconds fecha de fin en segundos.
     */
    private void setDateTo(int timeSeconds){
        this.dateTo =timeSeconds;
        Calendar cal=new GregorianCalendar();
        cal.setTimeInMillis((long)timeSeconds*1000);
        btnDateTo.setText(cal.get(Calendar.DAY_OF_MONTH) + "/" +
                (cal.get(Calendar.MONTH) + 1) + "/" +
                cal.get(Calendar.YEAR));
    }

}
