package es.ubu.tfg.moodroid.view;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import java.io.Serializable;

import es.ubu.tfg.moodroid.R;
import es.ubu.tfg.moodroid.presenter.ExecutorManager;
import es.ubu.tfg.moodroid.presenter.MoodleFunctionExecutor;
import es.ubu.tfg.moodroid.presenter.OnFunctionCompletedListener;

/**
 * Clase abstracta que implementa una vista que puede ser poblada con un contenido de tipo genérico
 * obtenido a través de una función de Moodle y cuyo título se puede personalizar.
 *
 * @param <T> tipo del contenido.
 *
 *  @author Víctor Díez Rodríguez
 *  @version 1.0
 */
public abstract class MoodroidPopulableUI<T extends Serializable> extends TitleListFragment implements
        ActionBarTitleChanger {
    /**
     * Fragment que se muestra mientras se cargan los datos.
     */
    private LoadingFragment fragment;
    /**
     * Contenido que se desea mostrar.
     */
    private T content = null;
    /**
     * El cotenido está cargado.
     */
    private boolean dataLoaded;
    /**
     * La actividad que contiene esta vista se está mostrando.
     */
    private boolean activityRunning=false;
    /**
     * El esta vista se está mostrando.
     */
    private boolean fragmentShown=false;
    /**
     * Ejecutor que se encarga de descargar los datos de Moodle.
     */
    private MoodleFunctionExecutor executor;

    /**
     * Puebla la vista con objetos.
     * @param objects objetos con los que poblar la vista.
     * @param rootView vista que poblar.
     */
    protected abstract void populateInterface(T objects, View rootView);

    /**
     * Descarga el contenido con el que se desea poblar la vista.
     * @param callback callback que se ejecuta cuando se ha descargado el contenido.
     * @return ejecutor que se encarga de descargar el contenido.
     */
    protected abstract MoodleFunctionExecutor<T> loadContent(OnFunctionCompletedListener<T> callback);

    /**
     * Reestablece el contenido previamente cargado si existe.
     * @param savedInstanceState contenedor del contenido previamente cargado.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityRunning=true;
        if (savedInstanceState != null) {
            content = (T) savedInstanceState.getSerializable("content");
            dataLoaded = savedInstanceState.getBoolean("dataLoaded");
        }
        setHasOptionsMenu(true);
    }

    /**
     * Se asegura de que la vista creada está mostrando el contenido si ya se ha descargado y
     * si no lo descarga.
     * @param view vista en la que mostrar el contenido.
     * @param savedInstanceState contenedor del contenido previamente cargado.
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (!isDataLoaded() || getContent()!=null) {
            createInterface();
        } else {
            reloadUI(view);
        }
    }


    /**
     * Crea la interfaz y muestra una barra de progreso mientras lo hace.
     * Aquí se registran los {@link es.ubu.tfg.moodroid.presenter.MoodleFunctionExecutor} que
     * se encargan de descargar los datos en el {@link es.ubu.tfg.moodroid.presenter.ExecutorManager}.
     */
    public void createInterface() {
        startProgressDialog();
        OnFunctionCompletedListener callback=new OnFunctionCompletedListener<T>() {
            @Override
            public void onFunctionCompleted(T resultado) {
                content = resultado;
                setDataLoaded(true);
                populateInterface(content, getView());
                stopProgressDialog();
                ExecutorManager.getInstance().cancelExecutor(executor);
            }
        };
        executor=loadContent(callback);
        ExecutorManager.getInstance().addExecutor(executor);

    }

    /**
     * Cancelan todos los {@link es.ubu.tfg.moodroid.presenter.MoodleFunctionExecutor} que no
     * han acabado y para la barra de progreso.
     */
    @Override
    public void onPause() {
        super.onPause();
        ExecutorManager.getInstance().cancelAll();
        stopProgressDialog();

    }

    /**
     * Guarda el contenido descargado para su posterior reutilización
     * @param outState contenedor donde se guarda el contenido.
     */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        activityRunning=false;
        outState.putSerializable("content", content);
        outState.putBoolean("dataLoaded", dataLoaded);
    }

    /**
     * Recarga la interfaz con datos previamente descargado.
     * @param rootView vista que pob
     *                 lar.
     */
    public void reloadUI(View rootView) {
        populateInterface(content, rootView);
    }

    /**
     * Comprueba si se han descargado los datos.
     * @return si lo datos se han descargado.
     */
    public boolean isDataLoaded() {
        return dataLoaded;
    }

    /**
     * Establece si los datos se han descargado.
     * @param loaded si los datos se han descargado.
     */
    public void setDataLoaded(boolean loaded) {
        dataLoaded = loaded;
    }

    /**
     * Obtiene el contenido descargado.
     * @return contenido descargado.
     */
    public T getContent() {
        return content;
    }

    /**
     * Muestra la barra de progreso.
     */
    public void startProgressDialog() {
        if(activityRunning && !fragmentShown) {
            fragmentShown=true;
            fragment = new LoadingFragment();
            getActivity().getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, fragment, "loading_fragment").commit();
        }
    }

    /**
     * Esconde la barra de progreso.
     */
    public void stopProgressDialog() {
        if(activityRunning && fragmentShown) {
            fragmentShown=false;
            getActivity().getSupportFragmentManager().beginTransaction().remove(fragment).commit();
        }
    }

    public boolean onBackPressed() {
        return false;
    }

    /**
     * Añade nuevas opciones al menú.
     * @param menu menú que contendrá las nuevas opciones.
     * @param inflater inflater para añadir las opciones al menú.
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.reload, menu);
    }

    /**
     * Regoge las pulsaciónes en los elementos del menú y actua en consecuencia.
     * @param item
     * @return si la pulsación se ha consumido o no.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_refresh) {
            createInterface();
            return true;
        } else{

            return super.onOptionsItemSelected(item);
        }
    }
}

