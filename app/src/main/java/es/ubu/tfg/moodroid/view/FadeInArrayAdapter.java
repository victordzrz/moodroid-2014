package es.ubu.tfg.moodroid.view;

import android.content.Context;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.ArrayAdapter;

import java.util.List;

/**
 * @author Víctor Díez Rodríguez
 * @version 1.0
 * @param <T> tipo que adapta el adaptador.
 */
public abstract class FadeInArrayAdapter<T> extends ArrayAdapter<T> {

    /**
     *
     * @param context contexto de la aplicación.
     * @param resource layout de cada elemento de la lista.
     * @param objects lista de elementos.
     * @see android.widget.ArrayAdapter#ArrayAdapter(android.content.Context, int, java.util.List)
     */
    public FadeInArrayAdapter(Context context, int resource, List<T> objects){
        super(context,resource,objects);
    }

    /**
     * Aplica una animación de aparición a una vista.
     * @param view vista a la que se desea aplicar la transición.
     * @return vista con la transición aplicada.
     */
    protected View applyRowAnimation(View view){
        if(view!=null){
            Animation fadeIn = new AlphaAnimation(0, 1);
            fadeIn.setInterpolator(new DecelerateInterpolator()); //add this
            fadeIn.setDuration(750);

            view.startAnimation(fadeIn);
        }

        return view;

    }
}
