package es.ubu.tfg.moodroid.model.parser;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import es.ubu.tfg.moodroid.model.Event;

/**
 * Clase para parsear una cadena JSON en una lista de eventos.
 * @author Víctor Díez Rodríguez
 * @version 1.0
 *
 *
 */
public class EventListParser implements MoodleJSONParser<ArrayList<Event>> {

    private static EventListParser instance;

    private EventListParser(){}

    /**
     * Obtiene una instancia del parser.
     * @return instancia del parser
     */
    public static EventListParser getInstance(){
        if(instance==null){
            instance=new EventListParser();
        }
        return instance;
    }

    /**
     * Parsea la cadena JSON
     *
     * @param jsonString cadena a parsear
     * @return lista de eventos.
     */
    @Override
    public ArrayList<Event> parseJSON(String jsonString) {
        ArrayList<Event> events =new ArrayList<Event>();
        if(jsonString!=null) {

            try {
                JSONObject respuesta = new JSONObject(jsonString);
                JSONArray eventosJSON = respuesta.getJSONArray("events");
                Log.i("Calendario", "Events obtenidos");

                for (int i = 0; i < eventosJSON.length(); i++) {
                    JSONObject eventoJSON = eventosJSON.getJSONObject(i);
                    Event event = new Event(eventoJSON.getInt("id"), eventoJSON.getString("name"));
                    event.setCourseid(eventoJSON.getInt("courseid"));
                    event.setDescription(eventoJSON.getString("description"));
                    event.setDuration(eventoJSON.getInt("timeduration"));
                    event.setTimeStart(eventoJSON.getInt("timestart"));
                    events.add(event);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return events;
    }
}
