package es.ubu.tfg.moodroid.model.parser;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import es.ubu.tfg.moodroid.model.Course;

/**
 * Clase para parsear una cadena JSON en una lista de cursos.
 * @author Víctor Díez Rodríguez
 * @version 1.0
 *
 *
 */
public class CourseListParser implements MoodleJSONParser<ArrayList<Course>> {

    private static CourseListParser instance;

    private CourseListParser(){}


    /**
     * Obtiene una instancia del parser.
     * @return instancia del parser
     */
    public static CourseListParser getInstance(){
        if(instance==null){
            instance=new CourseListParser();
        }
        return instance;
    }

    /**
     * Parsea la cadena JSON
     *
     * @param jsonString cadena a parsear
     * @return lista de cursos.
     */
    @Override
    public ArrayList<Course> parseJSON(String jsonString) {
        final String TAG_NAME = "fullname";

        ArrayList<Course> courses=new ArrayList<Course>();
        if(jsonString!=null) {
            try {
                JSONArray jasonCursos = new JSONArray(jsonString);
                for (int i = 0; i < jasonCursos.length(); i++) {
                    JSONObject c = jasonCursos.getJSONObject(i);
                    final String fullname = c.getString(TAG_NAME);
                    final String courseid = c.getString("id");

                    courses.add(i, new Course(courseid, fullname));
                }
                Log.e("INFO", "Cursos obtenidos.");
            } catch (JSONException e) {
                Log.e("ERROR", "No se pudo obtener los cursos.");
            }
        }

        return courses;
    }
}
