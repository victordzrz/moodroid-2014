package es.ubu.tfg.moodroid.view.courses;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.AdapterView;

import java.util.ArrayList;

import es.ubu.tfg.moodroid.R;
import es.ubu.tfg.moodroid.presenter.CourseManager;
import es.ubu.tfg.moodroid.presenter.MoodleFunctionExecutor;
import es.ubu.tfg.moodroid.presenter.OnFunctionCompletedListener;
import es.ubu.tfg.moodroid.model.Course;
import es.ubu.tfg.moodroid.view.MainActivity;
import es.ubu.tfg.moodroid.view.MoodroidPopulableUI;

/**
 * Clase que implementa la vista para mostrar una lista de cursos.
 * @author Víctor Díez Rodríguez
 * @version 2.0
 */
public class CourseListFragment extends MoodroidPopulableUI<ArrayList<Course>> implements AdapterView.OnItemClickListener {


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitleEnabled(true);
        setTitle(getResources().getString(R.string.Courses));

    }

    @Override
    protected void populateInterface(ArrayList objects, View rootView) {

        setListAdapter(new CourseArrayAdapter(getActivity().getApplicationContext(), objects));

        getListView().setOnItemClickListener(this);
        setEmptyText(getResources().getString(R.string.no_courses_available));

    }

    @Override
    protected MoodleFunctionExecutor loadContent(OnFunctionCompletedListener<ArrayList<Course>> callback) {
        SharedPreferences preferences=PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        String token= preferences.getString("token","invalid_token");
        String userid=preferences.getString("userid","invalid_userid");
        return CourseManager.getCourses(userid, token, callback);

    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        ArrayList<Course> courses= getContent();
        Fragment courseFragment=new CourseContentFragment();
        Bundle args=new Bundle();
        args.putString("courseid",courses.get(i).getId());
        args.putString("fullname", courses.get(i).getName());
        courseFragment.setArguments(args);
        ((MainActivity)getActivity()).replaceFragment(courseFragment, FragmentTransaction.TRANSIT_FRAGMENT_OPEN, true);

    }
}