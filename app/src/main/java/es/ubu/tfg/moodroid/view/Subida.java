package es.ubu.tfg.moodroid.view;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

import es.ubu.tfg.moodroid.R;
import es.ubu.tfg.moodroid.model.Base64Encoder;
import es.ubu.tfg.moodroid.model.GestorPOST;
/**
 * Clase que implementa Subida.
 * @author Alvar Alonso �lvarez
 * @version 1.0
 */
public class Subida extends Activity {
	/**
	 * Bot�n creado para pasar a la actividad que busca los archivos del dispositivo del usuario.
	 */
	private Button open_file_explorer;
	/**
	 * Bot�n creado para subir al espacio privado del usuario en Moodle el archivo seleccionado.
	 */
	private Button subir;
	/**
	 * Muestra por pantalla el archivo seleccionado.
	 */
	private TextView archivo;
	/**
	 * El archivo seleccionado.
	 */
	private String archivo_seleccionado;
	/**
	 * El nombre del archivo seleccionado.
	 */
	private String archivo_nombre;
	/**
	 * El identificador del usuario que solicita los cursos.
	 */
	public static String token;

	/** 
	 * Crea la interfaz y le da la acci�n a cada elemento.
	 * @param savedInstanceState la configuraci�n inicial de la actividad.
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.upload);
		token = (String) getIntent().getSerializableExtra("token");

		archivo = (TextView)findViewById(R.id.txtNombreArchivo);

		open_file_explorer = (Button)findViewById(R.id.btnBuscar);
		open_file_explorer.setOnClickListener(new OnClickListener() {                        
			public void onClick(View v) {
				Intent file_explorer = new Intent(Subida.this,ExplorarArchivoSubida.class);
				startActivityForResult(file_explorer, 555);
			}
		});
		subir = (Button)findViewById(R.id.btnSubir);
		subir.setOnClickListener(new OnClickListener() {                        
			public void onClick(View v) {                                
				if(archivo_seleccionado==null || archivo_seleccionado.equals("")){
					Toast.makeText(Subida.this, "Selecciona un archivo",
							Toast.LENGTH_SHORT).show();
				}else{                                        
					try{        
						UploadFileTask uploadFileTask = new UploadFileTask(Subida.this);
						uploadFileTask.setToken(token);
						uploadFileTask.execute(archivo_seleccionado);
						String resultado = uploadFileTask.get();
						Toast.makeText(Subida.this, resultado, Toast.LENGTH_SHORT).show();
                        finish();
					}
					catch (Exception ex)
					{
						ex.printStackTrace();
					}                                        
				}                                
			}
		});
	}
	/**
	 * Se llama a la actividad y se devuelve el mensaje de que se ha seleccionado el archivo.
	 * @param requestCode el c�digo de la petici�n.
	 * @param resultCode el resultado del c�digo.
	 * @param data el Intent.
	 * @see android.app.Activity#onActivityResult(int, int, android.content.Intent)
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK && requestCode == 555) {
			if (data.hasExtra("archivo_seleccionado")) {
				archivo_seleccionado = data.getExtras().getString("archivo_seleccionado");
				archivo_nombre = data.getExtras().getString("archivo_nombre");
				archivo.setText(archivo_nombre);
			}
		}
	} 
}
/**
 * Clase para subir el archivo. Como puede tardar un tiempo en subir el archivo se usa AsyncTask para evitar bloquear la interfaz gr�fica.
 * @author Alvar Alonso �lvarez
 * @version 1.0
 */
class UploadFileTask extends AsyncTask<String,Void,String> {


	private String token = null;

	private Activity activity;
	/**
	 * Constructor de la clase con su actividad inicial.
	 * @param activity 
	 */
	public UploadFileTask(Activity activity){
		this.activity = activity;
	}

	/**
	 * Realiza la subida del archivo a la zona privada del usuario.
	 * @return el post.
	 */
	protected String doInBackground(String... string) {
		String fileName = string[0].substring(string[0].lastIndexOf("/") + 1);
		String encodedFile = Base64Encoder.base64encode(string[0]);
		String userID = getUserID(token);
		String contextID = getUserContextID(userID);

		HttpPost httpPost = new HttpPost(GestorPOST.WSURL);
		List<NameValuePair> params = new ArrayList<NameValuePair>(2);
		params.add(new BasicNameValuePair("wstoken", token));
		params.add(new BasicNameValuePair("wsfunction", "core_files_upload"));
		params.add(new BasicNameValuePair("moodlewsrestformat", "json"));
		params.add(new BasicNameValuePair("contextid", contextID));
		params.add(new BasicNameValuePair("component", "user"));
		params.add(new BasicNameValuePair("filearea", "private"));
		params.add(new BasicNameValuePair("itemid", "0"));
		params.add(new BasicNameValuePair("filepath", "/"));
		params.add(new BasicNameValuePair("filename", fileName));
		params.add(new BasicNameValuePair("filecontent", encodedFile));

		try {
			httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
			GestorPOST gestorPost = GestorPOST.obtenerGestorPost();
			if (gestorPost.obtenerJSONObject(httpPost).toString() != null) {
				return "Subida realizada correctamente";
			} else {
				return "Ocurri� un error en la subida";
			}
		} catch (Exception e) {
			return e.getMessage();
		}
	}
	/**
	 * Obtiene el id del usuario.
	 * @param token el identificador del usuario.
	 * @return el identificador del usuario.
	 */
	public String getUserID(String token) {
		String userID = "noid";
		HttpPost httpPost = new HttpPost(GestorPOST.WSURL);

		List<NameValuePair> params = new ArrayList<NameValuePair>(2);
		params.add(new BasicNameValuePair("wstoken", token));
		params.add(new BasicNameValuePair("wsfunction", "core_webservice_get_site_info"));
		params.add(new BasicNameValuePair("moodlewsrestformat", "json"));

		try {
			httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
			userID = execute(httpPost, "userid");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		System.out.println(userID);
		return userID;
	}
	/**
	 * Se ejecuta para obtener el identificador del usuario.
	 * @param httpPost el post para crear la conexi�n.
	 * @param jsonName 
	 * @return
	 */
	public String execute(HttpPost httpPost, String jsonName) {
		String jsonString = "";
		String resultString = "";

		try {
			HttpClient httpClient = new DefaultHttpClient();
			HttpResponse response = httpClient.execute(httpPost);
			HttpEntity entity = response.getEntity();

			if (entity != null) {
				InputStream instream = entity.getContent();
				jsonString = convertStreamToString(instream);
				instream.close();

				if (!jsonName.equals("")) {
					JSONObject jsonObject = new JSONObject(jsonString);
					return jsonObject.getString(jsonName);					
				} else {
					return jsonString;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return resultString;
	}
	/**
	 * Se conecta con la base de datos para obtener el contexto necesario para subir el archivo.
	 * @return la conexi�n.
	 */
	public Connection getConnection() {
		Connection conn = null;
		Properties connectionProps = new Properties();
		connectionProps.put("user", "root");
		connectionProps.put("password", "");

		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://10.0.2.2:3306/moodle", connectionProps);
			Log.e(Subida.class.toString(), "Conectado a la BBDD");
		} catch (Exception e) {
			Log.e(Subida.class.toString(), "No se pudo conectar a la BBDD");
		}
		
		return conn;
	}
	/**
	 * Obtiene el contexto del usuario.
	 * @param userID el identificador del usuario.
	 * @return el contexto.
	 */
	public String getUserContextID(String userID) {
		String contextID = "nocontextid";
		Statement stmt = null;
		String query = "select id from mdl_context" +
				" where instanceid=" + userID + " and contextlevel=30;";
		try {
			Connection conn = getConnection();
			
			if (conn == null) {
				throw new Exception("No hay conexi�n con la BD");
			}
			
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				contextID = rs.getString("id");
				System.out.println("Requested ID: " + contextID);
				break;
			}
			
		} catch (Exception e) {
			Log.e(Subida.class.toString(), e.getMessage()); 
		}

		return contextID;
	}

	/**
	 * Convierte a cadena el Stream recibido.
	 * @param is
	 * @return la cadena recibida.
	 */
	private String convertStreamToString(InputStream is) {
		Scanner s = new Scanner(is).useDelimiter("\\A");
		return s.hasNext() ? s.next() : "";
	}

	/**
	 * Se ejecuta si se a ejecutado el método doInBackground y muestra en pantalla un mensaje.
	 * @param feed
	 * (non-Javadoc)
	 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
	 */
	protected void onPostExecute(String feed) {
		Toast.makeText(activity, feed,
				Toast.LENGTH_SHORT).show();
	}

	public void setToken(String token) {
		this.token = token;
	}
}
/**
 * Clase que obtiene la respuesta al subir el archivo.
 * @author Alvar Alonso �lvarez
 * @version 1.0
 */
@SuppressWarnings("rawtypes")
class fileUploadResponseHandler implements ResponseHandler {
	/**
	 * Obtiene la respuesta.
	 * @return responseString la respuesta.
	 */
	@Override
	public Object handleResponse(HttpResponse response)
			throws ClientProtocolException, IOException {

		HttpEntity r_entity = response.getEntity();
		String responseString = EntityUtils.toString(r_entity);
		Log.d("UPLOAD", responseString);

		return responseString;
	}
}