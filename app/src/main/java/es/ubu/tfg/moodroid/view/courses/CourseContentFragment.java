package es.ubu.tfg.moodroid.view.courses;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import es.ubu.tfg.moodroid.R;
import es.ubu.tfg.moodroid.presenter.CourseManager;
import es.ubu.tfg.moodroid.presenter.MoodleFunctionExecutor;
import es.ubu.tfg.moodroid.presenter.OnFunctionCompletedListener;
import es.ubu.tfg.moodroid.model.Course;
import es.ubu.tfg.moodroid.model.Module;
import es.ubu.tfg.moodroid.view.FadeInArrayAdapter;
import es.ubu.tfg.moodroid.view.MainActivity;
import es.ubu.tfg.moodroid.view.MoodroidPopulableUI;

/**
 * Clase que implementa la vista del contenido de un curso.
 * @author Víctor Díez Rodríguez
 * @version 2.0
 */
public class CourseContentFragment extends MoodroidPopulableUI<ArrayList<Course>> {
	/**
	 * Token del usuario conectado.
	 */
	private String token;
	/**
	 * Id del curso.
	 */
	private String courseid;
	/**
	 * Nombre del curso.
	 */
	private String fullname;
    /**
     * Conjunto de módulos expandidos.
     */
    private HashSet<Integer> expandedModules;
    /**
     * Mapa de iconos de los módulos.
     */
    private HashMap<String,Integer> iconMap=null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        SharedPreferences sp= PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        token    = sp.getString("token","invalid_token");
        courseid = (String)getArguments().get("courseid");
        fullname = (String)getArguments().get("fullname");
        if(savedInstanceState!=null){
            iconMap=(HashMap<String,Integer>)savedInstanceState.getSerializable("iconMap");
            expandedModules =(HashSet<Integer>)savedInstanceState.getSerializable("expandedModules");
        }
        else{
            expandedModules =new HashSet<Integer>();
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitleEnabled(true);
        setTitle(fullname);
    }

    /**
     * Inicializa el mapa de iconos para acceder más rápidamente a ellos al crear las listas.
     */
    private void initIconMap(){
        iconMap=new HashMap<String, Integer>();

        iconMap.put("assign",R.drawable.assign);
        iconMap.put("book",R.drawable.book);
        iconMap.put("chat",R.drawable.chat);
        iconMap.put("choice",R.drawable.choice);
        iconMap.put("data",R.drawable.data);
        iconMap.put("folder",R.drawable.folder);
        iconMap.put("forum",R.drawable.forum);
        iconMap.put("forum",R.drawable.glossary);
        iconMap.put("glossary",R.drawable.glossary);
        iconMap.put("imscp",R.drawable.imscp);
        iconMap.put("label",R.drawable.label);
        iconMap.put("lesson",R.drawable.lesson);
        iconMap.put("lti",R.drawable.lti);
        iconMap.put("page",R.drawable.page);
        iconMap.put("quiz",R.drawable.quiz);
        iconMap.put("resource",R.drawable.resource);
        iconMap.put("scorm",R.drawable.scorm);
        iconMap.put("survey",R.drawable.survey);
        iconMap.put("url",R.drawable.url);
        iconMap.put("wiki",R.drawable.wiki);
        iconMap.put("workshop",R.drawable.workshop);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu,inflater);
        inflater.inflate(R.menu.enrolled_user_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_participantes:
                Fragment participantesCurso=new EnrolledUserListFragment();
                Bundle args=new Bundle();
                args.putString("courseid",courseid);
                args.putString("fullname",fullname);
                participantesCurso.setArguments(args);
                ((MainActivity)getActivity()).replaceFragment(participantesCurso, FragmentTransaction.TRANSIT_FRAGMENT_OPEN, true);

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSaveInstanceState (Bundle outState){
        super.onSaveInstanceState(outState);
        outState.putSerializable("iconMap", iconMap);
        outState.putSerializable("expandedModules", expandedModules);
    }

    @Override
    protected void populateInterface(ArrayList<Course> objects, View rootView) {
        setListAdapter(new CourseContentArrayAdapter(getActivity().getApplicationContext(), objects));
        setEmptyText(getResources().getString(R.string.no_content_available));
    }


    @Override
    protected MoodleFunctionExecutor loadContent(OnFunctionCompletedListener<ArrayList<Course>> callback) {
        return CourseManager.getCourseContents(courseid, token, callback);
    }


    /**
     * Adaptador que puebla una lista con los módulos de un curso.
     */
    private class ModuleArrayAdapter extends ArrayAdapter<Module> implements AdapterView.OnItemClickListener {
        /**
         * Módulos que contiene un curso.
         */
        private ArrayList<Module> contents;


        /**
         * Crea un nuevo adaptador de módulos.
         * @param context contexto de la aplicación.
         * @param contents módulos del curso.
         */
        public ModuleArrayAdapter(Context context, ArrayList<Module> contents) {
            super(context, R.layout.list_moduleitem, contents);
            if(iconMap==null){
                initIconMap();
            }
            this.contents = contents;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            View v = convertView;

            if (v == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = inflater.inflate(R.layout.list_moduleitem, null);
            }

            Module i = contents.get(position);

            if (i != null) {

                TextView contentName = (TextView) v.findViewById(R.id.modulo_nombre);
                ImageView contentIcon=(ImageView)v.findViewById(R.id.mod_icon);

                if(contentName!=null){
                    //Log.i("ContenidoAdapter",i.getName());
                    contentName.setText(i.getName());
                }
                if(contentIcon!=null){
                   //Log.e("ContenidoCurso",i.getImgurl());
                   contentIcon.setImageResource((Integer) iconMap.get(i.getType()));
                }

            }
            return v;

        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            String url=contents.get(position).getUrl()+"&token="+token;
            if(contents.get(position).getType().equals("resource")){
                DownloadManager downloadManager=(DownloadManager)getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
                DownloadManager.Request request=new DownloadManager.Request(Uri.parse(url));
                request.setDestinationInExternalPublicDir(
                        Environment.DIRECTORY_DOWNLOADS,
                        contents.get(position).getName());
                if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB) {
                    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                }
                downloadManager.enqueue(request);
            }
            else {
                Intent intentNovedades = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intentNovedades);
            }
        }
    }

    /**
     * Expande el contenido de un módulo.
     * @param position posición del módulo a expandir.
     */
    private void expandModule(Integer position, View v){
        if(v!=null) {
            View modulo = v.findViewById(R.id.lista_modulos);
            ImageView imagen = (ImageView) v.findViewById(R.id.modulo_expandir);
            if (expandedModules.contains(position)) {
                modulo.setVisibility(View.GONE);
                expandedModules.remove(position);
                imagen.setImageResource(R.drawable.ic_action_expand);
            } else {
                modulo.setVisibility(View.VISIBLE);
                expandedModules.add(position);
                imagen.setImageResource(R.drawable.ic_action_collapse);
            }
        }

    }

    /**
     * Adaptador que puebla una lista con el contenido de un curso y sus módulos.
     */
    private class CourseContentArrayAdapter extends FadeInArrayAdapter<Course> {
        /**
         * Cursos con contenido para poblar.
         */
        private ArrayList<Course> courses;


        /**
         * Crea un nuevo adaptador para un curso.
         * @param context contexto de la aplicación.
         * @param courses  cursos para poblar.
         */
        public CourseContentArrayAdapter(Context context, ArrayList<Course>  courses) {
            super(context, R.layout.list_coursecontent, courses);
            this.courses = courses;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent){
            View v = convertView;

            if (v == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = inflater.inflate(R.layout.list_coursecontent, null);
            }

            Course i = courses.get(position);

            if (i != null) {

                TextView moduleName = (TextView) v.findViewById(R.id.modulo_nombre);
                ListView contentList = (ListView) v.findViewById(R.id.lista_modulos);
                final ImageView expand=(ImageView)v.findViewById(R.id.modulo_expandir);

                if(moduleName!=null){
                    moduleName.setText(i.getName());
                }
                if(contentList!=null){
                    ModuleArrayAdapter adapter=new ModuleArrayAdapter(getActivity().getApplicationContext(),i.getModules());
                    contentList.setAdapter(adapter);
                    contentList.setOnItemClickListener(adapter);

                }
                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        expandModule(position,v);
                    }
                });

                if(expand!=null) {
                    if(i.getModules().size()==0){
                        expand.setVisibility(View.GONE);
                    }
                    else{
                        if(expandedModules.contains(position)){
                            contentList.setVisibility(View.VISIBLE);
                            expand.setImageResource(R.drawable.ic_action_collapse);
                        }
                        else{
                            contentList.setVisibility(View.GONE);
                            expand.setImageResource(R.drawable.ic_action_expand);
                        }
                    }
                }

            }


            return applyRowAnimation(v);

        }
    }
}