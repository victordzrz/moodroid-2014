package es.ubu.tfg.moodroid.presenter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Clase para descargar imágenes,
 *
 * @author Víctor Díez Rodríguez
 * @version 1.0
 */
public class ImageLoader extends AsyncTask<String, Void, Bitmap> {
    /**
     * Callback para devolver la imagen.
     */
    private OnImageLoadedListener listener;

    /**
     * Descarga la imagen en segundo plano.
     * @param params URL de la imagen a descargar.
     * @return  imagen descargada.
     */
    @Override
    protected Bitmap doInBackground(String... params) {
        HttpURLConnection conn = null;
        Bitmap imagenPerfil = null;
        try {
            URL imageUrl = new URL(params[0]);
            try {
                conn = (HttpURLConnection) imageUrl.openConnection();
                conn.connect();
                imagenPerfil = BitmapFactory.decodeStream(conn.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return imagenPerfil;
    }

    /**
     * Devuelve la imagen descargada.
     * @param result imagen descargada.
     */
    @Override
    protected void onPostExecute(Bitmap result) {
        if (result != null) {
            listener.onImageLoaded(result);
        }
    }

    /**
     * Establece un callback para devolver la imagen descargada.
     * @param listener
     */
    public void setOnImageLoadedListener(OnImageLoadedListener listener) {
        this.listener = listener;
    }

}

