package es.ubu.tfg.moodroid.test.model.parser;

import android.test.InstrumentationTestCase;

import java.util.ArrayList;
import java.util.Random;

import es.ubu.tfg.moodroid.model.Course;
import es.ubu.tfg.moodroid.model.parser.CourseListParser;

/**
 * Clase de test para CourseListParser
 */
public class CourseListParserTest extends InstrumentationTestCase {

    public void testGetInstance(){
        CourseListParser parser= CourseListParser.getInstance();
        final int expected=parser.hashCode();
        assertEquals(expected,CourseListParser.getInstance().hashCode());
    }

    public void testParse(){
        int courseCount=100;
        int[] IDs=new int[courseCount];
        String[] names=new String[courseCount];
        Random random=new Random();
        for(int i=0;i<courseCount;i++){
            IDs[i]=random.nextInt();
            names[i]=Long.toHexString(random.nextLong());
        }
        CourseListParser parser= CourseListParser.getInstance();
        ArrayList<Course> lista=parser.parseJSON(buildInput(IDs,names));


        assertEquals(100,lista.size());
        for(int i=0;i<courseCount;i++){
            assertEquals(lista.get(i).getName(),names[i]);
            assertEquals(lista.get(i).getId(),String.valueOf(IDs[i]));
        }

    }


    public String buildInput(int[] id, String[] name){
        StringBuilder entrada=new StringBuilder();
        entrada.append("[");
        for(int i=0;i<id.length;i++){
            entrada.append("{\"id\":");
            entrada.append(id[i]);
            entrada.append(",\"shortname\":\"Test S1\",\"fullname\":\"");
            entrada.append(name[i]);
            entrada.append("\",\"enrolledusercount\":101,\"idnumber\":\"\",\"visible\":1}");
            if(i<id.length-1)
                entrada.append(",");
        }
        entrada.append("]");

        return entrada.toString();
    }
}
