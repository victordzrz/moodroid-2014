package es.ubu.tfg.moodroid.view;



import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import es.ubu.tfg.moodroid.R;

/**
 * Fragment que muestra una barra de progreso indefinida.
 */
public class LoadingFragment extends Fragment {


    public LoadingFragment() {
    }


    /**
     * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_loading, container, false);
    }


}
