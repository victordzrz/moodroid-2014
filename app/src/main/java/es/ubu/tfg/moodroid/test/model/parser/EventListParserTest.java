package es.ubu.tfg.moodroid.test.model.parser;

import android.test.InstrumentationTestCase;

import java.util.ArrayList;
import java.util.Random;

import es.ubu.tfg.moodroid.model.Event;
import es.ubu.tfg.moodroid.model.parser.EventListParser;

/**
 * Clase de test para EventListParser
 */
public class EventListParserTest extends InstrumentationTestCase {

    public void testGetInstance(){
        EventListParser parser= EventListParser.getInstance();
        final int expected=parser.hashCode();
        assertEquals(expected,EventListParser.getInstance().hashCode());
    }

    public void testParse(){
        int courseCount=100;
        int[] IDs=new int[courseCount];
        String[] names=new String[courseCount];
        String[] description=new String[courseCount];
        int[] courseid=new int[courseCount];
        int[] timestart=new int[courseCount];
        int[] timeduration=new int[courseCount];
        Random random=new Random();
        for(int i=0;i<courseCount;i++){
            IDs[i]=random.nextInt();
            names[i]=Long.toHexString(random.nextLong());
            description[i]=Long.toHexString(random.nextLong());
            courseid[i]=random.nextInt();
            timestart[i]=random.nextInt();
            timeduration[i]=random.nextInt();

        }
        EventListParser parser= EventListParser.getInstance();
        ArrayList<Event> lista=parser.parseJSON(buildInput(IDs,names,description,courseid,timestart,timeduration));


        assertEquals(100,lista.size());
        for(int i=0;i<courseCount;i++){
            assertEquals(lista.get(i).getName(),names[i]);
            assertEquals(lista.get(i).getId(),IDs[i]);
            assertEquals(lista.get(i).getDescription(),description[i]);
            assertEquals(lista.get(i).getCourseid(),courseid[i]);
            assertEquals(lista.get(i).getTimeStart(),timestart[i]);
            assertEquals(lista.get(i).getDuration(),timeduration[i]);
        }

    }

    public String buildInput(int[] id, String[] name,String[] description,int[] courseid, int[] timestart, int[] timeduration){
        StringBuilder entrada=new StringBuilder();
        entrada.append("{\"events\":[");
        for(int i=0;i<id.length;i++){
            entrada.append("{\"id\":");
            entrada.append(id[i]);
            entrada.append(",\"name\":\"");
            entrada.append(name[i]);
            entrada.append("\",\"description\":\"");
            entrada.append(description[i]);
            entrada.append("\",\"format\":1,\"courseid\":");
            entrada.append(courseid[i]);
            entrada.append(",\"groupid\":0,\"userid\":1006,\"repeatid\":0,\"modulename\":\"\",\"instance\":0,\"eventtype\":\"user\",\"timestart\":");
            entrada.append(timestart[i]);
            entrada.append(",\"timeduration\":");
            entrada.append(timeduration[i]);
            entrada.append(",\"visible\":1,\"uuid\":\"\",\"sequence\":1,\"timemodified\":1402587965,\"subscriptionid\":null}");
            if(i<id.length-1)
                entrada.append(",");
        }
        entrada.append("],\"warnings\":[]}");

        return entrada.toString();
    }
}
