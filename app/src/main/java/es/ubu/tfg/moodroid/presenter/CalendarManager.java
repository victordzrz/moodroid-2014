package es.ubu.tfg.moodroid.presenter;

import android.util.Log;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import es.ubu.tfg.moodroid.model.Event;
import es.ubu.tfg.moodroid.model.parser.EventListParser;
import es.ubu.tfg.moodroid.model.parser.SimpleParser;

/**
 * Clase para gestionar los calendarios de Moodle.
 * @author Víctor Díez Rodríguez
 * @version 1.0
 */
public class CalendarManager {
    /**
     * Elmina un evento de Moodle.
     * @param eventID id del evento a eliminar.
     * @param token token de usuario.
     * @param callback callback que se debe ejecutar con el resultado de la función.
     * @return ejecutor que ejecuta la función.
     */
    public static MoodleFunctionExecutor deleteEvent(int eventID, String token, OnFunctionCompletedListener<String> callback){
        MoodleFunctionExecutor<String> executor=new MoodleFunctionExecutor<String>();
        executor.addParams("events[0][eventid]",String.valueOf(eventID));
        executor.addParams("events[0][repeat]","0");
        executor.addParams("wstoken", token);
        executor.setOnFunctionCompletedListener(callback);
        executor.setParser(SimpleParser.getInstance());
        executor.execute("core_calendar_delete_calendar_events");
        return executor;
    }

    /**
     * Crea un evento de Moodle.
     *
     * @param event evento a crear.
     * @param token token de usuario.
     * @param callback callback que se debe ejecutar con el resultado de la función.
     * @return ejecutor que ejecuta la función.
     */
    public static MoodleFunctionExecutor createEvent(Event event, String token, OnFunctionCompletedListener<String> callback){
        MoodleFunctionExecutor<String> executor=new MoodleFunctionExecutor<String>();
        executor.addParams("events[0][name]", event.getName());
        executor.addParams("events[0][description]", event.getDescription());
        executor.addParams("events[0][timestart]",String.valueOf(event.getTimeStart()));
        executor.addParams("events[0][timeduration]",String.valueOf(event.getDuration()));
        executor.addParams("wstoken",token);
        executor.setOnFunctionCompletedListener(callback);
        executor.setParser(SimpleParser.getInstance());
        executor.execute("core_calendar_create_calendar_events");
        return executor;
    }

    /**
     * Obtiene los elementos de Moodle.
     * @param month mes del que se desea obtener los eventos.
     * @param year año del que se desea obtener los eventos.
     * @param courseID lista de cursos de los que se desea obtener los eventos.
     * @param token token de usuario.
     * @param callback callback que se debe ejecutar con el resultado de la función.
     * @return ejecutor que ejecuta la función.
     */
    public static MoodleFunctionExecutor getEvents(int month, int year, int[] courseID, String token,
                                 OnFunctionCompletedListener<ArrayList<Event>> callback){
        Log.i("Calendario", "Solicitando eventos:" + month + year + courseID);
        MoodleFunctionExecutor<ArrayList<Event>> executor=new MoodleFunctionExecutor<ArrayList<Event>>();

        executor.setOnFunctionCompletedListener(callback);

        executor.addParams("wstoken",token);
        for(int i=0;i<courseID.length;i++){
            executor.addParams("events[courseids]["+i+"]",String.valueOf(courseID[i]));
        }

        //Tiempo del dia 1 del mes en el que está esl calendario;
        Calendar time=new GregorianCalendar(year,
                month,
                1);
        long timeStart=time.getTimeInMillis()/1000;
        time.add(Calendar.MONTH,1);

        long timeEnd=time.getTimeInMillis()/1000;

        executor.addParams("options[timestart]",String.valueOf(timeStart));
        executor.addParams("options[timeend]",String.valueOf(timeEnd));
        executor.setParser(EventListParser.getInstance());
        executor.execute("core_calendar_get_calendar_events");
        return executor;
    }
}
