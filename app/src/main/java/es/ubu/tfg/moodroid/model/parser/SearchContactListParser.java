package es.ubu.tfg.moodroid.model.parser;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import es.ubu.tfg.moodroid.model.Contact;

/**
 * Clase para parsear una cadena JSON en una lista de contactos.
 * @author Víctor Díez Rodríguez
 * @version 1.0
 *
 *
 */
public class SearchContactListParser implements MoodleJSONParser<ArrayList<Contact>> {

    private static SearchContactListParser instance;

    private SearchContactListParser(){}

    /**
     * Obtiene una instancia del parser.
     * @return instancia del parser
     */
    public static SearchContactListParser getInstance(){
        if(instance==null){
            instance=new SearchContactListParser();
        }
        return instance;
    }

    /**
     * Parsea la cadena JSON
     *
     * @param jsonString cadena a parsear
     * @return lista de contactos.
     */
    @Override
    public ArrayList<Contact> parseJSON(String jsonString) {
        ArrayList<Contact> contacts = new ArrayList<Contact>();
        Contact contact;

        if(jsonString!=null) {
            try {
                JSONArray contactosJson = new JSONArray(jsonString);
                for (int i = 0; i < contactosJson.length(); i++) {
                    JSONObject jsonContact = contactosJson.getJSONObject(i);
                    contact = new Contact(jsonContact.getString("id"));
                    contact.setFullname(jsonContact.getString("fullname"));
                    try {
                        String image = jsonContact.getString("profileimageurl");
                        contact.setProfileimageurl(image);
                    } catch (JSONException e) {
                        contact.setProfileimageurl(null);
                    }
                    contacts.add(contact);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            for (Contact c : contacts) {
                Log.e("Contactos", c.getFullname());
            }
        }
        return contacts;
    }
}
