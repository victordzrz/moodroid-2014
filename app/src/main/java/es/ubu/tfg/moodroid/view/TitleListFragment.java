package es.ubu.tfg.moodroid.view;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.ListFragment;
import android.support.v7.app.ActionBarActivity;
import android.view.View;

/**
 * Clase que implementa un ListFragment que permite cambiar el título de la actividad en la que está contenida.
 *
 * @version 1.0
 * @author Víctor Díez Rodríguez
 */
public class TitleListFragment extends ListFragment implements ActionBarTitleChanger {

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setListShownNoAnimation(true);
    }

    /**
     * @see es.ubu.tfg.moodroid.view.ActionBarTitleChanger#setTitleEnabled(boolean)
     */
    @Override
    public void setTitleEnabled(boolean enable) {
        FragmentActivity activity=getActivity();
        if(activity instanceof ActionBarActivity){
            ActionBarActivity actionBarActivity=(ActionBarActivity)getActivity();
            actionBarActivity.getSupportActionBar().setDisplayShowTitleEnabled(enable);
        }
        else{
            throw new RuntimeException("No se puede cambiar el título. La actividad no tiene ActionBar ");

        }
    }

    /**
     * @see es.ubu.tfg.moodroid.view.ActionBarTitleChanger#setTitle(String)
     **/
    @Override
    public void setTitle(String title) {
        FragmentActivity activity=getActivity();
        if(activity instanceof ActionBarActivity){
            ActionBarActivity actionBarActivity=(ActionBarActivity)getActivity();
            actionBarActivity.getSupportActionBar().setTitle(title);
        }
        else{
            throw new RuntimeException("No se puede cambiar el título. La actividad no tiene ActionBar ");
        }

    }
}
