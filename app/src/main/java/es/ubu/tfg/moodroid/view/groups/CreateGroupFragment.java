package es.ubu.tfg.moodroid.view.groups;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import es.ubu.tfg.moodroid.R;
import es.ubu.tfg.moodroid.presenter.GroupManager;
import es.ubu.tfg.moodroid.presenter.OnFunctionCompletedListener;
import es.ubu.tfg.moodroid.view.TitleFragment;

/**
 * Clase que implementa la vista para crear un nuevo grupo.
 *
 * @author Víctor Díez Rodríguez
 * @version 1.0
 */
public class CreateGroupFragment extends TitleFragment {
    /**
     * Id del curso del que se desea crear un grupo.
     */
    private String courseid;
    /**
     * Nombre del grupo.
     */
    private String name;
    /**
     * Descripción del grupo.
     */
    private String description;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        courseid=getArguments().getString("courseid");
        if(savedInstanceState!=null){
            name=savedInstanceState.getString("name");
            description =savedInstanceState.getString("description");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.create_group_layout,container,false);
        if(name!=null){
            ((EditText)rootView.findViewById(R.id.creargrupo_nombre)).setText(name);
        }
        if(description !=null){
            ((EditText)rootView.findViewById(R.id.creargrupo_descripcion)).setText(description);
        }
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.create_group_menu,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_group_accept) {
            createGroup();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("name",name);
        outState.putString("description",name);
    }

    /**
     * Crea un nuevo grupo en Moodle.
     */
    private void createGroup(){
        String token= PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext()).getString("token","invalid_token");
        String name=((EditText)getView().findViewById(R.id.creargrupo_nombre)).getText().toString();
        String descripcion=((EditText)getView().findViewById(R.id.creargrupo_descripcion)).getText().toString();
        GroupManager.createGroup(courseid, name, descripcion, token, new OnFunctionCompletedListener<String>() {
            @Override
            public void onFunctionCompleted(String jsonString) {
                Toast toast = Toast.makeText(getActivity().getApplicationContext(),
                        "Grupo creado",
                        Toast.LENGTH_SHORT);
                toast.show();
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
    }
}
