package es.ubu.tfg.moodroid.test.model.parser;

import android.test.InstrumentationTestCase;

import java.util.Random;

import es.ubu.tfg.moodroid.model.parser.SimpleParser;

/**
 * Clase de test para SimpleParser
 */
public class SimpleParserTest extends InstrumentationTestCase {

    public void testGetInstance(){
        SimpleParser simpleParser=SimpleParser.getInstance();
        final int expected=simpleParser.hashCode();
        assertEquals(expected,SimpleParser.getInstance().hashCode());
    }

    public void testParse(){
        int testCount=100;
        String[] strings=new String[testCount];
        Random random=new Random();
        for(int i=0;i<testCount;i++){
            strings[i]=Long.toHexString(random.nextLong());
        }
        SimpleParser parser= SimpleParser.getInstance();

        for(int i=0;i<testCount;i++){
            assertEquals(strings[i],parser.parseJSON(strings[i]));
        }

    }
}
