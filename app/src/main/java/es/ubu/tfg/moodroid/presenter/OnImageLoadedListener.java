package es.ubu.tfg.moodroid.presenter;

import android.graphics.Bitmap;

/**
 * Interfaz para devolver una imagen descargada.
 * @author Víctor Díez Rodríguez
 * @version 1.0
 *
 *
 */
public interface OnImageLoadedListener {
    /**
     * Devuelve la imagen descargada.
     * @param image imagen descargada.
     */
    public void onImageLoaded(Bitmap image);
}
