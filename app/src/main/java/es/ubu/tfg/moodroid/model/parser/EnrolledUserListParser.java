package es.ubu.tfg.moodroid.model.parser;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

import es.ubu.tfg.moodroid.model.Course;
import es.ubu.tfg.moodroid.model.Group;
import es.ubu.tfg.moodroid.model.EnrolledUser;

/**
 * Clase para parsear una cadena JSON en una lista de participantes.
 * @author Víctor Díez Rodríguez
 * @version 1.0
 *
 *
 */
public class EnrolledUserListParser implements MoodleJSONParser<ArrayList<EnrolledUser>>{

    private static EnrolledUserListParser instance;

    private EnrolledUserListParser(){}

    /**
     * Obtiene una instancia del parser.
     * @return instancia del parser
     */
    public static EnrolledUserListParser getInstance(){
        if(instance==null){
            instance=new EnrolledUserListParser();
        }
        return instance;
    }

    /**
     * Parsea la cadena JSON
     *
     * @param jsonString cadena a parsear
     * @return lista de participantes.
     */
    @Override
    public ArrayList<EnrolledUser> parseJSON(String jsonString) {
        final String TAG_NAME = "fullname";
        final String TAG_ID = "id";
        final String TAG_CITY="city";
        final String TAG_EMAIL="email";
        final String TAG_IMGURL="profileimageurl";
        final String TAG_FACCESS="firstaccess";
        final String TAG_LACCESS="lastaccess";
        final String TAG_ROLES="roles";
        final String TAG_ROLENAME="name";
        final String TAG_ENROLLEDCOURSES="enrolledcourses";
        final String TAG_GROUPS="groups";
        final String TAG_GROUPNAME="name";
        final String TAG_DESCRIPTION="description";

        ArrayList<EnrolledUser> enrolledUsers =new ArrayList<EnrolledUser>();

        if(jsonString!=null) {
            try {
                JSONArray participantesJson = new JSONArray(jsonString);

                for (int i = 0; i < participantesJson.length(); i++) {
                    JSONObject c = participantesJson.getJSONObject(i);

                    EnrolledUser enrolledUser = new EnrolledUser(c.getInt(TAG_ID), c.getString(TAG_NAME));

                    try {
                        enrolledUser.setCity(c.getString(TAG_CITY));
                    } catch (JSONException e) {
                        Log.e("ERROR", "No se pudo obtener la ciudad.");
                        e.printStackTrace();
                    }
                    enrolledUser.setEmail(c.getString(TAG_EMAIL));
                    enrolledUser.setProfileImageURL(c.getString(TAG_IMGURL));

                    enrolledUser.setFirstAccess(new Date(Long.valueOf(c.getInt(TAG_FACCESS)) * 1000));
                    enrolledUser.setLastAccess(new Date(Long.valueOf(c.getInt(TAG_LACCESS)) * 1000));

                    JSONArray roles = c.getJSONArray(TAG_ROLES);
                    for (int j = 0; j < roles.length(); j++) {
                        JSONObject rol = roles.getJSONObject(j);
                        enrolledUser.addRole(rol.getString(TAG_ROLENAME));
                    }

                    JSONArray cursos = c.getJSONArray(TAG_ENROLLEDCOURSES);
                    for (int j = 0; j < cursos.length(); j++) {
                        JSONObject curso = cursos.getJSONObject(j);
                        enrolledUser.addCourse(new Course(
                                String.valueOf(curso.getInt(TAG_ID)),
                                curso.getString(TAG_NAME)));
                    }

                    try {
                        JSONArray groups = c.getJSONArray(TAG_GROUPS);
                        for (int j = 0; j < groups.length(); j++) {
                            JSONObject groupJSON = groups.getJSONObject(j);
                            Group group = new Group(groupJSON.getInt(TAG_ID));
                            group.setDescription(groupJSON.getString(TAG_DESCRIPTION));
                            group.setName(groupJSON.getString(TAG_GROUPNAME));

                            enrolledUser.addGroup(group);
                        }
                    } catch (JSONException e) {
                        Log.e("INFO", "No tiene permiso para ver los grupos");
                    }

                    enrolledUsers.add(enrolledUser);
                }
            } catch (JSONException e) {
                Log.e("ERROR", "No se pudo obtener los participantes.");
                e.printStackTrace();
            }
        }
        return enrolledUsers;
    }
}
