package es.ubu.tfg.moodroid.view;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;

/**
 * Clase que implementa un Fragment que permite cambiar el título de la actividad en la que está contenida.
 *
 * @version 1.0
 * @author Víctor Díez Rodríguez
 */
public class TitleFragment extends Fragment implements ActionBarTitleChanger {

    /**
     * @see es.ubu.tfg.moodroid.view.ActionBarTitleChanger#setTitleEnabled(boolean)
     */
    @Override
    public void setTitleEnabled(boolean enable) {
        FragmentActivity activity=getActivity();
        if(activity instanceof ActionBarActivity){
            ActionBarActivity actionBarActivity=(ActionBarActivity)getActivity();
            actionBarActivity.getSupportActionBar().setDisplayShowTitleEnabled(enable);
        }
        else{
            throw new RuntimeException("No se puede cambiar el título. La actividad no tiene ActionBar ");

        }
    }

    /**
     *@see es.ubu.tfg.moodroid.view.ActionBarTitleChanger#setTitle(String)
     */
    @Override
    public void setTitle(String title) {
        FragmentActivity activity=getActivity();
        if(activity instanceof ActionBarActivity){
            ActionBarActivity actionBarActivity=(ActionBarActivity)getActivity();
            actionBarActivity.getSupportActionBar().setTitle(title);
        }
        else{
            throw new RuntimeException("No se puede cambiar el título. La actividad no tiene ActionBar ");
        }

    }
}
