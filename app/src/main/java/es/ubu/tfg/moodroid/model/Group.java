package es.ubu.tfg.moodroid.model;

import java.io.Serializable;

/**
 *  Clase para representar un grupo de Moodle.
 * @author Víctor Díez Rodríguez
 * @version 1.0
 *
 *
 */
public class Group implements Serializable {
    /**
     * id del grupo.
     */
    int id;
    /**
     * id del curso al que pertenece el grupo.
     */
    int courseid;
    /**
     * Nombre del grupo.
     */
    String name;
    /**
     * Descripción del grupo.
     */
    String description;

    public Group(int id){
        this.id=id;
    }

    public int getId() {
        return id;
    }

    public int getCourseid() {
        return courseid;
    }

    public void setCourseid(int courseid) {
        this.courseid = courseid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
