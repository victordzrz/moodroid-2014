package es.ubu.tfg.moodroid.model;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import android.util.Base64;
/**
 * Clase que implementa Base64Encoder.
 * @author Alvar Alonso �lvarez
 * @version 1.0
 */
public class Base64Encoder {
	/**
	 * Convierte fileName en Base 64 para subir el archivo al servidor. 
	 * @param fileName El archivo que queremos subir al servidor.
	 * @return El archivo convertido a Base 64.
	 */
	public static String base64encode(String fileName) {
		String encodedString = "";

		try {
			File file = new File(fileName);
			if (!file.exists()) {
				throw new Exception("File not found.");
			}
			InputStream inputStream = new FileInputStream(file);
			byte[] bytes;
			byte[] buffer = new byte[8192];
			int bytesRead;
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			try {
				while ((bytesRead = inputStream.read(buffer)) != -1) {
					output.write(buffer, 0, bytesRead);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			bytes = output.toByteArray();
			encodedString = Base64.encodeToString(bytes, Base64.DEFAULT);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return encodedString;
	}
}
