package es.ubu.tfg.moodroid.view;

/**
 * Interfaz que permite cambiar el título de la vista.
 * @author Víctor Díez Rodríguez
 * @version 1.0
 */
public interface ActionBarTitleChanger {
    /**
     * Habilita o deshabilita el título de la vista.
     * @param enable si el título está habilitado.
     */
    public void setTitleEnabled(boolean enable);

    /**
     * Establece el título de la vista.
     * @param title título de la vista.
     */
    public void setTitle(String title);
}
