package es.ubu.tfg.moodroid.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Clase para representar un participante de un curso de Moodle.
 * @author Víctor Díez Rodríguez
 * @version 1.0
 */
public class EnrolledUser implements Serializable {
    /**
     * id del participante.
     */
    private int id;
    /**
     * Nombre del participante.
     */
    private String fullname;
    /**
     * Email del participante.
     */
    private String email;
    /**
     * Ciudad del participante.
     */
    private String city;
    /**
     * Primer acceso de participante.
     */
    private Date firstAccess;
    /**
     * Último acceso del participante.
     */
    private Date lastAccess;
    /**
     * URL de la imagen de perfil del participante.
     */
    private String profileImageURL;
    /**
     * Roles del participante.
     */
    private ArrayList<String> roles;
    /**
     * Cursos en los que participa.
     */
    private ArrayList<Course> courses;
    /**
     * Grupos a los que pertenece el participante.
     */
    private ArrayList<Group> groups;


    public EnrolledUser(int id, String fullname){
        this.id=id;
        this.fullname=fullname;
        this.roles=new ArrayList<String>();
        this.courses=new ArrayList<Course>();
        this.groups =new ArrayList<Group>();
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setFirstAccess(Date firstAccess) {
        this.firstAccess = firstAccess;
    }

    public void setLastAccess(Date lastAccess) {
        this.lastAccess = lastAccess;
    }

    public void setProfileImageURL(String profileImageURL) {
        this.profileImageURL = profileImageURL;
    }

    public void addRole(String role){
        this.roles.add(role);
    }

    public void addCourse(Course course){
        this.courses.add(course);
    }

    public void addGroup(Group group){this.groups.add(group);}

    public int getId() {
        return id;
    }

    public String getFullname() {
        return fullname;
    }

    public String getEmail() {
        return email;
    }

    public String getCity() {
        return city;
    }

    public Date getFirstAccess() {
        return firstAccess;
    }

    public Date getLastAccess() {
        return lastAccess;
    }

    public String getProfileImageURL() {
        return profileImageURL;
    }

    public ArrayList<String> getRoles() {
        return roles;
    }

    public ArrayList<Course> getCourses() {
        return courses;
    }

    public ArrayList<Group> getGroups(){return groups;}
}
